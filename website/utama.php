<?php
	session_start();
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	date_default_timezone_set('Asia/Jakarta');
	set_time_limit(0);
	ob_start();


	define('CONNECTED_FILE_WEB', 'dispendaMedan100%');
	include("./includes/defines.php");
	include(P_INCLUDES.'sql_db.php');
	include(P_INCLUDES.'functions.php');

	$sql = new sql_db();
	if(!$sql)
		die('Cannot connect to database');

	$d = $sql->sql_query("select * from tbl_connect where id_connect='1'");
	$d1 = $sql->sql_fetchrow($d);

	define('CONNECTED', $d1["connected"]);

	include(P_INCLUDES.'session.php');
	$sess = new session();

	include(P_MODULES.'main.php');//define('P_MODULES', 'modules/');

	$sql->sql_close();
	ob_flush();
?>