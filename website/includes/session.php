<?php
class session extends sql_db
{
	public $status_session = false;
	public $no_session = '';

	function cek_session($session)
	{
		$d = parent::sql_query("select id_user from tbl_user where session_id='".$session."'");
		if(parent::sql_numrows($d)>=1)
		{
			$this->status_session = true;
			$this->no_session = $session;
			return true;
		}
		else
			return false;
	}

	function get_iduser($session)
	{
		$d = parent::sql_query("select id_user from tbl_user where session_id='".$session."'");
		$d1 = parent::sql_fetchrow($d);

		return $d1["id_user"];
	}

	function cek_permission()
	{
		return true;
	}

	function login($username, $password, $kode='')
	{

		$password = md5($password);
		if($kode != $_SESSION["captcha_session"])
			return false;
		else
		{
			$d = parent::sql_query("select id_level, id_user, email, id_jenis, id_status from tbl_user where username='$username' and `password`='$password'");
			if(parent::sql_numrows($d)>=1)
			{
				$d1 = parent::sql_fetchrow($d);
				$level = $b1["id_level"];
				$user = $d1["id_user"];
				$jenis = $d1["id_jenis"];
				$status = $d1["id_status"];


				$kode = $this->md5_session_login($d1["username"], $d1["nama"], $_SEVER["REMOTE_ADDR"]);
				$d = parent::sql_query("update tbl_user set session_id='$kode', ip_address='".$_SEVER["REMOTE_ADDR"]."' where id_user='".$d1["id_user"]."'");

				if($d)
				{
					$array[0] = $kode;
					$array[1] = $level;
					$array[2] = $user;
					$array[3] = $jenis;
					$array[4] = $status;

					return $array;
				}
				else
					return false;
			}
			else
				return false;
		}
	}

	function md5_session_login($username, $nama, $ip)
	{
		$mid = round(strlen($username)/2, 0);
		$a = substr($username, 0, $mid);
		$b = substr($username, $mid);

		$hasil = md5($a.$nama.$b);
		$hasil .= md5($ip);
		$hasil .= md5(date("Y-m-g G:i:s"));

		return $hasil;
	}

	function cek_email($email)
	{
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
 			 $emailErr = "Invalid email format";
		}
		else
			return true;
	}

	function registrasi($nama, $email, $password, $kode)
	{
		if(strlen($password)<6)
			return 'Password harus lebih dari 6 karakter';
		elseif($kode != $_SESSION["captcha_session"])
			return 'Kode yang Anda masukkan salah';
		elseif($this->cek_email($email) != true)
			return 'Format email yang Anda masukkan salah';
		else
		{
			$d = parent::sql_query("select id_user from tbl_user where email='$email'");
			if(parent::sql_numrows($d)>=1)
				return 'Email yang Anda masukkan sudah terigistrasi';
			else
			{
				$d = parent::sql_query("insert into tbl_user (nama, email, username,  password, id_level) values ('$nama','$email','$email',md5('$password'),'2')");
				if($d)
				{
					$d = parent::sql_query("select id_user, username, nama from tbl_user where email='$email'");
					$d1 = parent::sql_fetchrow($d);

					$kode = $this->md5_session_login($d1["username"], $d1["nama"], $_SEVER["REMOTE_ADDR"]);

					$c = parent::sql_query("update tbl_user set session_id='$kode', ip_address='".$_SEVER["REMOTE_ADDR"]."' where id_user='".$d1["id_user"]."'");

					$array[0] = $kode;
					$array[1] = $level;
					$array[2] = $user;
					$array[3] = $jenis;
					$array[4] = $status;

					return $array;
				}
				else
					return 'Error';
			}
		}
	}

	function update_profil($nama, $alamat, $email, $kontak, $username, $password, $user)
	{
		if($this->cek_email($email) != true)
			return 'Email yang Anda Masukkan Salah';
		else
		{
			if($password == '')
			{
				$d = parent::sql_query("update tbl_user set nama='$nama', email='$email', kontak='$kontak', alamat='$alamat', username='$username' where session_id='$user'");
				if($d) return true; else return 'Gagal';
			}
			else
			{
				if(strlen($password)<6)
					return 'Password harus 6 karakter atau lebih';
				else
				{
					$d = parent::sql_query("update tbl_user set nama='$nama', email='$email', kontak='$kontak', alamat='$alamat', username='$username', password=md5('$password') where session_id='$user'");
					if($d) return true; else return 'Gagal';
				}
			}
		}
	}
}
?>
