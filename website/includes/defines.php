<?php
	define('DS', DIRECTORY_SEPARATOR);
	//$parts = explode( DS, P_BASE );
	//define( 'P_ROOT',implode( DS, $parts ) );
	define('URI', $_SERVER["REQUEST_URI"]);

	define('P_TITLE', '::: DINAS PENANAMAN MODAL DAN PELAYANAN PERIZINAN TERPADU SATU PINTU
PEMERINTAHAN KABUPATEN NIAS SELATAN :::');
	define("P_SESSIONADMIN","SESS_WEBDISOSKERMEDAN");
	$_SESSION[P_SESSIONADMIN] = isset($_SESSION[P_SESSIONADMIN]) ? $_SESSION[P_SESSIONADMIN] : '';
	define("P_SESSIONUSER","SESS_USERWEBDISOSKERMEDAN");
	$_SESSION[P_SESSIONUSER] = isset($_SESSION[P_SESSIONUSER]) ? $_SESSION[P_SESSIONUSER] : '';
	define('P_BATAS', 50);
	define('P_LIBRARIES', 'libraries/');
	define('P_MODULES', 'modules/');
	define("P_INCLUDES", 'includes/');
	define("P_CSS",'css/');
	define("P_ADMIN",'AdM14W3b/');
	define("P_AD",'admin/');
	define("P_JS",'js/');
	define('P_IMAGES', 'images/');
	define('P_PLUGIN', 'plugin/');
	define('P_SESSION', $_SESSION[P_SESSIONADMIN]);
	define('P_SESS', $_SESSION[P_SESSIONUSER]);
	define('P_SERVERNAME', 'http://'.$_SERVER["SERVER_NAME"]);	

	define('P_NAME', 'disosnaker');

	$slash = explode("/", URI);
	$jlh_slash = count($slash) - 4;
	if($jlh_slash >0)
	{
		$slash = "";
		for($i=0; $i<$jlh_slash; $i++)
		{
			$slash .= '../';
		}

		define('P_SLASH', $slash);
	}
	else
		define('P_SLASH', './');

?>
