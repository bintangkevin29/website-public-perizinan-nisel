<?php
function anti($data)
{
	$data1 = mysql_real_escape_string(stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
	return $data1;
}

function tgl_indo($tgl)
{
	$tanggal = substr($tgl,8,2);
	$bulan = getBulan(substr($tgl,5,2));
	$tahun = substr($tgl,0,4);

	return $tanggal.' '.$bulan.' '.$tahun;		 
}	

function getBulan($bln)
{
	switch ($bln)
	{
		case 1: 
			return "Januari";
			break;
		case 2:
			return "Februari";
			break;
		case 3:
			return "Maret";
			break;
		case 4:
			return "April";
			break;
		case 5:
			return "Mei";
			break;
		case 6:
			return "Juni";
			break;
		case 7:
			return "Juli";
			break;
		case 8:
			return "Agustus";
			break;
		case 9:
			return "September";
			break;
		case 10:
			return "Oktober";
			break;
		case 11:
			return "November";
			break;
		case 12:
			return "Desember";
			break;
	}
}

function getBulanShort($bln)
{
	switch ($bln)
	{
		case 1: 
			return "Jan";
			break;
		case 2:
			return "Feb";
			break;
		case 3:
			return "Mar";
			break;
		case 4:
			return "Apr";
			break;
		case 5:
			return "Mei";
			break;
		case 6:
			return "Jun";
			break;
		case 7:
			return "Jul";
			break;
		case 8:
			return "Agu";
			break;
		case 9:
			return "Sep";
			break;
		case 10:
			return "Okt";
			break;
		case 11:
			return "Nov";
			break;
		case 12:
			return "Des";
			break;
	}
}

function max_file()
{
	$phpmaxsize = ini_get('upload_max_filesize');
	$phpmaxsize = trim($phpmaxsize);
	$last = strtolower($phpmaxsize{strlen($phpmaxsize)-1});

	switch($last)
	{
		case 'g':
			$phpmaxsize *= 1024;
		case 'm':
			$phpmaxsize *= 1024;
	}
	return $phpmaxsize;
}

function pesan($text, $link)
{
?>
	<script type="text/javascript">
		alert('<?php echo $text; ?>');
		document.location.href='<?php echo $link; ?>';
	</script>
<?php
}

function berhasil($link)
{
?>
	<script type="text/javascript">
		alert('Berhasil menyimpan data');
		document.location.href='<?php echo $link; ?>';
	</script>
<?php
}

function gagal($link)
{
?>
	<script type="text/javascript">
		alert('Gagal menyimpaan data');
		document.location.href='<?php echo $link; ?>';
	</script>
<?php
}

function get_thumbs($text)
{
	return str_replace('images', '.thumbs/images', $text);
}

function cek_email($email)
{
	if(preg_match('/^[^@]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$/', $email))
		return true;
	else
		return false;
}
?>