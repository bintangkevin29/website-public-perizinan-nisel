<?php
class wilayah extends session{
	function set()
	{
    	echo '<script type="text/javascript" src="'.P_SLASH.P_JS.'wilayah/wilayah.js"></script>';
	}

	private function onchange($jenis, $asal, $target, $opsi1='', $pesan_opsi1='', $opsi2='', $pesan_opsi2='')
	{
		if($jenis == 'kabupaten.html')
			return "onchange=\"wilayah('".P_SLASH."','#$asal','#$target','$jenis','#$opsi1','$pesan_opsi1','#$opsi2','$pesan_opsi2');\"";
		elseif($jenis == 'kecamatan.html')
			return "onchange=\"wilayah('".P_SLASH."','#$asal','#$target','$jenis','#$opsi1','$pesan_opsi1');\"";
		elseif($jenis == 'kelurahan.html')
			return "onchange=\"wilayah('".P_SLASH."','#$asal','#$target','$jenis');\"";
	}
	function get_provinsi($provinsi=0, $required=false, $name='', $onchange='', $target='', $opsi1='', $pesan_opsi1='', $opsi2='', $pesan_opsi2='')
	{
		if($onchange == true)
		{
			$onchange = $this->onchange('kabupaten.html', $name, $target, $opsi1, $pesan_opsi1, $opsi2, $pesan_opsi2);
		}
		$required = $this->get_required($required);
		echo '<select class="form-control" id="'.$name.'" name="'.$name.'" '.$required.' '.$onchange.'>';
		echo '<option value="">Pilih Provinsi</option>';
		$d = sql_db::sql_query("select id_provinsi, provinsi from tbl_provinsi order by id_provinsi asc");
		while($d1 = sql_db::sql_fetchrow($d))
		{
			if($provinsi == $d1["id_provinsi"])
				echo '<option value="'.$d1["id_provinsi"].'" selected>'.$d1["provinsi"].'</option>';
			else
				echo '<option value="'.$d1["id_provinsi"].'">'.$d1["provinsi"].'</option>';
		}
		echo '</select>';

	}

	function get_kabupaten($provinsi=0, $kabupaten=0, $required=false, $name='', $onchange='',$target, $opsi1='', $pesan_opsi1='')
	{
		if($onchange == true)
		{
			$onchange = $this->onchange('kecamatan.html', $name, $target, $opsi1, $pesan_opsi1);
		}
		$required = $this->get_required($required);
		echo '<select class="form-control" id="'.$name.'" name="'.$name.'" '.$required.' '.$onchange.'>';
		echo '<option value="">Pilih Kabupaten</option>';
		$d = parent::sql_query("select id_kabupaten, kabupaten from tbl_kabupaten where id_provinsi='$provinsi' order by id_kabupaten asc");
		while($d1 = parent::sql_fetchrow($d))
		{
			if($kabupaten == $d1["id_kabupaten"])
				echo '<option value="'.$d1["id_kabupaten"].'" selected>'.$d1["kabupaten"].'</option>';
			else
				echo '<option value="'.$d1["id_kabupaten"].'">'.$d1["kabupaten"].'</option>';
		}
		echo '</select>';
	}

	function get_kecamatan($provinsi=0, $kabupaten=0, $kecamatan=0, $required=false, $name='', $onchange='',$target)
	{
		if($onchange == true)
		{
			$onchange = $this->onchange('kelurahan.html', $name, $target);
		}

		$required = $this->get_required($required);
		echo '<select class="form-control" id="'.$name.'" name="'.$name.'" '.$required.' '.$onchange.'>';
		echo '<option value="">Pilih Kecamatan</option>';
		$d = parent::sql_query("select id_kecamatan, kecamatan from tbl_kecamatan where id_provinsi='$provinsi' and id_kabupaten='$kabupaten' order by id_kecamatan asc");
		while($d1 = parent::sql_fetchrow($d))
		{
			if($kecamatan == $d1["id_kecamatan"])
				echo '<option value="'.$d1["id_kecamatan"].'" selected>'.$d1["kecamatan"].'</option>';
			else
				echo '<option value="'.$d1["id_kecamatan"].'">'.$d1["kecamatan"].'</option>';
		}
		echo '</select>';
	}

	function get_kelurahan($provinsi=0, $kabupaten=0, $kecamatan=0, $kelurahan=0, $required=false, $name='', $onchange='')
	{
		$required = $this->get_required($required);
		echo '<select class="form-control" id="'.$name.'" name="'.$name.'" '.$required.' '.$onchange.'">';
		echo '<option value="">Pilih Kelurahan</option>';
		$d = parent::sql_query("select id_kelurahan, kelurahan from tbl_kelurahan where id_provinsi='$provinsi' and id_kabupaten='$kabupaten' and id_kecamatan='$kecamatan' order by id_kecamatan asc");
		while($d1 = parent::sql_fetchrow($d))
		{
			if($kelurahan == $d1["id_kelurahan"])
				echo '<option value="'.$d1["id_kelurahan"].'" selected>'.$d1["kelurahan"].'</option>';
			else
				echo '<option value="'.$d1["id_kelurahan"].'">'.$d1["kelurahan"].'</option>';
		}
		echo '</select>';
	}

	private function get_required($required)
	{
		if($required == true)
			return 'required="required"';
		else
			return '';
	}
}
?>
