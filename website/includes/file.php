<?php
class files extends sql_db
{

	function insert($ijin, $dir, $nama_file, $jenis_file, $keterangan, $user)
	{
		$c = parent::sql_query("insert into tbl_file (id_ijin, dir, nama_file, id_jenis_file, keterangan, id_user) values ('$ijin','$dir','$nama_file','$jenis_file','$keterangan','$user')");

		if($c)
			return true;
		else
			return false;
	}

	public function exists_file($upload_dir, $nama_file)
	{
		if(file_exists($upload_dir.$nama_file))
		{
			$true = false;
			$ext = pathinfo($nama_file, PATHINFO_EXTENSION);
			$file = basename($nama_file, ".$ext");
			$i=1;
			while($true == false)
			{
				if(!file_exists($upload_dir.$file.'('.$i.').'.$ext))
				{
					$true = true;
					$nama = $file.'('.$i.').'.$ext;
				}
				$i++;
			}

			return $nama;
		}
		else
			return $nama_file;
	}

	public function upload_file($tmp_file, $type_file, $upload_dir, $nama_file='')
	{
		$nama = $this->exists_file($upload_dir,$nama_file);
		if(move_uploaded_file($tmp_file, $upload_dir.$nama))
			return $nama;
		else
			return false;
	}

	public function upload_dokumen($tmp_file, $type_file, $upload_dir, $nama_file)
	{
		if($this->cek_dokumen($type_file) == false)
			return false;
		else
		{
			if(move_uploaded_file($tmp_file, $upload_dir.$this->exists_file($upload_dir,$nama_file)))
				return true;
			else
				return false;
		}

	}

	public function upload_gambar($tmp_file, $type_file, $upload_dir, $nama_file, $width_size=0)
	{

		if($this->cek_gambar($type_file) == false)
			return false;
		else
		{
			$nama = $this->exists_file($upload_dir,$nama_file);
			if(!move_uploaded_file($tmp_file, $upload_dir.$nama))
				return false;
			else
			{
				$count = count($width_size);
				for($i=0; $i<$count; $i++)
				{
					$this->upload_thumb($type_file, $upload_dir, $nama, $width_size[$i], 'thumb'.$i.'_');
				}
				return $nama;
			}
		}
	}

	private function upload_thumb($type_file, $upload_dir, $nama_file, $width, $thumb)
	{
		if($type_file == 'image/jpg' or $type_file == 'image/jpeg')
			$im_src = imagecreatefromjpeg($upload_dir.$nama_file);
		elseif($type_file == 'image/png')
			$im_src = imagecreatefrompng($upload_dir.$nama_file);
		elseif($type_file == 'image/gif')
			$im_src = imagecreatefromgif($upload_dir.$nama_file);

		$src_width = imageSX($im_src);
		$src_height = imageSY($im_src);

		$dst_width1 = $width;
		$dst_height1 = ($dst_width1/$src_width)*$src_height;

		$im1 = imagecreatetruecolor($dst_width1,$dst_height1);
  		imagecopyresampled($im1, $im_src, 0, 0, 0, 0, $dst_width1, $dst_height1, $src_width, $src_height);

		if($type_file == 'image/jpg' or $type_file == 'image/jpeg')
			$d = imagejpeg($im1,$upload_dir."thumb/".$thumb.$nama_file);
		elseif($type_file == 'image/png')
			$d = imagepng($im1,$upload_dir."thumb/".$thumb.$nama_file);
		elseif($type_file == 'image/gif')
			$d = imagegif($im1,$upload_dir."thumb/".$thumb.$nama_file);

		imagedestroy($im_src);
	  	imagedestroy($im1);

		return true;
	}

	public function upload_audio($tmp_file, $type_file, $upload_dir, $nama_file='')
	{
	}

	public function upload_video($tmp_file, $type_file, $upload_dir, $nama_file='')
	{
	}

	private function cek_dokumen($type_file)
	{
		return true;
	}

	public function cek_gambar($type_file)
	{
		if($type_file == 'image/jpg' or $type_file == 'image/jpeg' or $type_file == 'image/png' or $type_file == 'image/gif')
			return true;
		else
			return false;
	}

	private function cek_audio($type_file)
	{
	}

	private function cek_video($type_file)
	{
	}
}
?>
