<?php
class member extends session{
	function __contruct()
	{
		if(!parent::status_session)
			die('You cannot access the sistem');
	}

	private function show_data()
	{
	?>
    	<script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.bootstrap.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.min.js'; ?>"></script>
        <link href="<?php echo P_SLASH.P_JS; ?>datatable/css/jquery.dataTables.css" rel="stylesheet">
        <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable();
			} );
		</script>
        <div><a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>member/tambah.html" class="btn btn-primary">Tambah Member</a></div><br>
		<table class="display" id="example" width="100%">
        <thead>
	       	<tr>
            	<th>NAMA</th>
            	<th>ALAMAT</th>
                <th>EMAIL</th>
                <th>KONTAK</th>
                <th>USERNAME</th>
                <th>PUBLISH</th>
                <th>AKTIVITAS</th>
            </tr>
        </thead>
        <tbody>
	    <?php 
		$d = sql_db::sql_query("select * from tbl_user where id_user <>'".$_SESSION[P_SESSIONADMIN]."' order by nama asc");
		while($d1 = sql_db::sql_fetchrow($d))
		{
		?>
        	<tr>
            	<td><?php echo $d1["nama"]; ?></td>
                <td><?php echo $d1["alamat"]; ?></td>
                <td><?php echo $d1["email"]; ?></td>
                <td><?php echo $d1["kontak"]; ?></td>
                <td><?php echo $d1["username"]; ?></td>
                <td><?php echo $d1["publish"]; ?></td>
                <td class="text-center">
                	<a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>member/<?php echo urlencode($d1["kode"]); ?>/edit.html">EDIT<a>&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>member/<?php echo urlencode($d1["kode"]); ?>/hapus.html" onClick="return confirm('Apakah Anda yakin ingin menghapus data ini?' +  '\n\n' + 'Jika YA silahkan klik OK, Jika TIDAK klik CANCEL.')">HAPUS<a>
                </td>
            </tr>
        <?php
		}
		echo '</tbody></table>';
	}

	private function show_form($heading, $kode)
	{
		if($heading == 'edit')
		{
			$d = sql_db::sql_query("select * from tbl_user where lower(kode)='".urldecode($kode)."'");
			$d1 = sql_db::sql_fetchrow($d);
		}
		else
		{
			$d1["nama"] = '';
			$d1["alamat"] = '';
			$d1["email"] = '';
			$d1["kontak"] = '';
			$d1["username"] = '';
			$d1["publish"] = '';
		}
	?>
    	<link type="text/css" href="<?php echo P_SLASH.P_JS; ?>datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" />
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>datetimepicker/moment-with-locales.js"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript">
			$(function(){
				$("#datetimepicker1").datetimepicker({
					format:	'YYYY-MM-DD HH:mm:ss'
				});
			});
		</script>

    	<form class="form-horizontal" action="<?php echo URI; ?>" method="post">
        	<input type="hidden" name="mode" value="<?php echo $kode; ?>">
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Nama : </label>
                <div class="col-sm-7">
					<input type="text" name="nama" class="form-control" required="required" value="<?php echo $d1["nama"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>E-Mail : </label>
                <div class="col-sm-7">
					<input type="email" name="email" class="form-control" required="required" value="<?php echo $d1["email"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label">Alamat : </label>
                <div class="col-sm-7">
					<input type="text" name="alamat" class="form-control" value="<?php echo $d1["alamat"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>No. Telepon : </label>
                <div class="col-sm-7">
					<input type="text" name="kontak" class="form-control" required="required" value="<?php echo $d1["kontak"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Username : </label>
                <div class="col-sm-7">
					<input type="text" name="username" class="form-control" required="required" value="<?php echo $d1["username"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label">Password : </label>
                <div class="col-sm-7">
					<input type="password" name="password" class="form-control" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Publish : </label>
                <div class="col-sm-7">
					<input type="radio" name="publish" value="N" <?php if($d1["publish"] == '' or $d1["publish"] == 'N') echo 'checked="checked"'; ?> /> Tidak&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="publish" value="Y" <?php if($d1["publish"] == 'Y') echo 'checked="checked"'; ?> /> Ya&nbsp;&nbsp;&nbsp;
                </div>
			</div>
            <div class="form-group">
            	<label class="col-sm-3"></label>
                <div class="col-sm-7">
					<button class="btn btn-default" type="submit" name="submit_tambah">SIMPAN</button>
    	            <button class="btn btn-default" type="reset">RESET</button>
                </div>
			</div>
        </form>
    <?php
	}

	function heading($heading, $mode, $POST)
	{
		if($heading == 'tambah')
		{
			echo '<div class="panel-heading">Tambah Member</div>';
			echo '<div class="panel-body"">';
				if(isset($POST["mode"]))
					$this->tambah($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'edit')
		{
			echo '<div class="panel-heading">Edit Member</div>';
			echo '<div class="panel-body"">';
			if(isset($POST["mode"]))
					$this->edit($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'hapus')
		{
			echo '<div class="panel-heading">Hapus Member</div>';
			echo '<div class="panel-body"">';
			$this->hapus($mode);
			echo '</div>';
		}
		else
		{
			echo '<div class="panel-heading">Member</div>';
			echo '<div class="panel-body"">';
			$this->show_data();
			echo '</div>';
		}
	}

	function tambah($POST)
	{
		$kontak = anti($POST["kontak"]);
		$alamat = anti($POST["alamat"]);
		$username = anti($POST["username"]);
		$email = anti($POST["email"]);
		$nama = anti($POST["nama"]);
		$password = anti($POST["password"]);
		$publish = anti($POST["publish"]);


		if(!$this->cek_form($nama, $kontak, $username, $password, $publish))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('tambah','');
		}
		else
		{
			$d = sql_db::sql_query("insert into tbl_user (nama, email, alamat, kontak, username, password, publish, id_level) values ('$nama','$email','$alamat','$kontak','$username',md5('$password'),'$publish','1')");
			if($d)
				berhasil(P_SLASH.P_AD.P_PLUGIN.'member/tambah.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'member/tambah.html');
		}
	}

	private function edit($POST)
	{
		$kontak = anti($POST["kontak"]);
		$alamat = anti($POST["alamat"]);
		$username = anti($POST["username"]);
		$email = anti($POST["email"]);
		$nama = anti($POST["nama"]);
		$password = anti($POST["password"]);
		$publish = anti($POST["publish"]);
		$kode = anti($POST["mode"]);

			if(empty($password))
				$d = sql_db::sql_query(" update  tbl_user set nama='$nama', email='$email', alamat='$alamat', kontak='$kontak', username='$username', publish='$publish' where kode='$kode'");
			else
				$d = sql_db::sql_query(" update  tbl_user set nama='$nama', email='$email', alamat='$alamat', kontak='$kontak', username='$username', password=md5('$password'), publish='$publish' where kode='$kode'");

			if($d)
				berhasil(P_SLASH.P_AD.P_PLUGIN.'member/'.$kode.'/edit.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'member/'.$kode.'/edit.html');
	}

	private function hapus($kode)
	{
		$d = sql_db::sql_query("delete from tbl_user where kode='$kode'");
		if($d)
			berhasil(P_SLASH.P_AD.P_PLUGIN.'member.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'member.html');
	}

	private function cek_form($nama, $kontak, $username, $password, $publish)
	{
		if($nama == '' or $kontak == '' or $username == '' or $password == '' or $publish=='')
			return false;
		else
			return true;
	}
}

$member = new member;
$opt = isset($_GET["opt"]) ? $_GET["opt"] : '';
$mode = isset($_GET["mode"]) ? $_GET["mode"] : '';

echo '<div class="panel panel-default" style="width:100%;">';
$member->heading(anti($opt), anti($mode), $_POST);
echo '</div>';
?>
