<?php
class module extends session{
	function __contruct()
	{
		if(!parent::status_session)
			die('You cannot access the sistem');
	}

	private function show_data()
	{
	?>
    	<script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.bootstrap.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.min.js'; ?>"></script>
        <link href="<?php echo P_SLASH.P_JS; ?>datatable/css/jquery.dataTables.css" rel="stylesheet">
        <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable();
			} );
		</script>     
		<table class="display" id="example" width="100%">
        <thead>
	       	<tr>
            	<th>MODULE</th>
            	<th>JENIS</th>
            	<th>PUBLISH</th>
                <th>AKTIVITAS</th>
            </tr>
        </thead>
        <tbody>
	    <?php
		$d = sql_db::sql_query("select * from tbl_module where id_connect='0' order by waktu desc");
		while($d1 = sql_db::sql_fetchrow($d))
		{			
		?>
        	<tr>
            	<td><?php echo $d1["nama"]; ?></td>
                <td><?php if($d1["jenis"] == '0') echo 'Plugin Menu Atas'; elseif($d1["jenis"] == '2') echo 'Kategori Content'; elseif($d1["jenis"] == '4') echo 'Plugin Menu Kiri'; ?></td>               
                <td><?php echo $d1["publish"]; ?></td>
                <td class="text-center">
                	<a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>module/<?php echo urlencode($d1["kode"]); ?>/edit.html">EDIT<a>&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>module/<?php echo urlencode($d1["kode"]); ?>/hapus.html" onClick="return confirm('Apakah Anda yakin ingin menghapus data ini?' +  '\n\n' + 'Jika YA silahkan klik OK, Jika TIDAK klik CANCEL.')">HAPUS<a>
                </td>
            </tr>
        <?php
		}
		echo '</tbody></table>';
	}

	private function show_form($heading, $kode)
	{
		if($heading == 'edit')
		{
			$d = sql_db::sql_query("select * from tbl_module where lower(kode)='".urldecode($kode)."'");
			$d1 = sql_db::sql_fetchrow($d);
		}
		else
		{
			$d1["nama"] = '';			
			$d1["publish"] = '';			
		}		
	?>
    	    
		<div align="right" style="margin-bottom:20px;">
			<a href="<?php echo P_SLASH; ?>admin/plugin/module.html" class="link_back">Kembali</a>	 					
        </div> 					
    	<form class="form-horizontal" action="<?php echo URI; ?>" method="post">
        	<input type="hidden" name="mode" value="<?php echo $kode; ?>">      	
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Nama : </label>
                <div class="col-sm-7">                	
					<input type="text" name="nama" class="form-control" required="required" value="<?php echo $d1["nama"]; ?>" />
                </div>
			</div>            
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Publish : </label>
                <div class="col-sm-7">                	
					<input type="radio" name="publish" value="N" <?php if($d1["publish"] == '' or $d1["publish"] == 'N') echo 'checked="checked"'; ?> /> Tidak&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="publish" value="Y" <?php if($d1["publish"] == 'Y') echo 'checked="checked"'; ?> /> Ya&nbsp;&nbsp;&nbsp;
                </div>
			</div>            
            <div class="form-group">
            	<label class="col-sm-3"></label>
                <div class="col-sm-7">
					<button class="btn btn-default" type="submit" name="submit_tambah">SIMPAN</button>
    	            <button class="btn btn-default" type="reset">RESET</button>
                </div>
			</div>          
        </form>
    <?php
	}
	
	function heading($heading, $mode, $POST)
	{
		if($heading == 'tambah')
		{
			echo '<div class="panel-heading">Tambah Modul</div>';
			echo '<div class="panel-body"">';
				if(isset($POST["mode"]))
					$this->tambah($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'edit')
		{
			echo '<div class="panel-heading">Edit Modul</div>';
			echo '<div class="panel-body"">';
			if(isset($POST["mode"]))
					$this->edit($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'hapus')
		{
			echo '<div class="panel-heading">Hapus Modul</div>';
			echo '<div class="panel-body"">';
			$this->hapus($mode);
			echo '</div>';
		}
		else
		{
			echo '<div class="panel-heading">Modul</div>';
			echo '<div class="panel-body"">';
			$this->show_data();
			echo '</div>';
		}
	}
		

	private function edit($POST)
	{		
		$publish = anti($POST["publish"]);		
		$nama = anti($POST["nama"]);		
		$kode = anti($POST["mode"]);
		
		if(!$this->cek_form($nama, $publish))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('edit','$kode');
		}
		else
		{					
			$d = sql_db::sql_query(" update  tbl_module set nama='$nama', publish='$publish' where kode='$kode'");

			if($d)
				berhasil(P_SLASH.P_AD.P_PLUGIN.'module/'.$kode.'/edit.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'module/'.$kode.'/edit.html');
		}		
	}
	
	private function hapus($kode)
	{		
		$d = sql_db::sql_query("delete from tbl_module where kode='$kode'");
		if($d)
			berhasil(P_SLASH.P_AD.P_PLUGIN.'module.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'module.html');
	}
	
	private function cek_form($nama, $publish)
	{
		if($nama == '' or $publish=='')
			return false;
		else
			return true;
	}
}

$module = new module;
$opt = isset($_GET["opt"]) ? $_GET["opt"] : '';
$mode = isset($_GET["mode"]) ? $_GET["mode"] : '';

echo '<div class="panel panel-default" style="width:100%;">';
$module->heading(anti($opt), anti($mode), $_POST);
echo '</div>';
?>