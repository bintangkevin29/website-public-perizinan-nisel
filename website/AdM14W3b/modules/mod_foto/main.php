<?php
class album extends session
{
	function __contruct()
	{
		if(!parent::status_session)
			die('You cannot access the sistem');
	}
	
	private function show_data()
	{
	?>
    	<script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.bootstrap.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.min.js'; ?>"></script>
        <link href="<?php echo P_SLASH.P_JS; ?>datatable/css/jquery.dataTables.css" rel="stylesheet">
        <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable();
			} );
		</script>
    	<div><a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>foto/tambah.html" class="btn btn-primary">Tambah Foto</a></div><br>
        <div>
        	<form action="<?php echo URI; ?>" method="post">
        	Album :
            <select name="filter_album" onchange="this.form.submit();">
            	<?php
					$id_album = isset($_POST["filter_album"])? anti($_POST["filter_album"]):'0';
					$id = 1;
					$d = sql_db::sql_query("select a.id_album, a.album, b.galleri from tbl_album a inner join tbl_galleri b on a.id_galleri=b.id_galleri order by b.galleri asc, a.album asc");
					while($d1 = sql_db::sql_fetchrow($d))
					{
						if($d1["id_album"] == $id_album or ($id==1 and $id_album==0))
						{
							echo '<option value="'.$d1["id_album"].'" selected="selected">'.$d1["galleri"].' - '.$d1["album"].'</option>';
							$id_album = $d1["id_album"];
						}
						else
							echo '<option value="'.$d1["id_album"].'">'.$d1["galleri"].' - '.$d1["album"].'</option>';
						$id++;
					}
				?>
            </select>
            </form>
        </div>
		<table class="display table-responsive" id="example" width="100%">
        <thead>
        	<tr>
            	<th>GALLERI</th>
                <th>ALBUM</th>
                <th>KETERANGAN</th>
                <th>TANGGAL</th>
                <th>PUBLISH</th>
                <th>GAMBAR</th>
                <th>AKTIVITAS</th>
            </tr>
        </thead>
        <tbody>
     <?php
		$d = sql_db::sql_query("select a.id_foto, a.keterangan, b.album, c.galleri, a.waktu, a.publish, a.kode, a.nama_file from tbl_foto a inner join tbl_album b on a.id_album=b.id_album inner join tbl_galleri c on b.id_galleri=c.id_galleri where a.id_album='$id_album' order by a.waktu asc");
		while($d1 = sql_db::sql_fetchrow($d))
		{
		?>
        	<tr>
            	<td><?php echo $d1["galleri"]; ?></td>
                <td><?php echo $d1["album"]; ?></td>
                <td><?php echo $d1["keterangan"]; ?></td>
                <td><?php echo tgl_indo($d1["waktu"]); ?></td>
                <td class="text-center"><?php echo $d1["publish"]; ?></td>                
                <td class="text-center"><img src="<?php echo P_SERVERNAME.get_thumbs($d1["nama_file"]); ?>" /></td>
                <td class="text-center">
                	<a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>foto/<?php echo urlencode($d1["kode"]); ?>/edit.html">EDIT<a>&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>foto/<?php echo urlencode($d1["kode"]); ?>/hapus.html" onClick="return confirm('Apakah Anda yakin ingin menghapus data ini?' +  '\n\n' + 'Jika YA silahkan klik OK, Jika TIDAK klik CANCEL.')">HAPUS<a>
                </td>
            </tr>
        <?php
		}
		echo '</tbody></table>';
	}
	
	private function show_form($heading, $kode)
	{
		if($heading == 'edit')
		{
			$d = sql_db::sql_query("select * from tbl_foto where lower(kode)='".urldecode($kode)."'");
			$d1 = sql_db::sql_fetchrow($d);
		}
		else
		{
			$d1["keterangan"] = '';
			$d1["id_album"] = '';
			$d1["publish"] = '';
			$d1["file"] = '';
			$d1["nama_file"] = '';
			$d1["waktu"] = date("Y-m-d H:i:s");
		}		

		$_SESSION["KCFINDER"] = array(
			'disabled' => false,
		    'uploadURL' => "/userfiles/".P_NAME."/album/",
		    'uploadDir' => "",
			'thumbWidth' => 100,
		    'thumbHeight' => 100
		);
	?>

    	<link type="text/css" href="<?php echo P_SLASH.P_JS; ?>datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" />		
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>datetimepicker/moment-with-locales.js"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript">
			$(function(){
				$("#datetimepicker1").datetimepicker({
					format:	'YYYY-MM-DD HH:mm:ss'
				});
			});
		</script>
        <script type="text/javascript">
			function openKCFinder(field) {
				window.KCFinder = {
					callBack: function(url) {
						field.value = url;
						window.KCFinder = null;
					}
				};
				window.open('<?php echo P_SLASH; ?>js/kcfinder/browse.php?type=images&dir=files/public', 'kcfinder_textbox',
					'status=0, toolbar=0, location=0, menubar=0, directories=0, ' +
					'resizable=1, scrollbars=0, width=800, height=600'
				);
			}
			</script>		 					
		<div align="right" style="margin-bottom:20px;">
			<a href="<?php echo P_SLASH; ?>admin/plugin/foto.html" class="link_back">Kembali</a>	 					
        </div>
    	<form class="form-horizontal" action="<?php echo URI; ?>" method="post">
        	<input type="hidden" name="mode" value="<?php echo $kode; ?>">
        	<div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Galleri : </label>
                <div class="col-sm-7">                	
					<select name="album" required="required" class="form-control">
                    	<option value="">Pilih Album</option>
                        <?php
							$c = sql_db::sql_query("select a.galleri, b.album, b.id_album from tbl_galleri a inner join tbl_album b on a.id_galleri=b.id_galleri order by a.galleri asc, b.album asc");
							while($c1 = sql_db::sql_fetchrow($c))
							{								
								if($d1["id_album"] == $c1["id_album"])
									echo '<option value="'.$c1["id_album"].'" selected>'.$c1["galleri"].' - '.$c1["album"].'</option>';
								else
									echo '<option value="'.$c1["id_album"].'">'.$c1["galleri"].' - '.$c1["album"].'</option>';								
							}		
						?>
                    </select>
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label">Keterangan : </label>
                <div class="col-sm-7">                	
					<textarea name="keterangan" class="form-control"><?php echo $d1["keterangan"]; ?></textarea>
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Publish : </label>
                <div class="col-sm-7">                	
					<input type="radio" name="publish" value="N" <?php if($d1["publish"] == '' or $d1["publish"] == 'N') echo 'checked="checked"'; ?> /> Tidak&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="publish" value="Y" <?php if($d1["publish"] == 'Y') echo 'checked="checked"'; ?> /> Ya&nbsp;&nbsp;&nbsp;
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Tanggal : </label>
                <div class="col-sm-7">                	
					<input type="text" name="tanggal" class="form-control" id="datetimepicker1" required value="<?php echo $d1["waktu"]; ?>" />
                </div>
			</div>
            <div class="form-group">
            	<label class="col-sm-3 control-label"><span class="required">*</span>Gambar </label>
                <div class="col-sm-7">                	
					<input type="text" name="file" readonly="readonly" onclick="openKCFinder(this)" placeholder="Klik disini dan pilih gambar yang diinginkan dengan mengklik dua kali" style="cursor:pointer" class="form-control" value="<?php echo $d1["nama_file"]; ?>" required="required" />
                </div>                
            </div>
            <div class="form-group">
            	<label class="col-sm-3"></label>
                <div class="col-sm-7">
					<button class="btn btn-default" type="submit" name="submit_tambah">SIMPAN</button>
    	            <button class="btn btn-default" type="reset">RESET</button>
                </div>
			</div>           
        </form>
    <?php
	}
	
	function heading($heading, $mode, $POST)
	{
		if($heading == 'tambah')
		{
			echo '<div class="panel-heading">Tambah Foto</div>';
			echo '<div class="panel-body"">';
				if(isset($POST["mode"]))
					$this->tambah($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'edit')
		{
			echo '<div class="panel-heading">Edit Foto</div>';
			echo '<div class="panel-body"">';
			if(isset($POST["mode"]))
					$this->edit($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'hapus')
		{
			echo '<div class="panel-heading">Hapus Foto</div>';
			echo '<div class="panel-body"">';
			$this->hapus($mode);
			echo '</div>';
		}
		else
		{
			echo '<div class="panel-heading">Foto</div>';
			echo '<div class="panel-body"">';
			$this->show_data();
			echo '</div>';
		}		
	}
	
	function tambah($POST)
	{
		$keterangan = anti($POST["keterangan"]);
		$publish = anti($POST["publish"]);
		$album = anti($POST["album"]);
		$tanggal = anti($POST["tanggal"]);
		$file = anti($POST["file"]);
			
		if(!$this->cek_form($publish, $album, $tanggal, $file))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('tambah','');
		}
		else
		{					
			$d = sql_db::sql_query("insert into tbl_foto (id_album, keterangan, waktu, publish, nama_file, id_user, ip_address) values ('$album','$keterangan','$tanggal','$publish','$file','','".$_SERVER["REMOTE_ADDR"]."')");
			
			if($d)
				berhasil(P_SLASH.P_AD.P_PLUGIN.'foto/tambah.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'foto/tambah.html');
		}
	}

	private function edit($POST)
	{
		$keterangan = anti($POST["keterangan"]);
		$publish = anti($POST["publish"]);
		$album = anti($POST["album"]);
		$tanggal = anti($POST["tanggal"]);
		$file = anti($POST["file"]);
		$kode = anti($POST["mode"]);	

		if(!$this->cek_form( $publish, $album, $tanggal, $file))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('edit','$kode');
		}
		else
		{					
			$d = sql_db::sql_query(" update  tbl_foto set id_album='$album', keterangan='$keterangan', waktu='$tanggal', publish='$publish', nama_file='$file', id_user='', ip_address='".$_SERVER["REMOTE_ADDR"]."' where kode='$kode'");

			if($d)
				berhasil(P_SLASH.P_AD.P_PLUGIN.'foto/'.$kode.'/edit.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'foto/'.$kode.'/edit.html');
		}		
	}
	
	private function hapus($kode)
	{		
		$d = sql_db::sql_query("delete from tbl_foto where kode='$kode'");
		if($d)
			berhasil(P_SLASH.P_AD.P_PLUGIN.'foto.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'foto.html');
	}	

	private function cek_form($publish, $album, $tanggal, $file)
	{
		if($publish == '' or $album == '' or $tanggal == '' or $file== '')
			return false;
		else
			return true;
	}
}

$album = new album;
$opt = isset($_GET["opt"]) ? $_GET["opt"] : '';
$mode = isset($_GET["mode"]) ? $_GET["mode"] : '';

echo '<div class="panel panel-default" style="width:100%;">';
$album->heading(anti($opt), anti($mode), $_POST);
echo '</div>';
?>