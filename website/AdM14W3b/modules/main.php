<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content=""><link rel="shortcut icon" href="./images/logo.jpg" type="image/x-icon">

<title><?php echo P_TITLE; ?></title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>bootstrap/js/bootstrap.min.js"></script>
<link href="<?php echo P_SLASH.P_JS; ?>bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo P_SLASH.P_JS; ?>bootstrap/css/bootstrap-theme.css" rel="stylesheet">
<link href="<?php echo P_SLASH.P_CSS; ?>template.css" rel="stylesheet">
</head>
<body role="document">
	<?php
		include(P_SL.P_MODULES.'menu_componen/header_admin.php');
		echo '<div id="content">';
		if($sess->cek_session(P_SESSION) == true)
			include(P_MODULES.'mod_utama/content.php');
		else
			include(P_MODULES.'mod_utama/login.php');		
		echo '</div>';
		
		include(P_SL.P_MODULES.'menu_componen/foother.php');
	?>    
</body>
</html>
