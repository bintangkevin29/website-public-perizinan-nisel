<?php
class testimoni extends session
{
	function __contruct()
	{
		if(!parent::status_session)
			die('You cannot access the sistem');
	}
	
	private function show_data()
	{
	?>
    	<script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.bootstrap.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.min.js'; ?>"></script>
        <link href="<?php echo P_SLASH.P_JS; ?>datatable/css/jquery.dataTables.css" rel="stylesheet">
        <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable();
			} );
		</script>
    	<div><a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>testimoni/tambah.html" class="btn btn-primary">Tambah Testimoni</a></div><br>
		<table class="display table-responsive" id="example" width="100%">
        <thead>
        	<tr>
            	<th>NAMA</th>
                <th>JABATAN</th>
                <th>TESTIMONI</th>
                <th>PUBLISH</th>
                <th>AKTIVITAS</th>
            </tr>
        </thead>
        <tbody>
     <?php
		$d = sql_db::sql_query("select nama, testimoni, jabatan, publish, kode from tbl_testimoni order by nama asc");
		while($d1 = sql_db::sql_fetchrow($d))
		{
		?>
        	<tr>
            	<td><?php echo $d1["nama"]; ?></td>
                <td><?php echo $d1["jabatan"]; ?></td>
                <td><?php echo $d1["testimoni"]; ?></td>
                <td class="text-center"><?php echo $d1["publish"]; ?></td>
                <td class="text-center">
                	<a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>testimoni/<?php echo urlencode($d1["kode"]); ?>/edit.html">EDIT<a>&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>testimoni/<?php echo urlencode($d1["kode"]); ?>/hapus.html" onClick="return confirm('Apakah Anda yakin ingin menghapus data ini?' +  '\n\n' + 'Jika YA silahkan klik OK, Jika TIDAK klik CANCEL.')">HAPUS<a>
                </td>
            </tr>
        <?php
		}
		echo '</tbody></table>';
	}
	
	private function show_form($heading, $kode)
	{
		if($heading == 'edit')
		{
			$d = sql_db::sql_query("select * from tbl_testimoni where lower(kode)='".urldecode($kode)."'");
			$d1 = sql_db::sql_fetchrow($d);
		}
		else
		{
			$d1["nama"] = '';
			$d1["publish"] = '';
			$d1["testimoni"] = '';
			$d1["jabatan"] = '';
		}
		
	?>
    	
    	<form class="form-horizontal" action="<?php echo URI; ?>" method="post">
        	<input type="hidden" name="mode" value="<?php echo $kode; ?>">        	
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Nama : </label>
                <div class="col-sm-7">                	
					<input type="text" name="nama" class="form-control" required="required" value="<?php echo $d1["nama"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Jabatan : </label>
                <div class="col-sm-7">                	
					<input type="text" name="jabatan" class="form-control" required="required" value="<?php echo $d1["jabatan"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Testimoni : </label>
                <div class="col-sm-7">                	
                	<textarea name="testimoni" class="form-control" required="required" rows="5" cols="20"><?php echo $d1["testimoni"]; ?></textarea>
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Publish : </label>
                <div class="col-sm-7">                	
					<input type="radio" name="publish" value="N" <?php if($d1["publish"] == '' or $d1["publish"] == 'N') echo 'checked="checked"'; ?> /> Tidak&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="publish" value="Y" <?php if($d1["publish"] == 'Y') echo 'checked="checked"'; ?> /> Ya&nbsp;&nbsp;&nbsp;
                </div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-3"></label>
                <div class="col-sm-7">
					<button class="btn btn-default" type="submit" name="submit_tambah">SIMPAN</button>
    	            <button class="btn btn-default" type="reset">RESET</button>
                </div>
			</div>
        </form>
    <?php
	}
	
	function heading($heading, $mode, $POST)
	{
		if($heading == 'tambah')
		{
			echo '<div class="panel-heading">Tambah Testimoni</div>';
			echo '<div class="panel-body"">';
				if(isset($POST["mode"]))
					$this->tambah($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'edit')
		{
			echo '<div class="panel-heading">Edit Testimoni</div>';
			echo '<div class="panel-body"">';
			if(isset($POST["mode"]))
					$this->edit($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'hapus')
		{
			echo '<div class="panel-heading">Hapus Testimoni</div>';
			echo '<div class="panel-body"">';
			$this->hapus($mode);
			echo '</div>';
		}
		else
		{
			echo '<div class="panel-heading">Testimoni</div>';
			echo '<div class="panel-body"">';
			$this->show_data();
			echo '</div>';
		}
		
	}
	
	function tambah($POST)
	{
		$testimoni = anti($POST["testimoni"]);
		$publish = anti($POST["publish"]);
		$nama = anti($POST["nama"]);
		$jabatan = anti($POST["jabatan"]);
		
		if(!$this->cek_form($nama, $testimoni, $publish, $jabatan))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('tambah','');
		}
		else
		{			
			$d = sql_db::sql_query("insert into tbl_testimoni (nama, publish, testimoni, jabatan) values ('$nama','$publish','$testimoni','$jabatan')");			
		
			if($d)
				berhasil(P_SLASH.P_AD.P_PLUGIN.'testimoni/tambah.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'testimoni/tambah.html');
		}
	}
	
	private function edit($POST)
	{
		$testimoni = anti($POST["testimoni"]);
		$publish = anti($POST["publish"]);
		$nama = anti($POST["nama"]);
		$kode = anti($POST["mode"]);
		$jabatan = anti($POST["jabatan"]);
		
		
		
		$d = sql_db::sql_query("update tbl_testimoni set testimoni='$testimoni', publish='$publish', nama='$nama', jabatan='$jabatan' where kode='$kode'");
		
		if($d)
			berhasil(P_SLASH.P_AD.P_PLUGIN.'testimoni/'.$kode.'/edit.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'testimoni/'.$kode.'/edit.html');
	}
	
	private function hapus($kode)
	{
		
		$d = sql_db::sql_query("delete from tbl_testimoni where kode='$kode'");
		if($d)
			berhasil(P_SLASH.P_AD.P_PLUGIN.'testimoni.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'testimoni.html');
	}
	
	private function cek_form($nama, $testimoni, $publish, $jabatan)
	{
		if($nama == '' and $testimoni == '' and $publish == '' and $jabatan == '')
			return false;
		else
			return true;
	}
}

$testimoni = new testimoni;
$opt = isset($_GET["opt"]) ? $_GET["opt"] : '';
$mode = isset($_GET["mode"]) ? $_GET["mode"] : '';

echo '<div class="panel panel-default" style="width:100%;">';
$testimoni->heading(anti($opt), anti($mode), $_POST);
echo '</div>';
?>