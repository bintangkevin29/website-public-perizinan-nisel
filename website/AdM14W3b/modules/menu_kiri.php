<div style="width:100%">
    <div class="list-group">
      <a href="#" class="list-group-item" style="color: white; font-weight: bold; background-color: #D9AF07!important">
			Menu Content
      </a>
      <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>module.html" class="list-group-item">Modul</a>
      <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>kategori.html" class="list-group-item">Kategori</a>
      <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>content.html" class="list-group-item">Content</a>
    </div>

    <div class="list-group">
      <a href="#" class="list-group-item" style="color: white; font-weight: bold; background-color: #D9AF07!important">
        Menu Pesan dan Pengaduan
      </a>
      <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>shout.html" class="list-group-item">Pesan dan Saran (Shout Box)</a>
      <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>pengaduan.html" class="list-group-item">Pengaduan Masyarakat</a>
      <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>buku_tamu.html" class="list-group-item">Buku Tamu</a>
      <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>kat_polling.html" class="list-group-item">Kategori Polling</a>
      <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>polling.html" class="list-group-item">Polling</a>
    </div>
    <div class="list-group">
      <a href="#" class="list-group-item" style="color: white; font-weight: bold; background-color: #D9AF07!important">
        Menu Plugin
      </a>
      <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>kat_download.html" class="list-group-item">Kategori Download</a>
      <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>download.html" class="list-group-item">File Download</a>
      <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>album.html" class="list-group-item">Album Photo</a>
      <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>foto.html" class="list-group-item">Galeri Photo</a>
      <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>kalender.html" class="list-group-item">Kalender Kegiatan</a>
      <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>banner.html" class="list-group-item">Banner</a>
      <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>random.html" class="list-group-item">Random Image</a>
      <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>randomatas.html" class="list-group-item">Random Image Atas</a>
      <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>video.html" class="list-group-item">Video</a>
      <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>link.html" class="list-group-item">Link</a>
      <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>kontak.html" class="list-group-item">Kontak Saya</a>
      <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>socialmedia.html" class="list-group-item">Social Media</a>
      <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>statistik.html" class="list-group-item">Statistik Website</a>
    </div>

    <div class="list-group">
    	<a href="#" class="list-group-item" style="color: white; font-weight: bold; background-color: #D9AF07!important">
        	Link Informasi
        </a>
        <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>kat_link_info.html" class="list-group-item">Kategori Link Informasi</a>
        <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>link_info.html" class="list-group-item">Link Informasi</a>
    </div>
</div>
