<?php
class content extends session
{
	function __contruct()
	{
		if(!parent::status_session)
			die('You cannot access the sistem');
	}
	
	private function show_data()
	{
	?>
    	<script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.bootstrap.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.min.js'; ?>"></script>
        <link href="<?php echo P_SLASH.P_JS; ?>datatable/css/jquery.dataTables.css" rel="stylesheet">
        <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable();
			} );
		</script>
    	<div><a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>content/tambah.html" class="btn btn-primary">Tambah Content</a></div><br>
		<table class="display table-responsive" id="example" width="100%">
        <thead>
        	<tr>
            	<th>CONTENT</th>
                <th>KATEGORI</th>
                <th>TANGGAL</th>
                <th>PUBLISH</th>
                <th>AKTIVITAS</th>
            </tr>
        </thead>
        <tbody>
     <?php
		$d = sql_db::sql_query("select judul, urutan, kode, waktu, publish, id_module from tbl_content order by waktu asc");
		while($d1 = sql_db::sql_fetchrow($d))
		{
		?>
        	<tr>
            	<td><?php echo $d1["judul"]; ?></td>
                <td>
					<?php
						$c = sql_db::sql_query("select nama, id_connect from tbl_module where id_module='".$d1["id_module"]."'");
						$c1 = sql_db::sql_fetchrow($c);
						
						echo $c1["nama"];
						
						if($c1["id_connect"] != '0')
						{
							$b = sql_db::sql_query("select nama from tbl_module where id_module='".$c1["id_connect"]."'");
							$b1 = sql_db::sql_fetchrow($b);
							
							echo ' <<< '.$b1["nama"];
						}
						
						
					?>
                </td>
                <td><?php echo tgl_indo($d1["waktu"]); ?></td>
                <td class="text-center"><?php echo $d1["publish"]; ?></td>                
                <td class="text-center">
                	<a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>content/<?php echo urlencode($d1["kode"]); ?>/edit.html">EDIT<a>&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>content/<?php echo urlencode($d1["kode"]); ?>/hapus.html" onClick="return confirm('Apakah Anda yakin ingin menghapus data ini?' +  '\n\n' + 'Jika YA silahkan klik OK, Jika TIDAK klik CANCEL.')">HAPUS<a>
                </td>
            </tr>
        <?php
		}
		echo '</tbody></table>';
	}
	
	private function show_form($heading, $kode)
	{
		if($heading == 'edit')
		{
			$d = sql_db::sql_query("select * from tbl_content where lower(kode)='".urldecode($kode)."'");
			$d1 = sql_db::sql_fetchrow($d);
		}
		else
		{
			$d1["kategori"] = '';
			$d1["judul"] = '';
			$d1["publish"] = '';
			$d1["intro"] = '';
			$d1["isi"] = '';
			$d1["id_module"] = '';
			$d1["waktu"] = date("Y-m-d H:i:s");
		}
		
		$_SESSION["KCFINDER"] = array(
			'disabled' => false,
		    'uploadURL' => "/userfiles/discapil_medan/",
		    'uploadDir' => "",
			'thumbWidth' => 150,
		    'thumbHeight' => 150
		);

		
	?>
    	<link type="text/css" href="<?php echo P_SLASH.P_JS; ?>datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" />		
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>datetimepicker/moment-with-locales.js"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>ckeditor/ckeditor.js"></script>
        <script type="text/javascript">
			$(function(){
				$("#datetimepicker1").datetimepicker({
					format:	'YYYY-MM-DD HH:mm:ss'
				});
				CKEDITOR.replace( 'intro' );
				CKEDITOR.replace( 'isi' );
			});
		</script>	
        <div align="right" style="margin-bottom:20px;">
			<a href="<?php echo P_SLASH; ?>admin/plugin/content.html" class="link_back">Kembali</a>	 					
        </div>
    	<form class="form-horizontal" action="<?php echo URI; ?>" method="post">
        	<input type="hidden" name="mode" value="<?php echo $kode; ?>">
        	<div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Kategori Utama : </label>
                <div class="col-sm-7">                	
					<select name="kategori" required="required" class="form-control">
                    	<option value="">Pilih Kategori </option>
                        <option value="0" <?php if($d1["id_module"] == '0') echo 'selected="selected"'; ?>>Tidak Ada Kategori</option>
                        <?php
							$c = sql_db::sql_query("select id_module, nama from tbl_module where jenis='2' and id_connect='0' order by nama asc");
							while($c1 = sql_db::sql_fetchrow($c))
							{								
								if($d1["id_module"] == $c1["id_module"])
									echo '<option value="'.$c1["id_module"].'" selected>'.$c1["nama"].'</option>';
								else
									echo '<option value="'.$c1["id_module"].'">'.$c1["nama"].'</option>';
								
								$b = sql_db::sql_query("select id_module, nama from tbl_module where id_connect='".$c1["id_module"]."' order by nama asc");
								while($b1 = sql_db::sql_fetchrow($b))
								{
									if($d1["id_module"] == $b1["id_module"])
										echo '<option value="'.$b1["id_module"].'" selected>-- '.$b1["nama"].'</option>';
									else
										echo '<option value="'.$b1["id_module"].'">-- '.$b1["nama"].'</option>';

									$a = sql_db::sql_query("select id_module, nama from tbl_module where id_connect='".$b1["id_module"]."' order by nama asc");
									while($a1 = sql_db::sql_fetchrow($a))
									{
										if($d1["id_module"] == $a1["id_module"])
											echo '<option value="'.$a1["id_module"].'" selected>---- '.$a1["nama"].'</option>';
										else
											echo '<option value="'.$a1["id_module"].'">---- '.$a1["nama"].'</option>';
									}
								}
							}		
						?>
                    </select>
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Judul : </label>
                <div class="col-sm-7">                	
					<input type="text" name="judul" class="form-control" required value="<?php echo $d1["judul"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Publish : </label>
                <div class="col-sm-7">                	
					<input type="radio" name="publish" value="N" <?php if($d1["publish"] == '' or $d1["publish"] == 'N') echo 'checked="checked"'; ?> /> Tidak&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="publish" value="Y" <?php if($d1["publish"] == 'Y') echo 'checked="checked"'; ?> /> Ya&nbsp;&nbsp;&nbsp;
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Tanggal : </label>
                <div class="col-sm-7">                	
					<input type="text" name="tanggal" class="form-control" id="datetimepicker1" required value="<?php echo $d1["waktu"]; ?>" />
                </div>
			</div>
            <div class="form-group">
            	<label class="col-sm-3 control-label"><span class="required">*</span>Intro Text : </label>
            </div>
            <div class="form-group">
            	<textarea name="intro" id="intro" required><?php echo $d1["intro"]; ?></textarea>
            </div>
            <div class="form-group">
            	<label class="col-sm-3 control-label"><span class="required">*</span>Full Text : </label>
            </div>
            <div class="form-group">
            	<textarea name="isi" id="isi" required><?php echo $d1["isi"]; ?></textarea>
            </div>
            <div class="form-group">
            	<label class="col-sm-3"></label>
                <div class="col-sm-7">
					<button class="btn btn-default" type="submit" name="submit_tambah">SIMPAN</button>
    	            <button class="btn btn-default" type="reset">RESET</button>
                </div>
			</div>
            
        </form>
    <?php
	}
	
	function heading($heading, $mode, $POST)
	{
		if($heading == 'tambah')
		{
			echo '<div class="panel-heading">Tambah Content</div>';
			echo '<div class="panel-body"">';
				if(isset($POST["mode"]))
					$this->tambah($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'edit')
		{
			echo '<div class="panel-heading">Edit Content</div>';
			echo '<div class="panel-body"">';
			if(isset($POST["mode"]))
					$this->edit($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'hapus')
		{
			echo '<div class="panel-heading">Hapus Content</div>';
			echo '<div class="panel-body"">';
			$this->hapus($mode);
			echo '</div>';
		}
		else
		{
			echo '<div class="panel-heading">Content</div>';
			echo '<div class="panel-body"">';
			$this->show_data();
			echo '</div>';
		}
		
	}
	
	function tambah($POST)
	{
		$kategori = anti($POST["kategori"]);
		$publish = anti($POST["publish"]);
		$judul = anti($POST["judul"]);
		$tanggal = anti($POST["tanggal"]);
		$intro = $POST["intro"];
		$isi = $POST["isi"];
		
		
		if(!$this->cek_form($kategori, $publish, $judul, $tanggal, $intro, $isi))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('tambah','');
		}
		else
		{					
			$d = sql_db::sql_query("insert into tbl_content (id_module, judul, waktu, publish, intro, isi, id_user, ip_address) values ('$kategori','$judul','$tanggal','$publish','$intro','$isi','','".$_SERVER["REMOTE_ADDR"]."')");
			

			$d = sql_db::sql_error($d);
			if(empty($d["message"]))
				berhasil(P_SLASH.P_AD.P_PLUGIN.'content/tambah.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'content/tambah.html');
		}
	}
	
	private function edit($POST)
	{
		$kategori = anti($POST["kategori"]);
		$publish = anti($POST["publish"]);
		$judul = anti($POST["judul"]);
		$tanggal = anti($POST["tanggal"]);
		$intro = $POST["intro"];
		$isi = $POST["isi"];
		$kode = anti($POST["mode"]);
		
		if(!$this->cek_form($kategori, $publish, $judul, $tanggal, $intro, $isi))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('edit','$kode');
		}
		else
		{					
			$d = sql_db::sql_query(" update  tbl_content set id_module='$kategori', judul='$judul', waktu='$tanggal', publish='$publish', intro='$intro', isi='$isi', id_user='', ip_address='".$_SERVER["REMOTE_ADDR"]."' where kode='$kode'");
						
			$d = sql_db::sql_error($d);
			if(empty($d["message"]))
				berhasil(P_SLASH.P_AD.P_PLUGIN.'content/'.$kode.'/edit.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'content/'.$kode.'/edit.html');
		}		
	}
	
	private function hapus($kode)
	{		
		$d = sql_db::sql_query("delete from tbl_content where kode='$kode'");

		$d = sql_db::sql_error($d);
		if(empty($d["message"]))
			berhasil(P_SLASH.P_AD.P_PLUGIN.'content.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'content.html');
	}
	
	private function cek_form($kategori, $publish, $judul, $tanggal, $intro, $isi)
	{
		if($kategori== '' or $publish == '' or $judul == '' or $tanggal == '' or $intro == '' or $isi == '')
			return false;
		else
			return true;
	}
}

$content = new content;
$opt = isset($_GET["opt"]) ? $_GET["opt"] : '';
$mode = isset($_GET["mode"]) ? $_GET["mode"] : '';

echo '<div class="panel panel-default" style="width:100%;">';
$content->heading(anti($opt), anti($mode), $_POST);
echo '</div>';
?>