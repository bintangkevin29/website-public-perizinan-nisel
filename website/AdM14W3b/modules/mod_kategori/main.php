<?php
class kategori extends session
{
	function __contruct()
	{
		if(!parent::status_session)
			die('You cannot access the sistem');
	}
	
	private function show_data()
	{
	?>
    	<script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.bootstrap.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.min.js'; ?>"></script>
        <link href="<?php echo P_SLASH.P_JS; ?>datatable/css/jquery.dataTables.css" rel="stylesheet">
        <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable();
			} );
		</script>
    	<div><a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>kategori/tambah.html" class="btn btn-primary">Tambah Kategori</a></div><br>
		<table class="display table-responsive" id="example" width="100%">
        <thead>
        	<tr>
            	<th>KATEGORI</th>
                <th>TANGGAL</th>
                <th>PUBLISH</th>
                <th>MENU</th>
                <th>URUTAN</th>
                <th>AKTIVITAS</th>
            </tr>
        </thead>
        <tbody>
     <?php
		$d = sql_db::sql_query("select nama, urutan, kode, waktu, ismenu, publish from tbl_module where jenis='2' order by nama asc");
		while($d1 = sql_db::sql_fetchrow($d))
		{
		?>
        	<tr>
            	<td><?php echo $d1["nama"]; ?></td>
                <td><?php echo tgl_indo($d1["waktu"]); ?></td>
                <td class="text-center"><?php echo $d1["publish"]; ?></td>
                <td class="text-center"><?php echo $d1["ismenu"]; ?></td>
                <td class="text-center"><?php echo $d1["urutan"]; ?></td>
                <td class="text-center">
                	<a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>kategori/<?php echo urlencode($d1["kode"]); ?>/edit.html">EDIT<a>&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>kategori/<?php echo urlencode($d1["kode"]); ?>/hapus.html" onClick="return confirm('Apakah Anda yakin ingin menghapus data ini?' +  '\n\n' + 'Jika YA silahkan klik OK, Jika TIDAK klik CANCEL.')">HAPUS<a>
                </td>
            </tr>
        <?php
		}
		echo '</tbody></table>';
	}
	
	private function show_form($heading, $kode)
	{
		if($heading == 'edit')
		{
			$d = sql_db::sql_query("select * from tbl_module where lower(kode)='".urldecode($kode)."'");
			$d1 = sql_db::sql_fetchrow($d);
		}
		else
		{
			$d1["nama"] = '';
			$d1["publish"] = '';
			$d1["id_connect"] = '';
			$d1["ismenu"] = '';
		}
		
	?>
    	<script type="text/javascript">
			$(function(){
				$("#ismenu").click(function(e) {
					if($("#urutan").html() != '')
	                    $("#urutan").hide("slow");
						
					$("#urutan1").show("slow");
                });
				
				$("#ismenu1").click(function(e) {
					if($("#urutan1").html() != '')
	                    $("#urutan1").hide("slow");
						
                    $("#urutan").show("slow");
                });
				
				$("#kategori_utama").change(function(e) {
                    if($(this).val() == '0')
						$("#menu").show("slow");
					else
					{
						$("#menu").hide("slow");
						$("#urutan").hide("slow");
					}
                });
			});
		</script>
        <div align="right" style="margin-bottom:20px;">
			<a href="<?php echo P_SLASH; ?>admin/plugin/kategori.html" class="link_back">Kembali</a>	 					
        </div>
    	<form class="form-horizontal" action="<?php echo URI; ?>" method="post">
        	<input type="hidden" name="mode" value="<?php echo $kode; ?>">
        	<div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Kategori Utama : </label>
                <div class="col-sm-7">                	
					<select name="kategori_utama" id="kategori_utama" required="required" class="form-control">
                    	<option value="">Pilih Kategori Utama</option>
                        <option value="0" <?php if($d1["id_connect"] == '0') echo 'selected="selected"'; ?>>Tidak Ada Kategori Utama</option>
                        <?php
							$c = sql_db::sql_query("select id_module, nama from tbl_module where jenis='2' and id_connect='0' order by nama asc");
							while($c1 = sql_db::sql_fetchrow($c))
							{
								if($d1["id_connect"] == $c1["id_module"])
									echo '<option value="'.$c1["id_module"].'" selected>'.$c1["nama"].'</option>';
								else
									echo '<option value="'.$c1["id_module"].'">'.$c1["nama"].'</option>';


								$b = sql_db::sql_query("select id_module, nama from tbl_module where id_connect='".$c1["id_module"]."'");
								if(sql_db::sql_numrows($b)>0)
								{
									while($b1 = sql_db::sql_fetchrow($b))
									{
										if($d1["id_connect"] == $b1["id_module"])
											echo '<option value="'.$b1["id_module"].'" selected>--'.$b1["nama"].'</option>';
										else
											echo '<option value="'.$b1["id_module"].'">--'.$b1["nama"].'</option>';
									}
								}								
							}		
						?>
                    </select>
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Kategori : </label>
                <div class="col-sm-7">                	
					<input type="text" name="kategori" class="form-control" required="required" value="<?php echo $d1["nama"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Publish : </label>
                <div class="col-sm-7">                	
					<input type="radio" name="publish" value="N" <?php if($d1["publish"] == '' or $d1["publish"] == 'N') echo 'checked="checked"'; ?> /> Tidak&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="publish" value="Y" <?php if($d1["publish"] == 'Y') echo 'checked="checked"'; ?> /> Ya&nbsp;&nbsp;&nbsp;
                </div>
			</div>
            <div class="form-group" id="menu" <?php if($d1["id_connect"]=='' or $d1["id_connect"] <> '0') echo 'style="display:none;"'; ?>>
				<label class="col-sm-3 control-label"><span class="required">*</span>Diletakkan Di Menu : </label>
                <div class="col-sm-7">                	
					<input type="radio" id="ismenu" name="menu" value="N" <?php if($d1["ismenu"] == 'N') echo 'checked="checked"'; ?> /> Tidak&nbsp;&nbsp;&nbsp;
                    <input type="radio" id="ismenu1" name="menu" value="Y" <?php if($d1["ismenu"] == 'Y') echo 'checked="checked"'; ?> /> Ya&nbsp;&nbsp;&nbsp;
                </div>
			</div>
            <div id="urutan1" class="form-group" <?php if($d1["ismenu"]=='' or $d1["ismenu"] == 'Y') echo 'style="display:none;"'; ?>>
				<label class="col-sm-3 control-label"><span class="required">*</span>Urutan : </label>
                <div class="col-sm-7">                	
					<select name="urutan1" required="required" class="form-control">
                    	<option value="0">Pilih Urutan</option>
                        <?php
							$c = sql_db::sql_query("select id_module, nama, urutan from tbl_module where ismenu='N' and (jenis='1' or jenis='2') and id_connect='0' order by urutan asc");
							while($c1 = sql_db::sql_fetchrow($c))
							{
								if($d1["urutan"] == $c1["urutan"])
									echo '<option value="'.$c1["urutan"].'" selected>'.$c1["urutan"].' - '.$c1["nama"].'</option>';
								else
									echo '<option value="'.$c1["urutan"].'">'.$c1["urutan"].' - '.$c1["nama"].'</option>';
								$urutan = $c1["urutan"];
							}
							echo '<option value="'.($urutan+1).'">** '.($urutan+1).' - Paling Bawah **</option>';
						?>
                    </select>
                </div>                
			</div>
            <div id="urutan" class="form-group" <?php if($d1["ismenu"]=='' or $d1["ismenu"] == 'N') echo 'style="display:none;"'; ?>>
				<label class="col-sm-3 control-label"><span class="required">*</span>Urutan : </label>
                <div class="col-sm-7">                	
					<select name="urutan" required="required" class="form-control">
                    	<option value="0">Pilih Urutan</option>
                        <?php
							$c = sql_db::sql_query("select id_module, nama, urutan from tbl_module where ismenu='Y' order by urutan asc");
							while($c1 = sql_db::sql_fetchrow($c))
							{
								if($d1["urutan"] == $c1["urutan"])
									echo '<option value="'.$c1["urutan"].'" selected>'.$c1["urutan"].' - '.$c1["nama"].'</option>';
								else
									echo '<option value="'.$c1["urutan"].'">'.$c1["urutan"].' - '.$c1["nama"].'</option>';
								$urutan = $c1["urutan"];
							}
							echo '<option value="'.($urutan+1).'">** '.($urutan+1).' - Paling Kanan **</option>';
						?>
                    </select>
                </div>                
			</div>
            <div class="form-group">
            	<label class="col-sm-3"></label>
                <div class="col-sm-7">
					<button class="btn btn-default" type="submit" name="submit_tambah">SIMPAN</button>
    	            <button class="btn btn-default" type="reset">RESET</button>
                </div>
			</div>
        </form>
    <?php
	}
	
	function heading($heading, $mode, $POST)
	{
		if($heading == 'tambah')
		{
			echo '<div class="panel-heading">Tambah Kategori</div>';
			echo '<div class="panel-body"">';
				if(isset($POST["mode"]))
					$this->tambah($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'edit')
		{
			echo '<div class="panel-heading">Edit Kategori</div>';
			echo '<div class="panel-body"">';
			if(isset($POST["mode"]))
					$this->edit($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'hapus')
		{
			echo '<div class="panel-heading">Hapus Kategori</div>';
			echo '<div class="panel-body"">';
			$this->hapus($mode);
			echo '</div>';
		}
		else
		{
			echo '<div class="panel-heading">Kategori</div>';
			echo '<div class="panel-body"">';
			$this->show_data();
			echo '</div>';
		}
		
	}
	
	function tambah($POST)
	{
		$kategori_utama = anti($POST["kategori_utama"]);
		$kategori = anti($POST["kategori"]);
		$publish = anti($POST["publish"]);
		$menu = anti($POST["menu"]);
		$urutan = anti($POST["urutan"]);
		$urutan1 = anti($POST["urutan1"]);
		
		if(!$this->cek_form($kategori_utama, $kategori, $publish))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('tambah','');
		}
		else
		{
			if($menu == 'N')
				$urutan = $urutan1;
				
			if($kategori_utama != 0)
			{
				$menu = 'N';
				$urutan = 0;
			}
			elseif($urutan != 0)
				$d = sql_db::sql_query("update tbl_module set urutan=urutan+1 where ismenu='$menu' and urutan>='$urutan' and id_connect='0'");
			
			$d = sql_db::sql_query("insert into tbl_module (id_connect, nama, publish, ismenu, urutan, jenis) values ('$kategori_utama','$kategori','$publish','$menu','$urutan','2')");			

			
			if($d)
				berhasil(P_SLASH.P_AD.P_PLUGIN.'kategori/tambah.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'kategori/tambah.html');
		}
	}
	
	private function edit($POST)
	{
		$kategori_utama = anti($POST["kategori_utama"]);
		$kategori = anti($POST["kategori"]);
		$publish = anti($POST["publish"]);
		$menu = anti($POST["menu"]);
		$urutan = anti($POST["urutan"]);
		$urutan1 = anti($POST["urutan1"]);
		$kode = anti($POST["mode"]);
		
		if($menu == 'N')
			$urutan = $urutan1;
		
		if($kategori_utama != 0)
		{
				$menu = 'N';
				$urutan = 0;
		}

		$d = sql_db::sql_query("select urutan, ismenu from tbl_module where kode='$kode'");
		$d1 = sql_db::sql_fetchrow($d);
		
		if($urutan != $d1["urutan"] or $menu != $d1["ismenu"])
		{
			if($menu == $d1["ismenu"])
			{
				if($urutan == 0)
					$c = sql_db::sql_query("update tbl_module set urutan=urutan-1 where urutan > '".$d1["urutan"]."' and ismenu='$menu'");
				else
				{
					if($d1["urutan"] != 0)
						$c = sql_db::sql_query("update tbl_module set urutan=urutan-1 where urutan > '".$d1["urutan"]."' and ismenu='$menu'");
					$c = sql_db::sql_query("update tbl_module set urutan=urutan+1 where urutan >= '$urutan' and ismenu='$menu'");
				}
			}
			else
			{
				if($urutan == 0)
					$c = sql_db::sql_query("update tbl_module set urutan=urutan-1 where urutan > '".$d1["urutan"]."' and ismenu='".$d1["ismenu"]."'");
				else
				{
					if($d1["urutan"] != 0)
						$c = sql_db::sql_query("update tbl_module set urutan=urutan-1 where urutan > '".$d1["urutan"]."' and ismenu='".$d1["ismenu"]."'");
					$c = sql_db::sql_query("update tbl_module set urutan=urutan+1 where urutan >= '$urutan' and ismenu='$menu'");
				}
			}
		}
		
		$d = sql_db::sql_query("update tbl_module set nama='$kategori', publish='$publish', ismenu='$menu', id_connect='$kategori_utama', urutan='$urutan' where kode='$kode'");
		
		if($d)
			berhasil(P_SLASH.P_AD.P_PLUGIN.'kategori/'.$kode.'/edit.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'kategori/'.$kode.'/edit.html');
	}
	
	private function hapus($kode)
	{
		$d = sql_db::sql_query("select ismenu, urutan from tbl_module where kode='$kode'");
		$d1 = sql_db::sql_fetchrow($d);
		
		if($d1["ismenu"] == 'Y' and $d1["urutan"] != 0)
			sql_db::sql_query("update tbl_module set urutan=urutan-1 where urutan>'".$d1["urutan"]."'");
		
		$d = sql_db::sql_query("delete from tbl_module where kode='$kode'");
		if($d)
			berhasil(P_SLASH.P_AD.P_PLUGIN.'kategori.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'kategori.html');
	}
	
	private function cek_form($kategori_utama, $kategori, $publish)
	{
		if($kategori_utama == '' and $kategori == '' and $publish == '')
			return false;
		else
			return true;
	}
}

$kategori = new kategori;
$opt = isset($_GET["opt"]) ? $_GET["opt"] : '';
$mode = isset($_GET["mode"]) ? $_GET["mode"] : '';

echo '<div class="panel panel-default" style="width:100%;">';
$kategori->heading(anti($opt), anti($mode), $_POST);
echo '</div>';
?>