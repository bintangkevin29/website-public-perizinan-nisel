<?php
class link_info extends session
{
	function __contruct()
	{
		if(!parent::status_session)
			die('You cannot access the sistem');
	}
	
	private function show_data()
	{
	?>
    	<script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.bootstrap.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.min.js'; ?>"></script>
        <link href="<?php echo P_SLASH.P_JS; ?>datatable/css/jquery.dataTables.css" rel="stylesheet">
        <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable();
			} );
		</script>
    	<div><a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>link_info/tambah.html" class="btn btn-primary">Tambah Link Informasi</a></div><br>
        <div>
        	<form action="<?php echo URI; ?>" method="post">
        	Kategori Link Informasi :
            <select name="filter_link_info" onchange="this.form.submit();">
            	<?php
					$id_kat_link_info = isset($_POST["filter_link_info"])? anti($_POST["filter_link_info"]):'0';
					$id = 1;
					$d = sql_db::sql_query("select a.id_kat_link_info, a.kategori, b.jenis from tbl_kat_link_info a inner join tbl_jenis_link b on a.id_jenis=b.id_jenis order by a.id_jenis asc, a.kategori asc");
					while($d1 = sql_db::sql_fetchrow($d))
					{											
						if($d1["id_kat_link_info"] == $id_kat_link_info or ($id==1 and $id_kat_link_info==0))
						{
							echo '<option value="'.$d1["id_kat_link_info"].'" selected="selected">'.$d1["jenis"].' - '.$d1["kategori"].'</option>';
							$id_kat_link_info = $d1["id_kat_link_info"];
						}
						else
							echo '<option value="'.$d1["id_kat_link_info"].'">'.$d1["jenis"].' - '.$d1["kategori"].'</option>';
							$id++;
					}
				?>
            </select>
            </form>
        </div>
		<table class="display" id="example" width="100%">
        <thead>
        	<tr>
            	<th>JENIS LINK INFORMASI</th>
                <th>KATEGORI</th>                
                <th>NAMA</th>
                <th>PUBLISH</th>                
                <th>LINK</th>
                <th>AKTIVITAS</th>
            </tr>
        </thead>
        <tbody>
     <?php
		$d = sql_db::sql_query("select a.nama, a.link, a.publish, b.kategori, c.jenis, a.kode from tbl_link_info a inner join tbl_kat_link_info b on a.id_kat_link_info=b.id_kat_link_info inner join tbl_jenis_link c on c.id_jenis=b.id_jenis");
		while($d1 = sql_db::sql_fetchrow($d))
		{			
		?>
        	<tr>
            	<td><?php echo $d1["jenis"]; ?></td>
                <td><?php echo $d1["kategori"]; ?></td>
                <td><?php echo $d1["nama"]; ?></td>
                <td class="text-center"><?php echo $d1["publish"]; ?></td>                
                <td class="text-center"><?php echo $d1["link"]; ?></a></td>
                <td class="text-center">
                	<a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>link_info/<?php echo urlencode($d1["kode"]); ?>/edit.html">EDIT<a>&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>link_info/<?php echo urlencode($d1["kode"]); ?>/hapus.html" onClick="return confirm('Apakah Anda yakin ingin menghapus data ini?' +  '\n\n' + 'Jika YA silahkan klik OK, Jika TIDAK klik CANCEL.')">HAPUS<a>
                </td>
            </tr>
        <?php
		}
		echo '</tbody></table>';
	}
	
	private function show_form($heading, $kode)
	{
		if($heading == 'edit')
		{
			$d = sql_db::sql_query("select * from tbl_link_info where lower(kode)='".urldecode($kode)."'");
			$d1 = sql_db::sql_fetchrow($d);
		}
		else
		{
			$d1["nama"] = '';
			$d1["id_kat_link_info"] = '';
			$d1["publish"] = '';
			$d1["link"] = '';
		}
		
		$_SESSION["KCFINDER"] = array(
			'disabled' => false,
		    'uploadURL' => "/userfiles/wahyu_wahab/file_link_info/",
		    'uploadDir' => "",
			'thumbWidth' => 200,
		    'thumbHeight' => 200
		);
	?>
    	<link type="text/css" href="<?php echo P_SLASH.P_JS; ?>datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" />		
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>datetimepicker/moment-with-locales.js"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript">
			$(function(){
				$("#datetimepicker1").datetimepicker({
					format:	'YYYY-MM-DD HH:mm:ss'
				});
			});
		</script>
        <script type="text/javascript">
			
			function openKCFinderFile(field) {
				window.KCFinder = {
					callBack: function(url) {
						field.value = url;
						window.KCFinder = null;
					}
				};
				window.open('<?php echo P_SLASH; ?>js/kcfinder/browse.php?type=files&dir=files/public', 'kcfinder_textbox',
					'status=0, toolbar=0, location=0, menubar=0, directories=0, ' +
					'resizable=1, scrollbars=0, width=800, height=600'
				);
			}
			</script>
		<div align="right" style="margin-bottom:20px;">
			<a href="<?php echo P_SLASH; ?>admin/plugin/link_info.html" class="link_back">Kembali</a>	 					
        </div> 					
    	<form class="form-horizontal" action="<?php echo URI; ?>" method="post">
        	<input type="hidden" name="mode" value="<?php echo $kode; ?>">
        	<div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Kategori : </label>
                <div class="col-sm-7">                	
					<select name="kat_link_info" required="required" class="form-control">
                    	<option value="">Pilih Kategori Link</option>
                        <?php
							$c = sql_db::sql_query("select a.id_kat_link_info, a.kategori, b.jenis from tbl_kat_link_info a inner join tbl_jenis_link b on a.id_jenis=b.id_jenis order by a.id_jenis asc, a.kategori asc");
							while($c1 = sql_db::sql_fetchrow($c))
							{													
								if($d1["id_kat_link_info"] == $c1["id_kat_link_info"])
									echo '<option value="'.$c1["id_kat_link_info"].'" selected>'.$c1["jenis"].' - '.$c1["kategori"].'</option>';
								else
									echo '<option value="'.$c1["id_kat_link_info"].'">'.$c1["jenis"].' - '.$c1["kategori"].'</option>';								
							}		
						?>
                    </select>
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Nama Link : </label>
                <div class="col-sm-7">                	
					<input type="text" name="nama" class="form-control" required="required" value="<?php echo $d1["nama"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Link : </label>
                <div class="col-sm-7">                	
					<input type="text" name="link" class="form-control" required="required" value="<?php echo $d1["link"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Publish : </label>
                <div class="col-sm-7">                	
					<input type="radio" name="publish" value="N" <?php if($d1["publish"] == '' or $d1["publish"] == 'N') echo 'checked="checked"'; ?> /> Tidak&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="publish" value="Y" <?php if($d1["publish"] == 'Y') echo 'checked="checked"'; ?> /> Ya&nbsp;&nbsp;&nbsp;
                </div>
			</div>
            <div class="form-group">
            	<label class="col-sm-3"></label>
                <div class="col-sm-7">
					<button class="btn btn-default" type="submit" name="submit_tambah">SIMPAN</button>
    	            <button class="btn btn-default" type="reset">RESET</button>
                </div>
			</div>
            
        </form>
    <?php
	}
	
	function heading($heading, $mode, $POST)
	{
		if($heading == 'tambah')
		{
			echo '<div class="panel-heading">Tambah Link Informasi</div>';
			echo '<div class="panel-body"">';
				if(isset($POST["mode"]))
					$this->tambah($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'edit')
		{
			echo '<div class="panel-heading">Edit Link Informasi</div>';
			echo '<div class="panel-body"">';
			if(isset($POST["mode"]))
					$this->edit($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'hapus')
		{
			echo '<div class="panel-heading">Hapus Link Informasi</div>';
			echo '<div class="panel-body"">';
			$this->hapus($mode);
			echo '</div>';
		}
		else
		{
			echo '<div class="panel-heading">Link Informasi</div>';
			echo '<div class="panel-body"">';
			$this->show_data();
			echo '</div>';
		}
	}

	function tambah($POST)
	{
		$link = anti($POST["link"]);
		$publish = anti($POST["publish"]);
		$kat_link_info = anti($POST["kat_link_info"]);
		$nama = anti($POST["nama"]);

		
	
		if(!$this->cek_form($publish, $kat_link_info, $nama, $link))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('tambah','');
		}
		else
		{					
			$d = sql_db::sql_query("insert into tbl_link_info (id_kat_link_info, nama, link, publish) values ('$kat_link_info','$nama','$link','$publish')");
			

			if($d)
				berhasil(P_SLASH.P_AD.P_PLUGIN.'link_info/tambah.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'link_info/tambah.html');
		}
	}

	private function edit($POST)
	{
		$link = anti($POST["link"]);
		$publish = anti($POST["publish"]);
		$kat_link_info = anti($POST["kat_link_info"]);
		$nama = anti($POST["nama"]);
		$kode = anti($POST["mode"]);
	

		if(!$this->cek_form( $publish, $kat_link_info, $nama, $link))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('edit','$kode');
		}
		else
		{					
			$d = sql_db::sql_query(" update  tbl_link_info set id_kat_link_info='$kat_link_info', nama='$nama', link='$link', publish='$publish' where kode='$kode'");


			if($d)
				berhasil(P_SLASH.P_AD.P_PLUGIN.'link_info/'.$kode.'/edit.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'link_info/'.$kode.'/edit.html');
		}		
	}

	
	private function hapus($kode)
	{		
		$d = sql_db::sql_query("delete from tbl_link_info where kode='$kode'");
		if($d)
			berhasil(P_SLASH.P_AD.P_PLUGIN.'link_info.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'link_info.html');
	}

	private function cek_form($publish, $kat_link_info, $tanggal, $file)
	{
		if($publish == '' or $kat_link_info == '' or $tanggal == '' or $file== '')
			return false;
		else
			return true;
	}
}

$link_info = new link_info;
$opt = isset($_GET["opt"]) ? $_GET["opt"] : '';
$mode = isset($_GET["mode"]) ? $_GET["mode"] : '';

echo '<div class="panel panel-default" style="width:100%;">';
$link_info->heading(anti($opt), anti($mode), $_POST);
echo '</div>';
?>