<?php
class polling extends session
{
	function __contruct()
	{
		if(!parent::status_session)
			die('You cannot access the sistem');
	}
	
	private function show_data()
	{
	?>
    	<script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.bootstrap.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.min.js'; ?>"></script>
        <link href="<?php echo P_SLASH.P_JS; ?>datatable/css/jquery.dataTables.css" rel="stylesheet">
        <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable();
			} );
		</script>
    	<div><a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>polling/tambah.html" class="btn btn-primary">Tambah Polling</a></div><br>
		<table class="display table-responsive" id="example" width="100%">
        <thead>
        	<tr>
            	<th>KATEGORI POLIING</th>
                <th>POLLING</th>
                <th>PUBLISH</th>
                <th>URUTAN</th>
                <th>AKTIVITAS</th>
            </tr>
        </thead>
        <tbody>
     <?php
		$d = sql_db::sql_query("select a.kode, a.polling, a.publish, b.kat_polling, a.urutan from tbl_polling a left join tbl_kat_polling b on a.id_kat_polling=b.id_kat_polling order by a.id_kat_polling asc, a.urutan asc");
		while($d1 = sql_db::sql_fetchrow($d))
		{
		?>
        	<tr>
            	<td><?php echo $d1["kat_polling"]; ?></td>
                <td><?php echo $d1["polling"]; ?></td>
                <td class="text-center"><?php echo $d1["publish"]; ?></td>
                <td class="text-center"><?php echo $d1["urutan"]; ?></td>
                <td class="text-center">
                	<a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>polling/<?php echo urlencode($d1["kode"]); ?>/edit.html">EDIT<a>&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>polling/<?php echo urlencode($d1["kode"]); ?>/hapus.html" onClick="return confirm('Apakah Anda yakin ingin menghapus data ini?' +  '\n\n' + 'Jika YA silahkan klik OK, Jika TIDAK klik CANCEL.')">HAPUS<a>
                </td>
            </tr>
        <?php
		}
		echo '</tbody></table>';
	}
	
	private function show_form($heading, $kode)
	{
		if($heading == 'edit')
		{
			$d = sql_db::sql_query("select * from tbl_polling where lower(kode)='".urldecode($kode)."'");
			$d1 = sql_db::sql_fetchrow($d);
		}
		else
		{
			$d1["id_kat_polling"] = '';
			$d1["polling"] = '';
			$d1["urutan"] = '';
			$d1["publish"] = '';
		}
		
	?>  
    	<div align="right" style="margin-bottom:20px;">
			<a href="<?php echo P_SLASH; ?>admin/plugin/polling.html" class="link_back">Kembali</a>	 					
        </div>  			
    	<form class="form-horizontal" action="<?php echo URI; ?>" method="post">
        	<input type="hidden" name="mode" value="<?php echo $kode; ?>">
        	<div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Kategori Polling : </label>
                <div class="col-sm-7">                	
					<select name="kat_polling" id="kat_polling" required="required" class="form-control">
                    	<option value="">Pilih Kategori Polling</option>
                        <?php
							$c = sql_db::sql_query("select id_kat_polling, kat_polling from tbl_kat_polling order by kat_polling asc");
							while($c1 = sql_db::sql_fetchrow($c))
							{
								if($d1["id_kat_polling"] == $c1["id_kat_polling"])
									echo '<option value="'.$c1["id_kat_polling"].'" selected>'.$c1["kat_polling"].'</option>';
								else
									echo '<option value="'.$c1["id_kat_polling"].'">'.$c1["kat_polling"].'</option>';
							}		
						?>
                    </select>
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Polling : </label>
                <div class="col-sm-7">                	
					<input type="text" name="polling" class="form-control" required="required" value="<?php echo $d1["polling"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Publish : </label>
                <div class="col-sm-7">                	
					<input type="radio" name="publish" value="N" <?php if($d1["publish"] == '' or $d1["publish"] == 'N') echo 'checked="checked"'; ?> /> Tidak&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="publish" value="Y" <?php if($d1["publish"] == 'Y') echo 'checked="checked"'; ?> /> Ya&nbsp;&nbsp;&nbsp;
                </div>
			</div>            
            <div id="urutan" class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Urutan : </label>
                <div class="col-sm-7">                	
					<select name="urutan" required="required" class="form-control">
                    	<option value="0">Pilih Urutan</option>
                        <?php
							for($i=1; $i<=8; $i++)
							{
								if($d1["urutan"] == $i)
									echo '<option value="'.$i.'" selected="selected">Urutan Ke-'.$i.'</option>';
								else
									echo '<option value="'.$i.'">Urutan Ke-'.$i.'</option>';
									
							}							
						?>
                    </select>
                </div>                
			</div>
            <div class="form-group">
            	<label class="col-sm-3"></label>
                <div class="col-sm-7">
					<button class="btn btn-default" type="submit" name="submit_tambah">SIMPAN</button>
    	            <button class="btn btn-default" type="reset">RESET</button>
                </div>
			</div>
        </form>
    <?php
	}
	
	function heading($heading, $mode, $POST)
	{
		if($heading == 'tambah')
		{
			echo '<div class="panel-heading">Tambah Polling</div>';
			echo '<div class="panel-body"">';
				if(isset($POST["mode"]))
					$this->tambah($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'edit')
		{
			echo '<div class="panel-heading">Edit Polling</div>';
			echo '<div class="panel-body"">';
			if(isset($POST["mode"]))
					$this->edit($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'hapus')
		{
			echo '<div class="panel-heading">Hapus Polling</div>';
			echo '<div class="panel-body"">';
			$this->hapus($mode);
			echo '</div>';
		}
		else
		{
			echo '<div class="panel-heading">Polling</div>';
			echo '<div class="panel-body"">';
			$this->show_data();
			echo '</div>';
		}
		
	}
	
	function tambah($POST)
	{
		$kat_polling = anti($POST["kat_polling"]);
		$polling = anti($POST["polling"]);
		$publish = anti($POST["publish"]);
		$urutan = anti($POST["urutan"]);
		
		if(!$this->cek_form($kat_polling, $polling, $publish, $urutan))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('tambah','');
		}
		else
		{						
			$d = sql_db::sql_query("insert into tbl_polling (id_kat_polling, polling, publish, urutan) values ('$kat_polling','$polling','$publish','$urutan')");
						
			if($d)
				berhasil(P_SLASH.P_AD.P_PLUGIN.'polling/tambah.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'polling/tambah.html');
		}
	}
	
	private function edit($POST)
	{
		$kat_polling = anti($POST["kat_polling"]);
		$polling = anti($POST["polling"]);
		$publish = anti($POST["publish"]);
		$urutan = anti($POST["urutan"]);
		$kode = anti($POST["mode"]);
		
		
		$d = sql_db::sql_query("update tbl_polling set polling='$polling', publish='$publish', id_kat_polling='$kat_polling', urutan='$urutan' where kode='$kode'");
		
		if($d)
			berhasil(P_SLASH.P_AD.P_PLUGIN.'polling/'.$kode.'/edit.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'polling/'.$kode.'/edit.html');
	}
	
	private function hapus($kode)
	{		
		sql_db::sql_query("update tbl_polling set urutan=urutan-1 where urutan>'".$d1["urutan"]."'");
		
		$d = sql_db::sql_query("delete from tbl_polling where kode='$kode'");
		if($d)
			berhasil(P_SLASH.P_AD.P_PLUGIN.'polling.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'polling.html');
	}
	
	private function cek_form($kat_polling, $polling, $publish, $urutan)
	{
		if($kat_polling == '' and $polling == '' and $publish == '' and $urutan == '')
			return false;
		else
			return true;
	}
}

$polling = new polling;
$opt = isset($_GET["opt"]) ? $_GET["opt"] : '';
$mode = isset($_GET["mode"]) ? $_GET["mode"] : '';

echo '<div class="panel panel-default" style="width:100%;">';
$polling->heading(anti($opt), anti($mode), $_POST);
echo '</div>';
?>