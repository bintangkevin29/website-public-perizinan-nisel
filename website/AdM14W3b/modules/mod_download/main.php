<?php
class download extends session
{
	function __contruct()
	{
		if(!parent::status_session)
			die('You cannot access the sistem');
	}
	
	private function show_data()
	{
	?>
    	<script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.bootstrap.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.min.js'; ?>"></script>
        <link href="<?php echo P_SLASH.P_JS; ?>datatable/css/jquery.dataTables.css" rel="stylesheet">
        <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable();
			} );
		</script>
    	<div><a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>download/tambah.html" class="btn btn-primary">Tambah File Download</a></div><br>
        <div>
        	<form action="<?php echo URI; ?>" method="post">
        	Kategori Download :
            <select name="filter_download" onchange="this.form.submit();">
            	<?php
					$id_kat_download = isset($_POST["filter_download"])? anti($_POST["filter_download"]):'0';
					$id = 1;
					$d = sql_db::sql_query("select id_kat_download, nama, id_connect from tbl_kat_download order by id_connect asc, nama asc");
					while($d1 = sql_db::sql_fetchrow($d))
					{
						$kat_download='';
						if($d1["id_connect"] <> '0')
						{
							$c = sql_db::sql_query("select nama from tbl_kat_download where id_module='".$d1["id_connect"]."'");
							$c1 = sql_db::sql_fetchrow($c);
							$kat_download = $c1["nama"];
						}
						
						if($d1["id_kat_download"] == $id_kat_download or ($id==1 and $id_kat_download==0))
						{
							echo '<option value="'.$d1["id_kat_download"].'" selected="selected">'.$kat_download.' - '.$d1["nama"].'</option>';
							$id_kat_download = $d1["id_kat_download"];
					}
					else
							echo '<option value="'.$d1["id_kat_download"].'">'.$kat_download.' - '.$d1["nama"].'</option>';
						$id++;
					}
				?>
            </select>
            </form>
        </div>
		<table class="display" id="example" width="100%">
        <thead>
        	<tr>
            	<th>KATEGORI UTAMA</th>
                <th>KATEGORI</th>
                <th>FILE DOWNLOAD</th>
                <th>TANGGAL</th>
                <th>PUBLISH</th>                
                <th>DOWNLOAD</th>
                <th>AKTIVITAS</th>
            </tr>
        </thead>
        <tbody>
     <?php
		$d = sql_db::sql_query("select a.nama as download, b.nama as kat_download, a.waktu, a.publish, a.gambar, a.nama_file, b.id_connect, a.kode from tbl_download a inner join tbl_kat_download b on a.id_kat_download=b.id_kat_download order by a.nama asc, b.nama asc");
		while($d1 = sql_db::sql_fetchrow($d))
		{
			if($d1["id_connect"] <> '0')
			{
				$c = sql_db::sql_query("select nama from tbl_kat_download where id_kat_download='".$d1["id_connect"]."'");
				$c1 = sql_db::sql_fetchrow($c);
				$kat_download = $c1["nama"];
			}
			else
				$kat_download = $d1["kat_download"];
		?>
        	<tr>
            	<td><?php echo $kat_download; ?></td>
                <td><?php echo $d1["kat_download"]; ?></td>
                <td><?php echo $d1["download"]; ?></td>
                <td><?php echo tgl_indo($d1["waktu"]); ?></td>
                <td class="text-center"><?php echo $d1["publish"]; ?></td>                
                <td class="text-center"><a target="_blank" href="<?php echo P_SERVERNAME.$d1["nama_file"]; ?>" />DOWNLOAD</a></td>
                <td class="text-center">
                	<a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>download/<?php echo urlencode($d1["kode"]); ?>/edit.html">EDIT<a>&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>download/<?php echo urlencode($d1["kode"]); ?>/hapus.html" onClick="return confirm('Apakah Anda yakin ingin menghapus data ini?' +  '\n\n' + 'Jika YA silahkan klik OK, Jika TIDAK klik CANCEL.')">HAPUS<a>
                </td>
            </tr>
        <?php
		}
		echo '</tbody></table>';
	}
	
	private function show_form($heading, $kode)
	{
		if($heading == 'edit')
		{
			$d = sql_db::sql_query("select * from tbl_download where lower(kode)='".urldecode($kode)."'");
			$d1 = sql_db::sql_fetchrow($d);
		}
		else
		{
			$d1["nama"] = '';
			$d1["id_kat_download"] = '';
			$d1["publish"] = '';
			$d1["file"] = '';
			$d1["nama_file"] = '';
			$d1["gambar"] = '';
			$d1["waktu"] = date("Y-m-d H:i:s");
		}
		
		$_SESSION["KCFINDER"] = array(
			'disabled' => false,
		    'uploadURL' => "/userfiles/".P_NAME."/file_download/",
		    'uploadDir' => "",
			'thumbWidth' => 200,
		    'thumbHeight' => 200
		);
	?>
    	<link type="text/css" href="<?php echo P_SLASH.P_JS; ?>datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" />		
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>datetimepicker/moment-with-locales.js"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript">
			$(function(){
				$("#datetimepicker1").datetimepicker({
					format:	'YYYY-MM-DD HH:mm:ss'
				});
			});
		</script>
        <script type="text/javascript">
			
			function openKCFinderFile(field) {
				window.KCFinder = {
					callBack: function(url) {
						field.value = url;
						window.KCFinder = null;
					}
				};
				window.open('<?php echo P_SLASH; ?>js/kcfinder/browse.php?type=files&dir=files/public', 'kcfinder_textbox',
					'status=0, toolbar=0, location=0, menubar=0, directories=0, ' +
					'resizable=1, scrollbars=0, width=800, height=600'
				);
			}
			</script>
		<div align="right" style="margin-bottom:20px;">
			<a href="<?php echo P_SLASH; ?>admin/plugin/download.html" class="link_back">Kembali</a>	 					
        </div> 					
    	<form class="form-horizontal" action="<?php echo URI; ?>" method="post">
        	<input type="hidden" name="mode" value="<?php echo $kode; ?>">
        	<div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Galleri : </label>
                <div class="col-sm-7">                	
					<select name="kat_download" required="required" class="form-control">
                    	<option value="">Pilih Kategori Download</option>
                        <?php
							$c = sql_db::sql_query("select id_kat_download, nama, id_connect from tbl_kat_download order by id_connect asc, nama asc");
							while($c1 = sql_db::sql_fetchrow($c))
							{
								$kat_download='';
								if($c1["id_connect"] <> '0')
								{
									$b = sql_db::sql_query("select nama from tbl_kat_download where id_kat_download='".$c1["id_connect"]."'");
									$b1 = sql_db::sql_fetchrow($b);
									$kat_download = $b1["nama"];
								}	
													
								if($d1["id_kat_download"] == $c1["id_kat_download"])
									echo '<option value="'.$c1["id_kat_download"].'" selected>'.$kat_download.' - '.$c1["nama"].'</option>';
								else
									echo '<option value="'.$c1["id_kat_download"].'">'.$kat_download.' - '.$c1["nama"].'</option>';								
							}		
						?>
                    </select>
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Nama File Download : </label>
                <div class="col-sm-7">                	
					<input type="text" name="download" class="form-control" required="required" value="<?php echo $d1["nama"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Publish : </label>
                <div class="col-sm-7">                	
					<input type="radio" name="publish" value="N" <?php if($d1["publish"] == '' or $d1["publish"] == 'N') echo 'checked="checked"'; ?> /> Tidak&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="publish" value="Y" <?php if($d1["publish"] == 'Y') echo 'checked="checked"'; ?> /> Ya&nbsp;&nbsp;&nbsp;
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Tanggal : </label>
                <div class="col-sm-7">                	
					<input type="text" name="tanggal" class="form-control" id="datetimepicker1" required value="<?php echo $d1["waktu"]; ?>" />
                </div>
			</div>
            <div class="form-group">
            	<label class="col-sm-3 control-label"><span class="required">*</span>File Download </label>
                <div class="col-sm-7">                	
					<input type="text" name="file" readonly="readonly" onclick="openKCFinderFile(this)" placeholder="Klik disini dan pilih file yang diinginkan dengan mengklik dua kali" style="cursor:pointer" class="form-control" value="<?php echo $d1["nama_file"]; ?>" required="required" />
                </div>                
            </div>
            <div class="form-group">
            	<label class="col-sm-3"></label>
                <div class="col-sm-7">
					<button class="btn btn-default" type="submit" name="submit_tambah">SIMPAN</button>
    	            <button class="btn btn-default" type="reset">RESET</button>
                </div>
			</div>
            
        </form>
    <?php
	}
	
	function heading($heading, $mode, $POST)
	{
		if($heading == 'tambah')
		{
			echo '<div class="panel-heading">Tambah File Download</div>';
			echo '<div class="panel-body"">';
				if(isset($POST["mode"]))
					$this->tambah($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'edit')
		{
			echo '<div class="panel-heading">Edit File Download</div>';
			echo '<div class="panel-body"">';
			if(isset($POST["mode"]))
					$this->edit($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'hapus')
		{
			echo '<div class="panel-heading">Hapus File Download</div>';
			echo '<div class="panel-body"">';
			$this->hapus($mode);
			echo '</div>';
		}
		else
		{
			echo '<div class="panel-heading">File Download</div>';
			echo '<div class="panel-body"">';
			$this->show_data();
			echo '</div>';
		}
	}

	function tambah($POST)
	{
		$download = anti($POST["download"]);
		$publish = anti($POST["publish"]);
		$kat_download = anti($POST["kat_download"]);
		$tanggal = anti($POST["tanggal"]);
		$file = anti($POST["file"]);
		
	
		if(!$this->cek_form($publish, $kat_download, $tanggal, $file))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('tambah','');
		}
		else
		{					
			$d = sql_db::sql_query("insert into tbl_download (id_kat_download, nama, waktu, publish, nama_file, id_user, ip_address) values ('$kat_download','$download','$tanggal','$publish','$file','','".$_SERVER["REMOTE_ADDR"]."')");
			

			if($d)
				berhasil(P_SLASH.P_AD.P_PLUGIN.'download/tambah.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'download/tambah.html');
		}
	}

	private function edit($POST)
	{
		$download = anti($POST["download"]);
		$publish = anti($POST["publish"]);
		$kat_download = anti($POST["kat_download"]);
		$tanggal = anti($POST["tanggal"]);
		$file = anti($POST["file"]);
		$kode = anti($POST["mode"]);
	

		if(!$this->cek_form( $publish, $kat_download, $tanggal, $file))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('edit','$kode');
		}
		else
		{					
			$d = sql_db::sql_query(" update  tbl_download set id_kat_download='$kat_download', nama='$download', waktu='$tanggal', publish='$publish', nama_file='$file', id_user='', ip_address='".$_SERVER["REMOTE_ADDR"]."' where kode='$kode'");


			if($d)
				berhasil(P_SLASH.P_AD.P_PLUGIN.'download/'.$kode.'/edit.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'download/'.$kode.'/edit.html');
		}		
	}

	
	private function hapus($kode)
	{		
		$d = sql_db::sql_query("delete from tbl_download where kode='$kode'");
		if($d)
			berhasil(P_SLASH.P_AD.P_PLUGIN.'download.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'download.html');
	}

	private function cek_form($publish, $kat_download, $tanggal, $file)
	{
		if($publish == '' or $kat_download == '' or $tanggal == '' or $file== '')
			return false;
		else
			return true;
	}
}

$download = new download;
$opt = isset($_GET["opt"]) ? $_GET["opt"] : '';
$mode = isset($_GET["mode"]) ? $_GET["mode"] : '';

echo '<div class="panel panel-default" style="width:100%;">';
$download->heading(anti($opt), anti($mode), $_POST);
echo '</div>';
?>