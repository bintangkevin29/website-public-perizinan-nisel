<?php
class link extends session
{
	function __contruct()
	{
		if(!parent::status_session)
			die('You cannot access the sistem');
	}
	
	private function show_data()
	{
	?>
    	<script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.bootstrap.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.min.js'; ?>"></script>
        <link href="<?php echo P_SLASH.P_JS; ?>datatable/css/jquery.dataTables.css" rel="stylesheet">
        <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable();
			} );
		</script>
    	<div><a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>link/tambah.html" class="btn btn-primary">Tambah Link</a></div><br>
		<table class="display table-responsive" id="example" width="100%">
        <thead>
        	<tr>
            	<th>LETAK</th>
            	<th>NAMA</th>
                <th>LINK</th>
                <th>PUBLISH</th>
                <th>AKTIVITAS</th>
            </tr>
        </thead>
        <tbody>
     <?php
		$d = sql_db::sql_query("select letak, nama, link, publish, kode from tbl_link order by nama asc");
		while($d1 = sql_db::sql_fetchrow($d))
		{
		?>
        	<tr>
            	<td><?php echo $d1["letak"]; ?></td>
            	<td><?php echo $d1["nama"]; ?></td>
                <td><?php echo $d1["link"]; ?></td>
                <td class="text-center"><?php echo $d1["publish"]; ?></td>
                <td class="text-center">
                	<a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>link/<?php echo urlencode($d1["kode"]); ?>/edit.html">EDIT<a>&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>link/<?php echo urlencode($d1["kode"]); ?>/hapus.html" onClick="return confirm('Apakah Anda yakin ingin menghapus data ini?' +  '\n\n' + 'Jika YA silahkan klik OK, Jika TIDAK klik CANCEL.')">HAPUS<a>
                </td>
            </tr>
        <?php
		}
		echo '</tbody></table>';
	}
	
	private function show_form($heading, $kode)
	{
		if($heading == 'edit')
		{
			$d = sql_db::sql_query("select * from tbl_link where lower(kode)='".urldecode($kode)."'");
			$d1 = sql_db::sql_fetchrow($d);
		}
		else
		{
			$d1["nama"] = '';
			$d1["publish"] = '';
			$d1["link"] = '';
			$d1["letak"] = '';
		}
		
	?>
    	<div align="right" style="margin-bottom:20px;">
			<a href="<?php echo P_SLASH; ?>admin/plugin/link.html" class="link_back">Kembali</a>	 					
        </div>
    	<form class="form-horizontal" action="<?php echo URI; ?>" method="post">
        	<input type="hidden" name="mode" value="<?php echo $kode; ?>">    
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Letak : </label>
                <div class="col-sm-7">                	
					<select name="letak" required="required" class="form-control">
                    	<option value="">Pilih Letak</option>
                        <option value="Atas" <?php if($d1["letak"] == 'Atas') echo 'selected="selected"'; ?>>Link Content Atas</option>
                        <option value="Tengah" <?php if($d1["letak"] == 'Tengah') echo 'selected="selected"'; ?>>Link Content Tengah</option>
                        <option value="Bawah2" <?php if($d1["letak"] == 'Bawah2') echo 'selected="selected"'; ?>>Link Content Bawah</option>
                        <option value="Bawah" <?php if($d1["letak"] == 'Bawah') echo 'selected="selected"'; ?>>Bawah</option>
						<option value="Header Website" <?php if($d1["letak"] == 'Header Website') echo 'selected="selected"'; ?>>Header Website</option>
						<option value="Header Portal" <?php if($d1["letak"] == 'Header Portal') echo 'selected="selected"'; ?>>Header Portal</option>
                    </select>
                </div>
			</div>    	
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Nama : </label>
                <div class="col-sm-7">                	
					<input type="text" name="nama" class="form-control" required="required" value="<?php echo $d1["nama"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Link : </label>
                <div class="col-sm-7">                	
					<input type="text" name="link" class="form-control" required="required" value="<?php echo $d1["link"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Publish : </label>
                <div class="col-sm-7">                	
					<input type="radio" name="publish" value="N" <?php if($d1["publish"] == '' or $d1["publish"] == 'N') echo 'checked="checked"'; ?> /> Tidak&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="publish" value="Y" <?php if($d1["publish"] == 'Y') echo 'checked="checked"'; ?> /> Ya&nbsp;&nbsp;&nbsp;
                </div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-3"></label>
                <div class="col-sm-7">
					<button class="btn btn-default" type="submit" name="submit_tambah">SIMPAN</button>
    	            <button class="btn btn-default" type="reset">RESET</button>
                </div>
			</div>
        </form>
    <?php
	}
	
	function heading($heading, $mode, $POST)
	{
		if($heading == 'tambah')
		{
			echo '<div class="panel-heading">Tambah Link</div>';
			echo '<div class="panel-body"">';
				if(isset($POST["mode"]))
					$this->tambah($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'edit')
		{
			echo '<div class="panel-heading">Edit Link</div>';
			echo '<div class="panel-body"">';
			if(isset($POST["mode"]))
					$this->edit($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'hapus')
		{
			echo '<div class="panel-heading">Hapus Link</div>';
			echo '<div class="panel-body"">';
			$this->hapus($mode);
			echo '</div>';
		}
		else
		{
			echo '<div class="panel-heading">Link</div>';
			echo '<div class="panel-body"">';
			$this->show_data();
			echo '</div>';
		}
		
	}
	
	function tambah($POST)
	{
		$nama = anti($POST["nama"]);
		$publish = anti($POST["publish"]);
		$link = anti($POST["link"]);
		$letak = anti($POST["letak"]);
		
		if(!$this->cek_form($nama, $link, $publish, $letak))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('tambah','');
		}
		else
		{			
			$d = sql_db::sql_query("insert into tbl_link (nama, publish, link, letak) values ('$nama','$publish','$link','$letak')");
		
			if($d)
				berhasil(P_SLASH.P_AD.P_PLUGIN.'link/tambah.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'link/tambah.html');
		}
	}
	
	private function edit($POST)
	{
		$nama = anti($POST["nama"]);
		$publish = anti($POST["publish"]);
		$link = anti($POST["link"]);
		$kode = anti($POST["mode"]);
		$letak = anti($POST["letak"]);
		
		
		$d = sql_db::sql_query("update tbl_link set link='$link', publish='$publish', nama='$nama', letak='$letak' where kode='$kode'");
		
		if($d)
			berhasil(P_SLASH.P_AD.P_PLUGIN.'link/'.$kode.'/edit.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'link/'.$kode.'/edit.html');
	}
	
	private function hapus($kode)
	{
		
		$d = sql_db::sql_query("delete from tbl_link where kode='$kode'");
		if($d)
			berhasil(P_SLASH.P_AD.P_PLUGIN.'link.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'link.html');
	}
	
	private function cek_form($nama, $link, $publish,$letak)
	{
		if($nama == '' and $link == '' and $publish == '' or $letak == '')
			return false;
		else
			return true;
	}
}

$link = new link;
$opt = isset($_GET["opt"]) ? $_GET["opt"] : '';
$mode = isset($_GET["mode"]) ? $_GET["mode"] : '';

echo '<div class="panel panel-default" style="width:100%;">';
$link->heading(anti($opt), anti($mode), $_POST);
echo '</div>';
?>
