<?php
class kontak extends session
{
	function __contruct()
	{
		if(!parent::status_session)
			die('You cannot access the sistem');
	}
	
	private function show_data()
	{
	?>
    	<script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.bootstrap.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.min.js'; ?>"></script>
        <link href="<?php echo P_SLASH.P_JS; ?>datatable/css/jquery.dataTables.css" rel="stylesheet">
        <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable();
			} );
		</script>
    	<div><a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>kontak/tambah.html" class="btn btn-primary">Tambah Kontak Saya</a></div><br>
		<table class="display table-responsive" id="example" width="100%">
        <thead>
        	<tr>
            	<th>NAMA</th>
                <th>EMAIL</th>
                <th>PUBLISH</th>
                <th>AKTIVITAS</th>
            </tr>
        </thead>
        <tbody>
     <?php
		$d = sql_db::sql_query("select kontak, email, publish, kode from tbl_kontak order by kontak asc");
		while($d1 = sql_db::sql_fetchrow($d))
		{
		?>
        	<tr>
            	<td><?php echo $d1["kontak"]; ?></td>
                <td><?php echo $d1["email"]; ?></td>
                <td class="text-center"><?php echo $d1["publish"]; ?></td>
                <td class="text-center">
                	<a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>kontak/<?php echo urlencode($d1["kode"]); ?>/edit.html">EDIT<a>&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>kontak/<?php echo urlencode($d1["kode"]); ?>/hapus.html" onClick="return confirm('Apakah Anda yakin ingin menghapus data ini?' +  '\n\n' + 'Jika YA silahkan klik OK, Jika TIDAK klik CANCEL.')">HAPUS<a>
                </td>
            </tr>
        <?php
		}
		echo '</tbody></table>';
	}
	
	private function show_form($heading, $kode)
	{
		if($heading == 'edit')
		{
			$d = sql_db::sql_query("select * from tbl_kontak where lower(kode)='".urldecode($kode)."'");
			$d1 = sql_db::sql_fetchrow($d);
		}
		else
		{
			$d1["kontak"] = '';
			$d1["publish"] = '';
			$d1["email"] = '';
		}
		
	?>
    	<div align="right" style="margin-bottom:20px;">
			<a href="<?php echo P_SLASH; ?>admin/plugin/kontak.html" class="link_back">Kembali</a>	 					
        </div>
    	<form class="form-horizontal" action="<?php echo URI; ?>" method="post">
        	<input type="hidden" name="mode" value="<?php echo $kode; ?>">        	
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Kontak : </label>
                <div class="col-sm-7">                	
					<input type="text" name="kontak" class="form-control" required="required" value="<?php echo $d1["kontak"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>E-Mail : </label>
                <div class="col-sm-7">                	
					<input type="email" name="email" class="form-control" required="required" value="<?php echo $d1["email"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Publish : </label>
                <div class="col-sm-7">                	
					<input type="radio" name="publish" value="N" <?php if($d1["publish"] == '' or $d1["publish"] == 'N') echo 'checked="checked"'; ?> /> Tidak&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="publish" value="Y" <?php if($d1["publish"] == 'Y') echo 'checked="checked"'; ?> /> Ya&nbsp;&nbsp;&nbsp;
                </div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-3"></label>
                <div class="col-sm-7">
					<button class="btn btn-default" type="submit" name="submit_tambah">SIMPAN</button>
    	            <button class="btn btn-default" type="reset">RESET</button>
                </div>
			</div>
        </form>
    <?php
	}
	
	function heading($heading, $mode, $POST)
	{
		if($heading == 'tambah')
		{
			echo '<div class="panel-heading">Tambah Kontak Saya</div>';
			echo '<div class="panel-body"">';
				if(isset($POST["mode"]))
					$this->tambah($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'edit')
		{
			echo '<div class="panel-heading">Edit Kontak Saya</div>';
			echo '<div class="panel-body"">';
			if(isset($POST["mode"]))
					$this->edit($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'hapus')
		{
			echo '<div class="panel-heading">Hapus Kontak Saya</div>';
			echo '<div class="panel-body"">';
			$this->hapus($mode);
			echo '</div>';
		}
		else
		{
			echo '<div class="panel-heading">Kontak Saya</div>';
			echo '<div class="panel-body"">';
			$this->show_data();
			echo '</div>';
		}
		
	}
	
	function tambah($POST)
	{
		$kontak = anti($POST["kontak"]);
		$publish = anti($POST["publish"]);
		$email = anti($POST["email"]);
		
		if(!$this->cek_form($kontak, $publish, $email))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('tambah','');
		}
		else
		{			
			$d = sql_db::sql_query("insert into tbl_kontak (kontak, publish, email) values ('$kontak','$publish','$email')");			
		
			if($d)
				berhasil(P_SLASH.P_AD.P_PLUGIN.'kontak/tambah.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'kontak/tambah.html');
		}
	}
	
	private function edit($POST)
	{
		$kontak = anti($POST["kontak"]);
		$publish = anti($POST["publish"]);
		$kode = anti($POST["mode"]);
		$email = anti($POST["email"]);
		
		
		
		$d = sql_db::sql_query("update tbl_kontak set kontak='$kontak', publish='$publish', email='$email' where kode='$kode'");
		
		if($d)
			berhasil(P_SLASH.P_AD.P_PLUGIN.'kontak/'.$kode.'/edit.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'kontak/'.$kode.'/edit.html');
	}
	
	private function hapus($kode)
	{
		
		$d = sql_db::sql_query("delete from tbl_kontak where kode='$kode'");
		if($d)
			berhasil(P_SLASH.P_AD.P_PLUGIN.'kontak.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'kontak.html');
	}
	
	private function cek_form($kontak, $publish, $email)
	{
		if($kontak == ''  and $publish == '' and $email == '')
			return false;
		else
			return true;
	}
}

$kontak = new kontak;
$opt = isset($_GET["opt"]) ? $_GET["opt"] : '';
$mode = isset($_GET["mode"]) ? $_GET["mode"] : '';

echo '<div class="panel panel-default" style="width:100%;">';
$kontak->heading(anti($opt), anti($mode), $_POST);
echo '</div>';
?>