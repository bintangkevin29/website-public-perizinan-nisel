<?php
class random extends session
{
	function __contruct()
	{
		if(!parent::status_session)
			die('You cannot access the sistem');
	}
	
	private function show_data()
	{
	?>
    	<script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.bootstrap.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.min.js'; ?>"></script>
        <link href="<?php echo P_SLASH.P_JS; ?>datatable/css/jquery.dataTables.css" rel="stylesheet">
        <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable();
			} );
		</script>
    	<div><a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>random/tambah.html" class="btn btn-primary">Tambah random</a></div><br>      

		<table class="display table-responsive" id="example" width="100%">
        <thead>
        	<tr>
            	<th>NAMA</th>
            	<th>KETERANGAN</th>
            	<th>LETAK</th>
                <th>PUBLISH</th>
                <th>TANGGAL</th>
                <th>GAMBAR</th>
                <th>AKTIVITAS</th>
            </tr>
        </thead>
        <tbody>
     <?php
		$d = sql_db::sql_query("select * from tbl_random where letak='bawah' order by waktu desc");
		while($d1 = sql_db::sql_fetchrow($d))
		{
		?>
        	<tr>
            	<td><?php echo $d1["nama"]; ?></td>
            	<td><?php echo $d1["keterangan"]; ?></td>
            	<td><?php echo $d1["letak"]; ?></td>
                <td><?php echo $d1["publish"]; ?></td>
                <td><?php echo tgl_indo($d1["waktu"]); ?></td>  
                <td class="text-center"><img src="<?php echo P_SERVERNAME.get_thumbs($d1["nama_file"]); ?>" /></td>
                <td class="text-center">
                	<a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>random/<?php echo urlencode($d1["kode"]); ?>/edit.html">EDIT<a>&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>random/<?php echo urlencode($d1["kode"]); ?>/hapus.html" onClick="return confirm('Apakah Anda yakin ingin menghapus data ini?' +  '\n\n' + 'Jika YA silahkan klik OK, Jika TIDAK klik CANCEL.')">HAPUS<a>
                </td>
            </tr>
        <?php
		}
		echo '</tbody></table>';
	}
	
	private function show_form($heading, $kode)
	{
		if($heading == 'edit')
		{
			$d = sql_db::sql_query("select * from tbl_random where lower(kode)='".urldecode($kode)."'");
			$d1 = sql_db::sql_fetchrow($d);
		}
		else
		{
			$d1["keterangan"] = '';
			$d1["nama"] = '';
			$d1["letak"] = '';
			$d1["publish"] = '';
			$d1["nama_file"] = '';
			$d1["waktu"] = date("Y-m-d H:i:s");
		}

		$_SESSION["KCFINDER"] = array(
			'disabled' => false,
		    'uploadURL' => "/userfiles/".P_NAME."/random/",
		    'uploadDir' => "",
			'thumbWidth' => 150,
		    'thumbHeight' => 150,
			'maxImageWidth' => 640,
		    'maxImageHeight' => 300
		);
	?>
    	<link type="text/css" href="<?php echo P_SLASH.P_JS; ?>datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" />		
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>datetimepicker/moment-with-locales.js"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript">
			$(function(){
				$("#datetimepicker1").datetimepicker({
					format:	'YYYY-MM-DD HH:mm:ss'
				});
			});
		</script>
        <script type="text/javascript">
			function openKCFinder(field) {
				window.KCFinder = {
					callBack: function(url) {
						field.value = url;
						window.KCFinder = null;
					}
				};

				window.open('<?php echo P_SLASH; ?>js/kcfinder/browse.php?type=images&dir=files/public', 'kcfinder_textbox',
					'status=0, toolbar=0, location=0, menubar=0, directories=0, ' +
					'resizable=1, scrollbars=0, width=800, height=600'
				);
			}
			</script>
		<div align="right" style="margin-bottom:20px;">
			<a href="<?php echo P_SLASH; ?>admin/plugin/random.html" class="link_back">Kembali</a>	 					
        </div>	 					
    	<form class="form-horizontal" action="<?php echo URI; ?>" method="post">
        	<input type="hidden" name="mode" value="<?php echo $kode; ?>">
            <div class="form-group">
				<label class="col-sm-3 control-label">Nama : </label>
                <div class="col-sm-7">                	
                	<input type="text" name="nama" class="form-control" value="<?php echo $d1["nama"]; ?>" />
                </div>
			</div>			
            <div class="form-group">
				<label class="col-sm-3 control-label">Keterangan : </label>
                <div class="col-sm-7">                	
                	<textarea name="keterangan" class="form-control"><?php echo $d1["keterangan"]; ?></textarea>
                </div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Letak : </label>
                <div class="col-sm-7">                	
					<input type="radio" name="letak" value="bawah" checked="checked"> Bawah
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Publish : </label>
                <div class="col-sm-7">                	
					<input type="radio" name="publish" value="N" <?php if($d1["publish"] == '' or $d1["publish"] == 'N') echo 'checked="checked"'; ?> /> Tidak&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="publish" value="Y" <?php if($d1["publish"] == 'Y') echo 'checked="checked"'; ?> /> Ya&nbsp;&nbsp;&nbsp;
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Tanggal : </label>
                <div class="col-sm-7">                	
					<input type="text" name="tanggal" class="form-control" id="datetimepicker1" required value="<?php echo $d1["waktu"]; ?>" />
                </div>
			</div>
            <div class="form-group">
            	<label class="col-sm-3 control-label"><span class="required">*</span>Gambar </label>
                <div class="col-sm-7">                	
					<input type="text" name="file" readonly="readonly" onclick="openKCFinder(this)" placeholder="Klik disini dan pilih gambar yang diinginkan dengan mengklik dua kali" style="cursor:pointer" class="form-control" value="<?php echo $d1["nama_file"]; ?>" required="required" />
                </div>                
            </div>
            <div class="form-group">
            	<label class="col-sm-3"></label>
                <div class="col-sm-7">
					<button class="btn btn-default" type="submit" name="submit_tambah">SIMPAN</button>
    	            <button class="btn btn-default" type="reset">RESET</button>
                </div>
			</div>           
        </form>
    <?php
	}
	
	function heading($heading, $mode, $POST)
	{
		if($heading == 'tambah')
		{
			echo '<div class="panel-heading">Tambah Random Image</div>';
			echo '<div class="panel-body"">';
				if(isset($POST["mode"]))
					$this->tambah($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'edit')
		{
			echo '<div class="panel-heading">Edit Random Image</div>';
			echo '<div class="panel-body"">';
			if(isset($POST["mode"]))
					$this->edit($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'hapus')
		{
			echo '<div class="panel-heading">Hapus Random Image</div>';
			echo '<div class="panel-body"">';
			$this->hapus($mode);
			echo '</div>';
		}
		else
		{
			echo '<div class="panel-heading">Random Image</div>';
			echo '<div class="panel-body"">';
			$this->show_data();
			echo '</div>';
		}
	}
	
	function tambah($POST)
	{
		$keterangan = anti($POST["keterangan"]);
		$publish = anti($POST["publish"]);
		$letak = anti($POST["letak"]);
		$tanggal = anti($POST["tanggal"]);
		$nama = anti($POST["nama"]);
		$file = anti($POST["file"]);
				
		if(!$this->cek_form($publish, $tanggal, $file, $letak))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('tambah','');
		}
		else
		{					
			$d = sql_db::sql_query("insert into tbl_random (nama, keterangan, waktu, publish, nama_file, id_user, ip_address, letak) values ('$nama','$keterangan','$tanggal','$publish','$file','','".$_SERVER["REMOTE_ADDR"]."','$letak')");
				
			if($d)
				berhasil(P_SLASH.P_AD.P_PLUGIN.'random/tambah.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'random/tambah.html');
		}
	}
	
	private function edit($POST)
	{
		$keterangan = anti($POST["keterangan"]);
		$publish = anti($POST["publish"]);
		$letak = anti($POST["letak"]);
		$tanggal = anti($POST["tanggal"]);
		$file = anti($POST["file"]);
		$nama = anti($POST["nama"]);
		$kode = anti($POST["mode"]);

		if(!$this->cek_form( $publish, $tanggal, $file, $letak))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('edit','$kode');
		}
		else
		{					
			$d = sql_db::sql_query(" update  tbl_random set nama='$nama', keterangan='$keterangan', waktu='$tanggal', publish='$publish', nama_file='$file', id_user='', ip_address='".$_SERVER["REMOTE_ADDR"]."',letak='$letak' where kode='$kode'");
	
			if($d)
				berhasil(P_SLASH.P_AD.P_PLUGIN.'random/'.$kode.'/edit.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'random/'.$kode.'/edit.html');
		}		
	}
	
	private function hapus($kode)
	{		
		$d = sql_db::sql_query("delete from tbl_random where kode='$kode'");
		if($d)
			berhasil(P_SLASH.P_AD.P_PLUGIN.'random.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'random.html');
	}
	
	private function cek_form($publish, $tanggal, $file, $letak)
	{
		if($publish == '' or $tanggal == '' or $file== '' or $letak == '')
			return false;
		else
			return true;
	}
}

$random = new random;
$opt = isset($_GET["opt"]) ? $_GET["opt"] : '';
$mode = isset($_GET["mode"]) ? $_GET["mode"] : '';

echo '<div class="panel panel-default" style="width:100%;">';
$random->heading(anti($opt), anti($mode), $_POST);
echo '</div>';
?>