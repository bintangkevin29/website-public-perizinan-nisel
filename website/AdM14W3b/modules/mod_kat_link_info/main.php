<?php
class kat_link_info extends session
{
	function __contruct()
	{
		if(!parent::status_session)
			die('You cannot access the sistem');
	}
	
	private function show_data()
	{
	?>
    	<script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.bootstrap.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.min.js'; ?>"></script>
        <link href="<?php echo P_SLASH.P_JS; ?>datatable/css/jquery.dataTables.css" rel="stylesheet">
        <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable();
			} );
		</script>
    	<div><a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>kat_link_info/tambah.html" class="btn btn-primary">Tambah Kategori Link Informasi</a></div><br>
		<table class="display table-responsive" id="example" width="100%">
        <thead>
        	<tr>
            	<th>JENIS LINK INFORMASI</th>
            	<th>KATEGORI</th>
                <th>PUBLISH</th>
                <th>AKTIVITAS</th>
            </tr>
        </thead>
        <tbody>
     <?php
		$d = sql_db::sql_query("select b.jenis, a.kategori, a.publish, a.kode from tbl_kat_link_info a inner join tbl_jenis_link b on a.id_jenis=b.id_jenis order by kategori asc");
		while($d1 = sql_db::sql_fetchrow($d))
		{
		?>
        	<tr>
            	<td><?php echo $d1["jenis"]; ?></td>
                <td><?php echo $d1["kategori"]; ?></td>
                <td class="text-center"><?php echo $d1["publish"]; ?></td>
                <td class="text-center">
                	<a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>kat_link_info/<?php echo urlencode($d1["kode"]); ?>/edit.html">EDIT<a>&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>kat_link_info/<?php echo urlencode($d1["kode"]); ?>/hapus.html" onClick="return confirm('Apakah Anda yakin ingin menghapus data ini?' +  '\n\n' + 'Jika YA silahkan klik OK, Jika TIDAK klik CANCEL.')">HAPUS<a>
                </td>
            </tr>
        <?php
		}
		echo '</tbody></table>';
	}
	
	private function show_form($heading, $kode)
	{
		if($heading == 'edit')
		{
			$d = sql_db::sql_query("select * from tbl_kat_link_info where lower(kode)='".urldecode($kode)."'");
			$d1 = sql_db::sql_fetchrow($d);
		}
		else
		{
			$d1["kategori"] = '';
			$d1["publish"] = '';
			$d1["jenis"] = '';
		}
		
	?>
    	<div align="right" style="margin-bottom:20px;">
			<a href="<?php echo P_SLASH; ?>admin/plugin/kat_link_info.html" class="link_back">Kembali</a>	 					
        </div>
    	<form class="form-horizontal" action="<?php echo URI; ?>" method="post">
        	<input type="hidden" name="mode" value="<?php echo $kode; ?>">        	
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Jenis Link Informasi: </label>
                <div class="col-sm-7">                	
                	<select name="jenis" class="form-control" required="required">
                    	<option value="">Pilih Jenis Link Informasi</option>
                        <?php
							$c = sql_db::sql_query("select * from tbl_jenis_link order by jenis asc");
							while($c1 = sql_db::sql_fetchrow($c))
							{
								if($c1["id_jenis"] == $d1["id_jenis"])
									echo '<option value="'.$c1["id_jenis"].'" selected="selected">'.$c1["jenis"].'</option>';
								else
									echo '<option value="'.$c1["id_jenis"].'">'.$c1["jenis"].'</option>';
							}
						?>
                    </select>					
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Kaegori : </label>
                <div class="col-sm-7">                	
					<input type="text" name="kategori" class="form-control" required="required" value="<?php echo $d1["kategori"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Publish : </label>
                <div class="col-sm-7">                	
					<input type="radio" name="publish" value="N" <?php if($d1["publish"] == '' or $d1["publish"] == 'N') echo 'checked="checked"'; ?> /> Tidak&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="publish" value="Y" <?php if($d1["publish"] == 'Y') echo 'checked="checked"'; ?> /> Ya&nbsp;&nbsp;&nbsp;
                </div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-3"></label>
                <div class="col-sm-7">
					<button class="btn btn-default" type="submit" name="submit_tambah">SIMPAN</button>
    	            <button class="btn btn-default" type="reset">RESET</button>
                </div>
			</div>
        </form>
    <?php
	}
	
	function heading($heading, $mode, $POST)
	{
		if($heading == 'tambah')
		{
			echo '<div class="panel-heading">Tambah Kategori Link Informasi</div>';
			echo '<div class="panel-body"">';
				if(isset($POST["mode"]))
					$this->tambah($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'edit')
		{
			echo '<div class="panel-heading">Edit Kategori Link Informasi</div>';
			echo '<div class="panel-body"">';
			if(isset($POST["mode"]))
					$this->edit($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'hapus')
		{
			echo '<div class="panel-heading">Hapus Kategori Link Informasi</div>';
			echo '<div class="panel-body"">';
			$this->hapus($mode);
			echo '</div>';
		}
		else
		{
			echo '<div class="panel-heading">Link</div>';
			echo '<div class="panel-body"">';
			$this->show_data();
			echo '</div>';
		}
		
	}
	
	function tambah($POST)
	{
		$kategori = anti($POST["kategori"]);
		$publish = anti($POST["publish"]);
		$jenis = anti($POST["jenis"]);
		
		if(!$this->cek_form($kategori, $jenis, $publish))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('tambah','');
		}
		else
		{			
			$d = sql_db::sql_query("insert into tbl_kat_link_info (kategori, publish, id_jenis) values ('$kategori','$publish','$jenis')");			
		
			if($d)
				berhasil(P_SLASH.P_AD.P_PLUGIN.'kat_link_info/tambah.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'kat_link_info/tambah.html');
		}
	}
	
	private function edit($POST)
	{
		$kategori = anti($POST["kategori"]);
		$publish = anti($POST["publish"]);
		$jenis = anti($POST["jenis"]);
		$kode = anti($POST["mode"]);
		
		
		$d = sql_db::sql_query("update tbl_kat_link_info set id_jenis='$jenis', publish='$publish', kategori='$kategori' where kode='$kode'");
		
		if($d)
			berhasil(P_SLASH.P_AD.P_PLUGIN.'kat_link_info/'.$kode.'/edit.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'kat_link_info/'.$kode.'/edit.html');
	}
	
	private function hapus($kode)
	{
		
		$d = sql_db::sql_query("delete from tbl_kat_link_info where kode='$kode'");
		if($d)
			berhasil(P_SLASH.P_AD.P_PLUGIN.'kat_link_info.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'kat_link_info.html');
	}
	
	private function cek_form($kategori, $kat_link_info, $publish)
	{
		if($kategori == '' and $kat_link_info == '' and $publish == '')
			return false;
		else
			return true;
	}
}

$kat_link_info = new kat_link_info;
$opt = isset($_GET["opt"]) ? $_GET["opt"] : '';
$mode = isset($_GET["mode"]) ? $_GET["mode"] : '';

echo '<div class="panel panel-default" style="width:100%;">';
$kat_link_info->heading(anti($opt), anti($mode), $_POST);
echo '</div>';
?>