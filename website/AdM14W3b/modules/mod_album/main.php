<?php
class album extends session
{
	function __contruct()
	{
		if(!parent::status_session)
			die('You cannot access the sistem');
	}
	private function show_data()
	{
	?>
    	<script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.bootstrap.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.min.js'; ?>"></script>
        <link href="<?php echo P_SLASH.P_JS; ?>datatable/css/jquery.dataTables.css" rel="stylesheet">
        <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable();
			} );
		</script>
    	<div><a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>album/tambah.html" class="btn btn-primary">Tambah Album</a></div><br>
		<table class="display table-responsive" id="example" width="100%">
        <thead>
        	<tr>
            	<th>GALLERI</th>
                <th>ALBUM</th>
                <th>TANGGAL</th>
                <th>PUBLISH</th>
                <th>GAMBAR</th>
                <th>AKTIVITAS</th>
            </tr>
        </thead>
        <tbody>
     <?php
		$d = sql_db::sql_query("select a.id_album, a.album, b.galleri, a.waktu, a.publish, a.kode, a.nama_file from tbl_album a inner join tbl_galleri b on a.id_galleri=b.id_galleri order by a.waktu asc");
		while($d1 = sql_db::sql_fetchrow($d))
		{
		?>
        	<tr>
            	<td><?php echo $d1["galleri"]; ?></td>
                <td><?php echo $d1["album"]; ?></td>
                <td><?php echo tgl_indo($d1["waktu"]); ?></td>
                <td class="text-center"><?php echo $d1["publish"]; ?></td>                
                <td class="text-center"><img src="<?php echo P_SERVERNAME.get_thumbs($d1["nama_file"]); ?>" /></td>
                <td class="text-center">
                	<a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>album/<?php echo urlencode($d1["kode"]); ?>/edit.html">EDIT<a>&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>album/<?php echo urlencode($d1["kode"]); ?>/hapus.html" onClick="return confirm('Apakah Anda yakin ingin menghapus data ini?' +  '\n\n' + 'Jika YA silahkan klik OK, Jika TIDAK klik CANCEL.')">HAPUS<a>
                </td>
            </tr>
        <?php
		}
		echo '</tbody></table>';
	}

	private function show_form($heading, $kode)
	{
		if($heading == 'edit')
		{
			$d = sql_db::sql_query("select * from tbl_album where lower(kode)='".urldecode($kode)."'");
			$d1 = sql_db::sql_fetchrow($d);
		}
		else
		{
			$d1["album"] = '';
			$d1["id_galleri"] = '';
			$d1["publish"] = '';
			$d1["file"] = '';
			$d1["id_galleri"] = '';
			$d1["nama_file"] = '';
			$d1["waktu"] = date("Y-m-d H:i:s");
		}
		
		$_SESSION["KCFINDER"] = array(
			'disabled' => false,
		    'uploadURL' => "/userfiles/".P_NAME."/album/",
		    'uploadDir' => "",
			'thumbWidth' => 100,
		    'thumbHeight' => 100
		);
	?>
    	<link type="text/css" href="<?php echo P_SLASH.P_JS; ?>datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" />		
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>datetimepicker/moment-with-locales.js"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript">
			$(function(){
				$("#datetimepicker1").datetimepicker({
					format:	'YYYY-MM-DD HH:mm:ss'
				});
			});
		</script>
        <script type="text/javascript">
			function openKCFinder(field) {
				window.KCFinder = {
					callBack: function(url) {
						field.value = url;
						window.KCFinder = null;
					}
				};
				window.open('<?php echo P_SLASH; ?>js/kcfinder/browse.php?type=images&dir=files/public', 'kcfinder_textbox',
					'status=0, toolbar=0, location=0, menubar=0, directories=0, ' +
					'resizable=1, scrollbars=0, width=800, height=600'
				);
			}
			</script>
        <div align="right" style="margin-bottom:20px;">
			<a href="<?php echo P_SLASH; ?>admin/plugin/album.html" class="link_back">Kembali</a>	 					
        </div>
    	<form class="form-horizontal" action="<?php echo URI; ?>" method="post">
        	<input type="hidden" name="mode" value="<?php echo $kode; ?>">
        	<div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Galleri : </label>
                <div class="col-sm-7">                	
					<select name="galleri" required="required" class="form-control">
                    	<option value="">Pilih Galleri</option>
                        <?php
							$c = sql_db::sql_query("select id_galleri, galleri from tbl_galleri order by galleri asc");
							while($c1 = sql_db::sql_fetchrow($c))
							{								
								if($d1["id_galleri"] == $c1["id_galleri"])
									echo '<option value="'.$c1["id_galleri"].'" selected>'.$c1["galleri"].'</option>';
								else
									echo '<option value="'.$c1["id_galleri"].'">'.$c1["galleri"].'</option>';								
							}		
						?>
                    </select>
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Album : </label>
                <div class="col-sm-7">                	
					<input type="text" name="album" class="form-control" required value="<?php echo $d1["album"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Publish : </label>
                <div class="col-sm-7">                	
					<input type="radio" name="publish" value="N" <?php if($d1["publish"] == '' or $d1["publish"] == 'N') echo 'checked="checked"'; ?> /> Tidak&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="publish" value="Y" <?php if($d1["publish"] == 'Y') echo 'checked="checked"'; ?> /> Ya&nbsp;&nbsp;&nbsp;
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Tanggal : </label>
                <div class="col-sm-7">                	
					<input type="text" name="tanggal" class="form-control" id="datetimepicker1" required value="<?php echo $d1["waktu"]; ?>" />
                </div>
			</div>
            <div class="form-group">
            	<label class="col-sm-3 control-label"><span class="required">*</span>Gambar </label>
                <div class="col-sm-7">                	
					<input type="text" name="file" readonly="readonly" onclick="openKCFinder(this)" placeholder="Klik disini dan pilih gambar yang diinginkan dengan mengklik dua kali" style="cursor:pointer" class="form-control" value="<?php echo $d1["nama_file"]; ?>" required="required" />
                </div>                
            </div>
            <div class="form-group">
            	<label class="col-sm-3"></label>
                <div class="col-sm-7">
					<button class="btn btn-default" type="submit" name="submit_tambah">SIMPAN</button>
    	            <button class="btn btn-default" type="reset">RESET</button>
                </div>
			</div>            
        </form>
    <?php
	}
	
	function heading($heading, $mode, $POST)
	{
		if($heading == 'tambah')
		{
			echo '<div class="panel-heading">Tambah Album</div>';
			echo '<div class="panel-body"">';
				if(isset($POST["mode"]))
					$this->tambah($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'edit')
		{
			echo '<div class="panel-heading">Edit Album</div>';
			echo '<div class="panel-body"">';
			if(isset($POST["mode"]))
					$this->edit($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'hapus')
		{
			echo '<div class="panel-heading">Hapus Album</div>';
			echo '<div class="panel-body"">';
			$this->hapus($mode);
			echo '</div>';
		}
		else
		{
			echo '<div class="panel-heading">Album</div>';
			echo '<div class="panel-body"">';
			$this->show_data();
			echo '</div>';
		}	
	}
	
	function tambah($POST)
	{
		$galleri = anti($POST["galleri"]);
		$publish = anti($POST["publish"]);
		$album = anti($POST["album"]);
		$tanggal = anti($POST["tanggal"]);
		$file = anti($POST["file"]);
				
		if(!$this->cek_form($galleri, $publish, $album, $tanggal, $file))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('tambah','');
		}
		else
		{					
			$d = sql_db::sql_query("insert into tbl_album (id_galleri, album, waktu, publish, nama_file, id_user, ip_address) values ('$galleri','$album','$tanggal','$publish','$file','','".$_SERVER["REMOTE_ADDR"]."')");			

			if($d)
				berhasil(P_SLASH.P_AD.P_PLUGIN.'album/tambah.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'album/tambah.html');
		}
	}
	
	private function edit($POST)
	{
		$galleri = anti($POST["galleri"]);
		$publish = anti($POST["publish"]);
		$album = anti($POST["album"]);
		$tanggal = anti($POST["tanggal"]);
		$file = anti($POST["file"]);
		$kode = anti($POST["mode"]);
		
		if(!$this->cek_form($galleri, $publish, $album, $tanggal, $file))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('edit','$kode');
		}
		else
		{					
			$d = sql_db::sql_query(" update  tbl_album set id_galleri='$galleri', album='$album', waktu='$tanggal', publish='$publish', nama_file='$file', id_user='', ip_address='".$_SERVER["REMOTE_ADDR"]."' where kode='$kode'");	

			if($d)
				berhasil(P_SLASH.P_AD.P_PLUGIN.'album/'.$kode.'/edit.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'album/'.$kode.'/edit.html');
		}		
	}
	
	private function hapus($kode)
	{		
		$d = sql_db::sql_query("delete from tbl_album where kode='$kode'");
		if($d)
			berhasil(P_SLASH.P_AD.P_PLUGIN.'album.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'album.html');
	}
	
	private function cek_form($galleri, $publish, $album, $tanggal, $file)
	{
		if($galleri== '' or $publish == '' or $album == '' or $tanggal == '' or $file== '')
			return false;
		else
			return true;
	}
}

$album = new album;
$opt = isset($_GET["opt"]) ? $_GET["opt"] : '';
$mode = isset($_GET["mode"]) ? $_GET["mode"] : '';

echo '<div class="panel panel-default" style="width:100%;">';
$album->heading(anti($opt), anti($mode), $_POST);
echo '</div>';
?>