<?php
class kalender extends session
{
	function __contruct()
	{
		if(!parent::status_session)
			die('You cannot access the sistem');
	}
	
	private function show_data()
	{
	?>
    	<script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.bootstrap.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.min.js'; ?>"></script>
        <link href="<?php echo P_SLASH.P_JS; ?>datatable/css/jquery.dataTables.css" rel="stylesheet">
        <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable();
			} );
		</script>
    	<div><a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>kalender/tambah.html" class="btn btn-primary">Tambah Kalender Kegiatan</a></div><br>
		<table class="display table-responsive" id="example" width="100%">
        <thead>
        	<tr>
            	<th>KEGIATAN</th>
            	<th>TANGGAL</th>
                <th>LINK</th>
                <th>PUBLISH</th>
                <th>KETERANGAN</th>
                <th>AKTIVITAS</th>
            </tr>
        </thead>
        <tbody>
     <?php
		$d = sql_db::sql_query("select * from tbl_kalender order by waktu desc");
		while($d1 = sql_db::sql_fetchrow($d))
		{
			$tanggal = tgl_indo($d1["tanggal"]);
			if($d1["tanggal1"] != '0000-00-00')
			{
				$tanggal = $tanggal." s.d. ".tgl_indo($d1["tanggal1"]);
			}
		?>
        	<tr>
            	<td><?php echo $d1["kegiatan"]; ?></td>
            	<td><?php echo $tanggal; ?></td>
                <td><?php echo $d1["link"]; ?></td>
                <td class="text-center"><?php echo $d1["publish"]; ?></td>
                <td><?php echo $d1["keterangan"]; ?></td>
                <td class="text-center">
                	<a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>kalender/<?php echo urlencode($d1["kode"]); ?>/edit.html">EDIT<a>&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>kalender/<?php echo urlencode($d1["kode"]); ?>/hapus.html" onClick="return confirm('Apakah Anda yakin ingin menghapus data ini?' +  '\n\n' + 'Jika YA silahkan klik OK, Jika TIDAK klik CANCEL.')">HAPUS<a>
                </td>
            </tr>
        <?php
		}
		echo '</tbody></table>';
	}
	
	private function show_form($heading, $kode)
	{
		if($heading == 'edit')
		{
			$d = sql_db::sql_query("select * from tbl_kalender where lower(kode)='".urldecode($kode)."'");
			$d1 = sql_db::sql_fetchrow($d);
		}
		else
		{
			$d1["kegiatan"] = '';
			$d1["publish"] = '';
			$d1["link"] = '';
			$d1["tanggal"] = '';
			$d1["tanggal1"] = '';
			$d1["keterangan"] = '';
		}
		
	?>
    	<div align="right" style="margin-bottom:20px;">
			<a href="<?php echo P_SLASH; ?>admin/plugin/kalender.html" class="link_back">Kembali</a>	 					
        </div>
    	<form class="form-horizontal" action="<?php echo URI; ?>" method="post">
        	<input type="hidden" name="mode" value="<?php echo $kode; ?>">                  	
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Kegiatan : </label>
                <div class="col-sm-7">                	
					<input type="text" name="kegiatan" class="form-control" required="required" value="<?php echo $d1["kegiatan"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Tanggal : </label>
                <div class="col-sm-7">                	
					<input type="date" name="tanggal" class="form-control" required="required" value="<?php echo $d1["tanggal"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label">s.d. Tanggal : </label>
                <div class="col-sm-7">                	
					<input type="date" name="tanggal1" class="form-control" value="<?php echo $d1["tanggal1"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label">Link : </label>
                <div class="col-sm-7">                	
					<input type="text" name="link" class="form-control" value="<?php echo $d1["link"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label">Keterangan : </label>
                <div class="col-sm-7">                	
					<textarea name="keterangan" class="form-control"><?php echo $d1["keterangan"]; ?></textarea>
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Publish : </label>
                <div class="col-sm-7">                	
					<input type="radio" name="publish" value="N" <?php if($d1["publish"] == '' or $d1["publish"] == 'N') echo 'checked="checked"'; ?> /> Tidak&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="publish" value="Y" <?php if($d1["publish"] == 'Y') echo 'checked="checked"'; ?> /> Ya&nbsp;&nbsp;&nbsp;
                </div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-3"></label>
                <div class="col-sm-7">
					<button class="btn btn-default" type="submit" name="submit_tambah">SIMPAN</button>
    	            <button class="btn btn-default" type="reset">RESET</button>
                </div>
			</div>
        </form>
    <?php
	}
	
	function heading($heading, $mode, $POST)
	{
		if($heading == 'tambah')
		{
			echo '<div class="panel-heading">Tambah Kalender Kegiatan</div>';
			echo '<div class="panel-body"">';
				if(isset($POST["mode"]))
					$this->tambah($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'edit')
		{
			echo '<div class="panel-heading">Edit Kalender Kegiatan</div>';
			echo '<div class="panel-body"">';
			if(isset($POST["mode"]))
					$this->edit($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'hapus')
		{
			echo '<div class="panel-heading">Hapus Kalender Kegiatan</div>';
			echo '<div class="panel-body"">';
			$this->hapus($mode);
			echo '</div>';
		}
		else
		{
			echo '<div class="panel-heading">Kalender Kegiatan</div>';
			echo '<div class="panel-body"">';
			$this->show_data();
			echo '</div>';
		}
		
	}
	
	function tambah($POST)
	{
		$kegiatan = anti($POST["kegiatan"]);
		$publish = anti($POST["publish"]);
		$link = anti($POST["link"]);
		$tanggal = anti($POST["tanggal"]);
		$tanggal1 = anti($POST["tanggal1"]);
		$keterangan = anti($POST["keterangan"]);
		
		if(!$this->cek_form($kegiatan, $publish, $tanggal))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('tambah','');
		}
		else
		{			
			$d = sql_db::sql_query("insert into tbl_kalender (kegiatan, link, tanggal, tanggal1, publish, keterangan) values ('$kegiatan','$link','$tanggal','$tanggal1','$publish','$keterangan')");
		
			if($d)
				berhasil(P_SLASH.P_AD.P_PLUGIN.'kalender/tambah.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'kalender/tambah.html');
		}
	}
	
	private function edit($POST)
	{
		$kegiatan = anti($POST["kegiatan"]);
		$publish = anti($POST["publish"]);
		$link = anti($POST["link"]);
		$tanggal = anti($POST["tanggal"]);
		$tanggal1 = anti($POST["tanggal1"]);
		$keterangan = anti($POST["keterangan"]);
		$kode = anti($POST["mode"]);
		
		
		$d = sql_db::sql_query("update tbl_kalender set link='$link', publish='$publish', kegiatan='$kegiatan', tanggal='$tanggal', tanggal1='$tanggal1', keterangan='$keterangan' where kode='$kode'");
		
		if($d)
			berhasil(P_SLASH.P_AD.P_PLUGIN.'kalender/'.$kode.'/edit.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'kalender/'.$kode.'/edit.html');
	}
	
	private function hapus($kode)
	{
		
		$d = sql_db::sql_query("delete from tbl_kalender where kode='$kode'");
		if($d)
			berhasil(P_SLASH.P_AD.P_PLUGIN.'link.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'link.html');
	}
	
	private function cek_form($kegiatan, $publish, $tanggal)
	{
		if($kegiatan == '' and $publish == '' or $tanggal == '')
			return false;
		else
			return true;
	}
}

$kalender = new kalender;
$opt = isset($_GET["opt"]) ? $_GET["opt"] : '';
$mode = isset($_GET["mode"]) ? $_GET["mode"] : '';

echo '<div class="panel panel-default" style="width:100%;">';
$kalender->heading(anti($opt), anti($mode), $_POST);
echo '</div>';
?>