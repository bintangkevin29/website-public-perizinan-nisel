<?php
class shout extends session{
	function __contruct()
	{
		if(!parent::status_session)
			die('You cannot access the sistem');
	}

	private function show_data()
	{
	?>
    	<script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.bootstrap.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS.'datatable/js/jquery.dataTables.min.js'; ?>"></script>
        <link href="<?php echo P_SLASH.P_JS; ?>datatable/css/jquery.dataTables.css" rel="stylesheet">
        <script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable();
			} );
		</script>
    	      
		<table class="display" id="example" width="100%">
        <thead>
	       	<tr>
            	<th>WAKTU</th>
            	<th>NAMA</th>
                <th>EMAIL</th>
                <th>TELEPON</th>
                <th>PESAN</th>
                <th>JAWABAN</th>                
                <th>AKTIVITAS</th>
            </tr>
        </thead>
        <tbody>
	    <?php
		$d = sql_db::sql_query("select * from tbl_pengaduan order by waktu desc");
		while($d1 = sql_db::sql_fetchrow($d))
		{			
		?>
        	<tr>
            	<td><?php echo $d1["waktu"]; ?></td>
                <td><?php echo $d1["nama"]; ?></td>
                <td><?php echo $d1["email"]; ?></td>
                <td><?php echo $d1["phone"]; ?></td>
                <td><?php echo $d1["isi"]; ?></td>
                <td><?php echo $d1["jawaban"]; ?></td>
                <td class="text-center">
                	<a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>pengaduan/<?php echo urlencode($d1["kode"]); ?>/edit.html">EDIT<a>&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo P_SLASH.P_AD.P_PLUGIN; ?>pengaduan/<?php echo urlencode($d1["kode"]); ?>/hapus.html" onClick="return confirm('Apakah Anda yakin ingin menghapus data ini?' +  '\n\n' + 'Jika YA silahkan klik OK, Jika TIDAK klik CANCEL.')">HAPUS<a>
                </td>
            </tr>
        <?php
		}
		echo '</tbody></table>';
	}

	private function show_form($heading, $kode)
	{
		if($heading == 'edit')
		{
			$d = sql_db::sql_query("select * from tbl_pengaduan where lower(kode)='".urldecode($kode)."'");
			$d1 = sql_db::sql_fetchrow($d);
		}
		else
		{
			$d1["nama"] = '';
			$d1["phnoe"] = '';
			$d1["email"] = '';
			$d1["isi"] = '';
			$d1["jawaban"] = '';
			$d1["publish"] = '';
			$d1["publish_jawaban"] = '';
			$d1["waktu"] = date("Y-m-d H:i:s");
		}		
	?>
    	<link type="text/css" href="<?php echo P_SLASH.P_JS; ?>datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" />		
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>datetimepicker/moment-with-locales.js"></script>
        <script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript">
			$(function(){
				$("#datetimepicker1").datetimepicker({
					format:	'YYYY-MM-DD HH:mm:ss'
				});
			});
		</script>        
		 <div align="right" style="margin-bottom:20px;">
			<a href="<?php echo P_SLASH; ?>admin/plugin/pengaduan.html" class="link_back">Kembali</a>	 					
        </div>					
    	<form class="form-horizontal" action="<?php echo URI; ?>" method="post">
        	<input type="hidden" name="mode" value="<?php echo $kode; ?>">      	
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Nama : </label>
                <div class="col-sm-7">                	
					<input type="text" name="nama" class="form-control" required="required" value="<?php echo $d1["nama"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>E-Mail : </label>
                <div class="col-sm-7">                	
					<input type="email" name="email" class="form-control" required="required" value="<?php echo $d1["email"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Telepon : </label>
                <div class="col-sm-7">                	
					<input type="number" name="phone" class="form-control" required="required" value="<?php echo $d1["phone"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Pesan : </label>
                <div class="col-sm-7">                	
					<textarea name="pesan" class="form-control" required><?php echo $d1["isi"]; ?></textarea>
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Publish : </label>
                <div class="col-sm-7">                	
					<input type="radio" name="publish" value="N" <?php if($d1["publish"] == '' or $d1["publish"] == 'N') echo 'checked="checked"'; ?> /> Tidak&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="publish" value="Y" <?php if($d1["publish"] == 'Y') echo 'checked="checked"'; ?> /> Ya&nbsp;&nbsp;&nbsp;
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label">Jawaban : </label>
                <div class="col-sm-7">                	
					<textarea name="jawaban" class="form-control" style="height:100px;"><?php echo $d1["jawaban"]; ?></textarea>
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Publish Jawaban : </label>
                <div class="col-sm-7">                	
					<input type="radio" name="publish_jawaban" value="N" <?php if($d1["publish_jawaban"] == '' or $d1["publish_jawaban"] == 'N') echo 'checked="checked"'; ?> /> Tidak&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="publish_jawaban" value="Y" <?php if($d1["publish_jawaban"] == 'Y') echo 'checked="checked"'; ?> /> Ya&nbsp;&nbsp;&nbsp;
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Tanggal : </label>
                <div class="col-sm-7">                	
					<input type="text" name="tanggal" class="form-control" id="datetimepicker1" required value="<?php echo $d1["waktu"]; ?>" />
                </div>
			</div>
            <div class="form-group">
            	<label class="col-sm-3"></label>
                <div class="col-sm-7">
					<button class="btn btn-default" type="submit" name="submit_tambah">SIMPAN</button>
    	            <button class="btn btn-default" type="reset">RESET</button>
                </div>
			</div>          
        </form>
    <?php
	}
	
	function heading($heading, $mode, $POST)
	{
		if($heading == 'tambah')
		{
			echo '<div class="panel-heading">Tambah Pengaduan</div>';
			echo '<div class="panel-body"">';
				if(isset($POST["mode"]))
					$this->tambah($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'edit')
		{
			echo '<div class="panel-heading">Edit Pengaduan</div>';
			echo '<div class="panel-body"">';
			if(isset($POST["mode"]))
					$this->edit($POST);
				else
					$this->show_form($heading, $mode);
			echo '</div>';
		}
		elseif($heading == 'hapus')
		{
			echo '<div class="panel-heading">Hapus Pengaduan</div>';
			echo '<div class="panel-body"">';
			$this->hapus($mode);
			echo '</div>';
		}
		else
		{
			echo '<div class="panel-heading">File Pengaduan</div>';
			echo '<div class="panel-body"">';
			$this->show_data();
			echo '</div>';
		}
	}
	
	function tambah($POST)
	{
		$pesan = anti($POST["pesan"]);
		$jawaban = anti($POST["jawaban"]);
		$publish = anti($POST["publish"]);
		$email = anti($POST["email"]);
		$tanggal = anti($POST["tanggal"]);
		$nama = anti($POST["nama"]);
		$phone = anti($POST["phone"]);
		$publish_jawaban = anti($POST["publish_jawaban"]);
		
		
		if(!$this->cek_form($nama, $email, $phone, $pesan, $publish, $tanggal))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('tambah','');
		}
		else
		{					
			$d = sql_db::sql_query("insert into tbl_pengaduan (nama, email, phone, waktu, publish, isi, jawaban, id_user, ip_address, publish_jawaban) values ('$nama','$email','$phone','$tanggal','$publish','$pesan','$jawaban','','".$_SERVER["REMOTE_ADDR"]."','$publish_jawaban')");			
			if($d)
				berhasil(P_SLASH.P_AD.P_PLUGIN.'pengaduan/tambah.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'pengaduan/tambah.html');
		}
	}

	private function edit($POST)
	{
		$pesan = anti($POST["pesan"]);
		$jawaban = anti($POST["jawaban"]);
		$publish = anti($POST["publish"]);
		$email = anti($POST["email"]);
		$tanggal = anti($POST["tanggal"]);
		$nama = anti($POST["nama"]);
		$phone = anti($POST["phone"]);
		$publish_jawaban = anti($POST["publish_jawaban"]);
		$kode = anti($POST["mode"]);
		
		if(!$this->cek_form($nama, $email, $phone, $pesan, $publish, $tanggal))
		{
			echo '<div class="error">Isi form dengan lengkap</div>';
			$this->show_form('edit','$kode');
		}
		else
		{					
			$d = sql_db::sql_query(" update  tbl_pengaduan set nama='$nama', email='$email', phone='$phone', isi='$pesan', jawaban='$jawaban', waktu='$tanggal', publish='$publish', id_user='', ip_address='".$_SERVER["REMOTE_ADDR"]."', publish_jawaban='$publish_jawaban' where kode='$kode'");

			if($d)
				berhasil(P_SLASH.P_AD.P_PLUGIN.'pengaduan/'.$kode.'/edit.html');
			else
				gagal(P_SLASH.P_AD.P_PLUGIN.'pengaduan/'.$kode.'/edit.html');
		}		
	}
	
	private function hapus($kode)
	{		
		$d = sql_db::sql_query("delete from tbl_pengaduan where kode='$kode'");
		if($d)
			berhasil(P_SLASH.P_AD.P_PLUGIN.'pengaduan.html');
		else
			gagal(P_SLASH.P_AD.P_PLUGIN.'pengaduan.html');
	}
	
	private function cek_form($nama, $email, $phone, $pesan, $publish, $tanggal)
	{
		if($nama == '' or $email == '' or $tanggal == '' or $phone== '' or $publish=='' or $pesan=='')
			return false;
		else
			return true;
	}
}

$shout = new shout;
$opt = isset($_GET["opt"]) ? $_GET["opt"] : '';
$mode = isset($_GET["mode"]) ? $_GET["mode"] : '';

echo '<div class="panel panel-default" style="width:100%;">';
$shout->heading(anti($opt), anti($mode), $_POST);
echo '</div>';
?>