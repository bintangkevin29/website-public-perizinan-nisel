<?php
	if(isset($_POST["nama"]) and isset($_POST["alamat"]) and isset($_POST["email"]) and isset($_POST["kontak"]) and  isset($_POST["username"]))
	{
		$nama = anti($_POST["nama"]);
		$alamat = anti($_POST["alamat"]);
		$email = anti($_POST["email"]);
		$kontak = anti($_POST["kontak"]);
		$username = anti($_POST["username"]);
		$password = anti($_POST["password"]);
		
		if(empty($nama) or empty($email) or empty($kontak) or empty($username))
			$error = 'Isi Form dengan Lengkap';
		else
		{
			$d = $sess->update_profil($nama, $alamat, $email, $kontak, $username, $password, $_SESSION[P_SESSIONADMIN]);
		}
		if($d)
			$error = 'Berhasil';
		else
			$error = $d;
	}
?>
<div class="panel panel-default" style="width:100%;">
	<div class="panel-heading">Beranda</div>
    <div class="panel-body">
    	<?php
		if(isset($error))
			echo $error;
			$d = $sql->sql_query("select * from tbl_user where session_id='".$_SESSION[P_SESSIONADMIN]."'");
			$d1 = $sql->sql_fetchrow($d);
		?>
    	<form action="<?php echo URI; ?>" method="post" class="form-horizontal">
	        <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Nama : </label>
                <div class="col-sm-7">                	
					<input type="text" name="nama" class="form-control" required="required" value="<?php echo $d1["nama"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label">Alamat : </label>
                <div class="col-sm-7">                	
					<input type="text" name="alamat" class="form-control" value="<?php echo $d1["alamat"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>E-Mail : </label>
                <div class="col-sm-7">                	
					<input type="email" name="email" class="form-control" required="required" value="<?php echo $d1["email"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>No. Telepon : </label>
                <div class="col-sm-7">                	
					<input type="text" name="kontak" class="form-control" required="required" value="<?php echo $d1["kontak"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"><span class="required">*</span>Username : </label>
                <div class="col-sm-7">                	
					<input type="text" name="username" class="form-control" required="required" value="<?php echo $d1["username"]; ?>" />
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label">Password : </label>
                <div class="col-sm-7">                	
					<input type="password" name="password" class="form-control" />
                    <span class="pesan_form">Jika Anda tidak ingin mengganti password form ini tidak perlu diisi</span>
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label"></label>
                <div class="col-sm-7">                	
					<button type="submit" class="btn btn-primary"> Simpan</button>
                </div>
			</div>
        </form>
    </div>
</div>
