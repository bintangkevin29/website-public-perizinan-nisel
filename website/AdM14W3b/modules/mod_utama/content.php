<?php include(P_MODULES.'mod_utama/menu_login.php'); ?>
<style type="text/css">
@media screen and (min-width: 990px) {
  .equal, .equal > div[class*='col-'] {  
      display: -webkit-box;
      display: -moz-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      flex:1 1 auto;
  }
}
</style>

<div class="container" id="isi_content" style="margin-top:20px;">
        <div class="row equal">    
            <div class="col-md-3">
                <?php include(P_MODULES.'menu_kiri.php'); ?>
            </div>
            <div class="col-md-9">
                <?php include(P_MODULES.'menu_kanan.php'); ?>
            </div>
        </div>
</div>