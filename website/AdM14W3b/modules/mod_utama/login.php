<?php
$error='';
include(P_MODULES.'mod_utama/menu_nonlogin.php');
if(isset($_POST["username"]) and isset($_POST["password"]))
{
	if(isset($_POST['g-recaptcha-response'])){
		$captcha=$_POST['g-recaptcha-response'];
	}
	if(!$captcha){
		echo '<h2>Please check the the captcha form.</h2>';
		exit;
	}
	$secretKey = "6LcsHFsUAAAAAKVYMKjkvXMS7BzU4k2FEzFSRxzF";
	$ip = $_SERVER['REMOTE_ADDR'];
	$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$captcha."&remoteip=".$ip);
	$responseKeys = json_decode($response,true);
	if(intval($responseKeys["success"]) !== 1) {
		echo '<h2>You are spammer ! Get the @$%K out</h2>';
	} else {


		$password = anti($_POST["password"]);
		$username = anti($_POST["username"]); 

		$data = $sess->login($username, $password,$kode);

		if($data != false)
		{
			$_SESSION[P_SESSIONADMIN] = $data[0];
			header("Location:".P_SLASH.'admin/');
		}
		else
			$error = "Anda salah memasukkan username atau password";


		$error = isset($error) ? $error:'';
	}
}
?>
<link href="<?php echo P_SLASH.P_CSS; ?>signin.css" rel="stylesheet">
<script src='https://www.google.com/recaptcha/api.js'></script> 
<div class="container" id="isi_content">
	<form class="form-signin" action="<?php echo URI; ?>" method="post">
		<div class="error"><?php echo $error; ?></div>
		<h3 class="text-center">Silahkan Sign In</h3>
		<div class="form-group">
			<input type="text" class="form-control" name="username" placeholder="Username" required="required">
		</div>
		<div class="form-group">
			<input type="password" class="form-control" name="password" placeholder="Password" required="required">
		</div>  
		<div class="g-recaptcha" data-sitekey="6LcsHFsUAAAAAG97wvX1Q27phx46UZksih2VxgfM"></div>
		<br>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign In</button>
		 
	</form>
</div>
