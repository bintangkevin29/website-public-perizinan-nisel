<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>

<meta charset="utf-8">
<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="<?php echo P_SLASH; ?>images/logo.jpg" type="image/x-icon">


<title><?php echo P_TITLE; ?></title><link rel="shortcut icon" href="images/logo.png" type="image/x-icon">
<script src="<?php echo P_SLASH.P_JS; ?>jquery.js"></script>
<script src="<?php echo P_SLASH.P_JS; ?>main.js"></script>
<script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>bootstrap/js/jquery.bootstrap.newsbox.min.js"></script>
<link href="<?php echo P_SLASH.P_JS; ?>bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo P_SLASH.P_CSS; ?>template.css" rel="stylesheet">
<link href="<?php echo P_SLASH.P_CSS; ?>elements.css" rel="stylesheet">
<link href="<?php echo P_SLASH.P_LIBRARIES; ?>font-awesome/css/font-awesome.min.css" rel="stylesheet">
</head>

<body id="grve-wrapper">
	<input type="hidden" id="type-page" value="main" />
	<?php 
		include(P_MODULES."menu_componen/menu.php");
		include(P_MODULES."menu_componen/header.php");
		include(P_MODULES."menu_componen/content.php");
	?>
</body>
</html>
