<div class="panel panel-default" id="testimoni" style="width:100%;">
    <div class="panel-heading">TESTIMONI</div>
    <div class="panel-body">
    	<?php
			$d = $sql->sql_query("select nama, jabatan, testimoni from tbl_testimoni where publish='Y' order by id_testimoni asc");
			while($d1 = $sql->sql_fetchrow($d))
			{
			?>
            	<div class="nama_testimoni"><?php echo $d1["nama"]; ?></div>
                <div class="jabatan_testimoni"><?php echo $d1["jabatan"]; ?></div>
                <div class="isi_testimoni"><?php echo $d1["testimoni"]; ?></div>
                <hr>
            <?php
			}
		?>
    </div>
</div>