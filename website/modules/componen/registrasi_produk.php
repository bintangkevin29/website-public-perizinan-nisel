<?php
	if(isset($_POST["nama"]) and isset($_POST["username"]) and isset($_POST["password"]) and isset($_POST["kode"]))
	{
		$password = anti($_POST["password"]);
		$username = anti($_POST["username"]);
    $nama = anti($_POST["nama"]);
		$kode = anti($_POST["kode"]);

		$data = $sess->registrasi($nama, $username, $password, $kode);

		if(is_array($data))
		{
			$_SESSION[P_SESSIONUSER] = $data[0];
			header("Location:".P_SLASH);
		}
		else
			$error = $data;
	}

	$error = isset($error) ? $error:'';
?>
<div class="panel panel-default panel-kiri">
  <div class="panel-heading">REGISTRASI</div>
  <div class="panel-body">
    <link href="<?php echo P_SLASH.P_CSS; ?>signin.css" rel="stylesheet">
	   <form class="form-signin" action="<?php echo URI; ?>" method="post">
       <div class="error"><?php echo $error; ?></div>
    	<h3 class="text-center">Silahkan Registrasi</h3>
    	<div class="form-group">
            <input type="email" class="form-control" name="username" placeholder="E-Mail" required="required">
      </div>
      <div class="form-group">
            <input type="text" class="form-control" name="nama" placeholder="Nama" required="required">
      </div>
      <div class="form-group">
          <input type="password" class="form-control" name="password" placeholder="Password" required="required">
      </div>
      <div class="form-group">
          <div><img src="<?php echo P_SLASH.P_JS; ?>captcha/captcha.php" class="img-responsive" style="width:100%;" /><br></div>
          <input type="text" class="form-control" name="kode" placeholder="Masukkan 8 Kode Di Atas" required="required">
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit">REGISTRASI</button>
    </form>
  </div>
</div>
