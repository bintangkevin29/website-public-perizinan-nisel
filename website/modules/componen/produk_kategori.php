<link rel="stylesheet" type="text/css" href="<?php echo P_SLASH.P_CSS; ?>produk.css">
<?php
	$kategori = strtolower(urldecode(anti($_GET["mode"])));	
	$e = $sql->sql_query("select id_kategori, kategori from eco_kategori where lower(kategori)='$kategori'");
	$e1 = $sql->sql_fetchrow($e);
?>
<div class="panel panel-default panel-kiri">
	<div class="panel-heading"><?php echo $e1["kategori"]; ?></div>
	<div class="panel-body">
			<?php
				$i = 1;
				$d = $sql->sql_query("select a.id_produk, b.file, a.produk, year(a.date_added) as tahun, month(a.date_added) as bulan, harga from eco_produk a inner join eco_produk_gambar b on a.id_produk=b.id_produk where a.id_kategori='".$e1["id_kategori"]."' group by a.id_produk  order by a.date_added desc, b.id_gambar asc  limit 0, 16");
				while($d1 = $sql->sql_fetchrow($d))
				{
					if($i % 4 == 1)
						echo '<div class="row">';
					
					echo '<div class="col-md-3 list-produk">';
						echo '<a href="'.P_SLASH.'produk/'.$d1["tahun"].'/'.$d1["bulan"].'/'.urlencode(strtolower($d1["produk"])).'.html">';
						echo '<div class="list-produk-kotak">';							
							echo '<div align="center"><img src="'.P_SERVERNAME.$d1["file"].'" class="img-responsive" /></div>';
							echo '<div class="list-produk-title">'.$d1["produk"].'</div>';
							

							$c = $sql->sql_query("select diskon from eco_produk_diskon where tanggal<=date(now()) and tanggal1>=date(now()) and id_produk='".$d1["id_produk"]."'");
							if($sql->sql_numrows($c))
							{
								$c1 = $sql->sql_fetchrow($c);
								
								echo '<div class="list-produk-diskon">Rp '.number_format($d1["harga"],0,',','.').'</div>';		
								echo '<div class="list-produk-price">Rp '.number_format($d1["harga"] - (($d1["harga"]/100)*$c1["diskon"]),0,',','.').'</div>';

								echo '<div class="a-label-diskon">';
									echo '<div class="label-diskon">'.$c1["diskon"].'%</div>';
								echo '</div>';	
							}else
							{
								echo '<div class="list-produk-diskon"></div>';		
								echo '<div class="list-produk-price">Rp '.number_format($d1["harga"],0,',','.').'</div>';
							}

						echo '</div>';
						echo '</a>';
					echo '</div>';
					
					if($i % 4 == 0)
						echo '</div>';
					$i++;
				}
				
				if($i-1 % 4 != 0)
					echo '</div>';
			?>
	</div>
</div>