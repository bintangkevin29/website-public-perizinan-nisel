<div class="panel panel-default">
<script type="text/javascript">
function addsmiley(code)  {
    var pretext = document.forms['shoutbox_form'].pesan.value;
      this.code = code;
      document.forms['shoutbox_form'].pesan.value = pretext + code;
  }
</script>
<?php

	if(isset($_POST["shoutbox_ip"]))
	{
		$ip_address = $_SERVER["REMOTE_ADDR"];
		$nama = anti($_POST["nama"]);
		$email = anti($_POST["email"]);
		$phone = anti($_POST["hp"]);
		$pesan = anti($_POST["pesan"]);
		$kode = anti($_POST["kode"]);

		
		if($nama == '' or $email == '' or $phone == '' or $pesan == '' or $kode == '')
			$error = 'Isi form dengan lengkap';
		elseif($_SESSION["captcha_session"] != $kode)
			$error = 'Masukkan kode captcha dengan benar';
		else
		{
			$c = $sql->sql_query("select id_shout from tbl_shout where ip_address='$ip_address' and date(waktu)=curdate()");
			if($sql->sql_numrows($c) > 1)
				$error = 'Pada hari ini Anda sudah mengisi pesan 2 kali';
			else
			{
				$c = $sql->sql_query("insert into tbl_shout (nama, email, phone, pesan, ip_address) values ('$nama','$email','$phone','$pesan','$ip_address')");
				if($c)
				{ 
					header("Location: http://disnaker.pemkomedan.go.id/nisel/website/plugin/kontak/pesan_hasil.html");
				}
				else
					$error = 'Gagal';
			}
		}		
	}
	
	$error = isset($error) ? $error : '';
	
	
?>
	<link href="<?php echo P_SLASH.P_CSS; ?>shoutbox.css" rel="stylesheet">
	<div class="panel-heading">Pesan dan Saran</div><br />
    	<div class="error"><?php echo $error; ?></div>
        <form action="<?php echo URI; ?>" method="post" class="form-horizontal" name="shoutbox_form">
        	<div class="form-group">
                <label class="col-sm-3 control-label"><span class="required">*</span>Nama : </label>
                <div class="col-sm-7">                	
                    <input type="text" name="nama" class="form-control" required="required" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><span class="required">*</span>E-Mail : </label>
                <div class="col-sm-7">                	
                    <input type="email" name="email" class="form-control" required="required" placeholder="E-Mail (Tidak akan dipublish)" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><span class="required">*</span>No. Handphone : </label>
                <div class="col-sm-7">                	
                    <input type="text" name="hp" class="form-control" required="required" placeholder="No. Handphone (Tidak akan dipublish)" />
                </div>
            </div>
            <div class="form-group" style="margin-bottom:4px;">
            	<label class="col-sm-3 control-label"><span class="required">*</span>Pesan : </label>
                <div class="col-sm-7">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Ambivalent.png" title="Ambivalent" style="cursor:pointer;" onclick="addsmiley('::Ambivalent::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Angel.png" title="Angel" style="cursor:pointer;" onclick="addsmiley('::Angel::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Angry.png" title="Angry" style="cursor:pointer;" onclick="addsmiley('::Angry::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Blush.png" title="Blusht" style="cursor:pointer;" onclick="addsmiley('::Blush::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Confused.png" title="Confused" style="cursor:pointer;" onclick="addsmiley('::Confused::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Crazy.png" title="Crazy" style="cursor:pointer;" onclick="addsmiley('::Crazy::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Crying.png" title="Crying" style="cursor:pointer;" onclick="addsmiley('::Crying::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Foot In Mouth.png" title="Foot In Mouth" style="cursor:pointer;" onclick="addsmiley('::Foot In Mouth::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Frown.png" title="Frown" style="cursor:pointer;" onclick="addsmiley('::Frown::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Gasp.png" title="Gasp" style="cursor:pointer;" onclick="addsmiley('::Gasp::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Grin.png" title="Grin" style="cursor:pointer;" onclick="addsmiley('::Grin::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Halo.png" title="Halo" style="cursor:pointer;" onclick="addsmiley('::Halo::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Heart.png" title="Heart" style="cursor:pointer;" onclick="addsmiley('::Heart::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Hot.png" title="Hot" style="cursor:pointer;" onclick="addsmiley('::Hot::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Kiss.png" title="Kiss" style="cursor:pointer;" onclick="addsmiley('::Kiss::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/LargeGasp.png" title="LargeGasp" style="cursor:pointer;" onclick="addsmiley('::LargeGasp::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Laugh.png" title="Laugh" style="cursor:pointer;" onclick="addsmiley('::Laugh::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Lips are Sealed.png" title="Lips are Sealed" style="cursor:pointer;" onclick="addsmiley('::Lips are Sealed::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Money-mouth.png" title="Money-mouth" style="cursor:pointer;" onclick="addsmiley('::Money-mouth::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Naughty.png" title="Naughty" style="cursor:pointer;" onclick="addsmiley('::Naughty::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Nerd.png" title="Nerd" style="cursor:pointer;" onclick="addsmiley('::Nerd::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/ohnoes.png" title="ohnoes" style="cursor:pointer;" onclick="addsmiley('::ohnoes::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Pirate.png" title="Pirate" style="cursor:pointer;" onclick="addsmiley('::Pirate::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Sarcastic.png" title="Sarcastic" style="cursor:pointer;" onclick="addsmiley('::Sarcastic::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Sick.png" title="Sick" style="cursor:pointer;" onclick="addsmiley('::Sick::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Smile.png" title="Smile" style="cursor:pointer;" onclick="addsmiley('::Smile::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Sticking Out Tongue.png" title="Sticking Out Tongue" style="cursor:pointer;" onclick="addsmiley('::Sticking Out Tongue::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Thumbs Down.png" title="Thumbs Down" style="cursor:pointer;" onclick="addsmiley('::Thumbs Down::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Thumbs Up.png" title="Thumbs Up" style="cursor:pointer;" onclick="addsmiley('::Thumbs Up::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Undecided.png" title="Undecided" style="cursor:pointer;" onclick="addsmiley('::Undecided::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/VeryAngry.png" title="VeryAngry" style="cursor:pointer;" onclick="addsmiley('::VeryAngry::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Wink.png" title="Wink" style="cursor:pointer;" onclick="addsmiley('::Wink::')">
                    <img src="<?php echo P_SLASH; ?>picture/smileys/Yum.png" title="Yum" style="cursor:pointer;" onclick="addsmiley('::Yum::')">
                </div>                
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-7">
                    <textarea name="pesan" class="form-control" style="height:100px;" required="required"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><span class="required">*</span>Captcha : </label>
                <div class="col-sm-7">
                    <img src="<?php echo P_SLASH.P_JS; ?>captcha/captcha.php">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-7">
                    <input type="text" maxlength="8" name="kode" class="form-control" placeholder="Masukkan 8 Kode Di Atas" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-7">
                    <input type="submit" class="btn btn-primary" value="KIRIM">
		            <input type="submit" class="btn btn-default" value="RESET">
                </div>
            </div>			
            
            <input type="hidden" name="shoutbox_ip" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>">
        </form>
<br>
<br>
<?php
	$d = mysql_query("select count(*) as jumlah from tbl_shout");
	$d1 = mysql_fetch_array($d);
	$jumlah = $d1["jumlah"];
	
	$posisi = 0;
	$batas = 10;
	
	if(isset($_GET["page"]))
	{
		if((int)$_GET["page"] != 0)
			$posisi = ($_GET["page"] -1)*$batas;
	}		
?>
<div style="margin-left:10px; margin-right:10px;">
<hr color=#e0cb91 noshade=noshade />
<?php
	function replace_emotion($text)
	{
		$text = str_replace('', '', $text);
		$text = str_replace('::Ambivalent::', '<img src="'.P_SLASH.'picture/smileys/Ambivalent.png" title="Ambivalent">', $text);
		$text = str_replace('::Angel::', '<img src="'.P_SLASH.'picture/smileys/Angel.png" title="Angel">', $text);
		$text = str_replace('::Angry::', '<img src="'.P_SLASH.'picture/smileys/Angry.png" title="Angry">', $text);
		$text = str_replace('::Blush::', '<img src="'.P_SLASH.'picture/smileys/Blush.png" title="Blusht">', $text);
		$text = str_replace('::Confused::', '<img src="'.P_SLASH.'picture/smileys/Confused.png" title="Confused">', $text);
		$text = str_replace('::Crazy::', '<img src="'.P_SLASH.'picture/smileys/Crazy.png" title="Crazy">', $text);
		$text = str_replace('::Crying::', '<img src="'.P_SLASH.'picture/smileys/Crying.png" title="Crying">', $text);
		$text = str_replace('::Foot In Mouth::', '<img src="'.P_SLASH.'picture/smileys/Foot In Mouth.png" title="Foot In Mouth">', $text);
		$text = str_replace('::Frown::', '<img src="'.P_SLASH.'picture/smileys/Frown.png" title="Frown">', $text);
		$text = str_replace('::Gasp::', '<img src="'.P_SLASH.'picture/smileys/Gasp.png" title="Gasp">', $text);
		$text = str_replace('::Grin::', '<img src="'.P_SLASH.'picture/smileys/Grin.png" title="Grin">', $text);
		$text = str_replace('::Halo::', '<img src="'.P_SLASH.'picture/smileys/Halo.png" title="Halo">', $text);
		$text = str_replace('::Heart::', '<img src="'.P_SLASH.'picture/smileys/Heart.png" title="Heart">', $text);
		$text = str_replace('::Hot::', '<img src="'.P_SLASH.'picture/smileys/Hot.png" title="Hot">', $text);
		$text = str_replace('::Kiss::', '<img src="'.P_SLASH.'picture/smileys/Kiss.png" title="Kiss">', $text);
		$text = str_replace('::LargeGasp::', '<img src="'.P_SLASH.'picture/smileys/LargeGasp.png" title="LargeGasp">', $text);
		$text = str_replace('::Laugh::', '<img src="'.P_SLASH.'picture/smileys/Laugh.png" title="Laugh">', $text);
		$text = str_replace('::Lips are Sealed::', '<img src="'.P_SLASH.'picture/smileys/Lips are Sealed.png" title="Lips are Sealed">', $text);
		$text = str_replace('::Money-mouth::', '<img src="'.P_SLASH.'picture/smileys/Money-mouth.png" title="Money-mouth">', $text);
		$text = str_replace('::Naughty::', '<img src="'.P_SLASH.'picture/smileys/Naughty.png" title="Naughty">', $text);
		$text = str_replace('::Nerd::', '<img src="'.P_SLASH.'picture/smileys/Nerd.png" title="Nerd">', $text);
		$text = str_replace('::ohnoes::', '<img src="'.P_SLASH.'picture/smileys/ohnoes.png" title="ohnoes">', $text);
		$text = str_replace('::Pirate::', '<img src="'.P_SLASH.'picture/smileys/Pirate.png" title="Pirate">', $text);
		$text = str_replace('::Sarcastic::', '<img src="'.P_SLASH.'picture/smileys/Sarcastic.png" title="Sarcastic">', $text);
		$text = str_replace('::Sick::', '<img src="'.P_SLASH.'picture/smileys/Sick.png" title="Sick">', $text);
		$text = str_replace('::Smile::', '<img src="'.P_SLASH.'picture/smileys/Smile.png" title="Smile">', $text);
		$text = str_replace('::Sticking Out Tongue::', '<img src="'.P_SLASH.'picture/smileys/Sticking Out Tongue.png" title="Sticking Out Tongue">', $text);
		$text = str_replace('::Thumbs Down::', '<img src="'.P_SLASH.'picture/smileys/Thumbs Down.png" title="Thumbs Down">', $text);
		$text = str_replace('::Thumbs Up::', '<img src="'.P_SLASH.'picture/smileys/Thumbs Up.png" title="Thumbs Up">', $text);
		$text = str_replace('::Undecided::', '<img src="'.P_SLASH.'picture/smileys/Undecided.png" title="Undecided">', $text);
		$text = str_replace('::VeryAngry::', '<img src="'.P_SLASH.'picture/smileys/VeryAngry.png" title="VeryAngry">', $text);
		$text = str_replace('::Wink::', '<img src="'.P_SLASH.'picture/smileys/Wink.png" title="Wink">', $text);
		$text = str_replace('::Yum::', '<img src="'.P_SLASH.'picture/smileys/Yum.png" title="Yum">', $text);
		
		return $text;
	}
	
	$d = mysql_query("select nama, pesan, date(waktu) as tanggal, time(waktu) as waktu from tbl_shout order by waktu desc limit $posisi, $batas");
	while($d1 = mysql_fetch_array($d))
	{
		$tanggal = tgl_indo($d1["tanggal"]);
	?>
		
    <?php
	}
	if($jumlah > $batas)
	{
		include(P_INCLUDES.'paging.php');
		doPages($batas, 'plugin/kontak/pesan_saran', '', $jumlah);
	}
?>
</div>
</div>