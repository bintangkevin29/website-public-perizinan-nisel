<div class="panel-heading"><h1 class="panel-title text-left">Kurs BCA</h1></div>
<div class="panel-body">
<?php
	$data = json_decode(file_get_contents('http://www.adisurya.net/kurs-bca/get'));
	$tgl = explode(" ", $data->LastUpdate);	
?>
<table class="table table-striped table-responsive">
	<thead>
    	<tr>
        	<th colspan="4" class="text-center"><?php echo tgl_indo($tgl[0]).' - '.$tgl[1].' WIB'; ?></th>
        </tr>
        <tr>
        	<th class="text-center">No</th>
        	<th class="text-center">Mata Uang</th>
            <th class="text-center">Jual</th>
            <th class="text-center">Beli</th>
        </tr>
    </thead>
    <tbody>
    	<?php
			$i=1;
			foreach($data->Data as $uang => $nilai)
			{
			?>
            	<tr>
                	<td class="text-center"><?php echo $i; ?></td>
                	<td class="text-center"><?php echo $uang; ?></td>
                    <td class="text-right"><?php echo number_format($nilai->Jual, 2, ',','.'); ?></td>
                    <td class="text-right"><?php echo number_format($nilai->Beli, 2, ',','.'); ?></td>
                </tr>
            <?php
				$i++;
			}
		?>
    </tbody>
</table>
	<p>Sumber: www.bca.co.id</p>
</div>