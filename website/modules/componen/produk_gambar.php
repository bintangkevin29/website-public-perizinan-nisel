 <!-- Include Cloud Zoom JavaScript -->
<script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>cloudzoom/cloudzoom.js"></script>
                
<!-- Include Thumbelina JavaScript -->
<script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>cloudzoom/thumbelina.js"></script>
        
                
<!-- Include Cloud Zoom CSS -->
<link href="<?php echo P_SLASH.P_JS; ?>cloudzoom/cloudzoom.css" type="text/css" rel="stylesheet" />
                
<!-- Include Thumbelina CSS -->
<link href="<?php echo P_SLASH.P_JS; ?>cloudzoom/thumbelina.css" type="text/css" rel="stylesheet" />


<script type = "text/javascript">
CloudZoom.quickStart();
            
// Initialize the slider.
$(function(){
	$('#slider1').Thumbelina({
    	$bwdBut:$('#slider1 .left'), 
        $fwdBut:$('#slider1 .right')
	});
});

</script>
        
<style>
            
/* div that surrounds Cloud Zoom image and content slider. */
#surround {
	width:80%;
    min-width: 256px;
    max-width: 480px;
	margin-left:20px;
	margin-top:20px;
}
            
/* Image expands to width of surround */
img.cloudzoom {
	width:100%;
}
            
            /* CSS for slider - will expand to width of surround */
            #slider1 {
                margin-left:20px;
                margin-right:20px;
                height:119px;
                border-top:1px solid #aaa;
                border-bottom:1px solid #aaa;
                position:relative;
            }
			
			#slider1 img{
				width:100px;
				height:100px;
			}
            
        </style>
        <script type="text/javascript">
            
             // The following piece of code can be ignored.
             $(function(){
                 $(window).resize(function() {
                     $('#info').text("Page width: "+$(this).width());
                 });
                 $(window).trigger('resize');
             });
             
            // The following piece of code can be ignored.
            if (window.location.hostname.indexOf("starplugins.") != -1) {
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-254857-7']);
                _gaq.push(['_trackPageview']);
                (function() {
                    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                })();
            }
             
        </script>



<div id="surround">
    
    <?php
		$gambar="";
		$i=1;
		$c = $sql->sql_query("select file from eco_produk_gambar where id_produk='".$d1["id_produk"]."'");
		while($c1 = $sql->sql_fetchrow($c))					
		{
			$thumb = str_replace("images/",".thumbs/images/", $c1["file"]);
			$gambar .= '<li>';
			$gambar .= '<li><img class="cloudzoom-gallery" src="'.P_SERVERNAME.$thumb.'" 
            	data-cloudzoom ="useZoom:\'.cloudzoom\', image:\''.P_SERVERNAME.$c1["file"].'\' "></li>';
			$gambar .= '</li>';
			
			if($i==1)
			{
				echo '<img class="cloudzoom" alt ="Cloud Zoom small image" id ="zoom1" src="'.P_SERVERNAME.$c1["file"].'" data-cloudzoom=\'
           				zoomSizeMode:"image",
			           autoInside: 550
				       \'>';
			}
			
			$i++;
		}
	?>
 
    <div id="slider1">
        <div class="thumbelina-but horiz left">&#706;</div>
        <ul>
			<?php echo $gambar; ?>
        </ul>
        <div class="thumbelina-but horiz right">&#707;</div>
    </div>
    
</div>
