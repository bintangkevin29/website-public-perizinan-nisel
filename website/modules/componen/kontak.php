<?php
	if(isset($_POST["validasi"]))	
	{
		if(anti($_POST["validasi"]) == md5(session_id()))
		{
			$nama = anti($_POST["nama"]);
			$email = anti($_POST["email"]);
			$isi = anti($_POST["isi"]);
			$hp = anti($_POST["hp"]);
			$kirim = anti($_POST["kirim"]);
			
			if($nama == '' or $email == '' or $isi == '')
			{
				pesan('isi form dengan lengkap', URI);
				exit();
			}
			
			if(cek_email($email) == false)
			{
				pesan('email tidak valid', URI);
				exit();
			}
			
			if(anti($_POST["kode"]) != $_SESSION["captcha_session"])
			{
				pesan('kode yang dimasukkan tidak sama', URI);
				exit();
			}
			
			$d = $sql->sql_query("insert into tbl_hasilkontak (nama, email, isi, waktu, phone, ip_address) values ('$nama','$email','$isi',now(),'$hp','".$_SERVER["REMOTE_ADDR"]."')");
			if($d)
			{
				$c = $sql->sql_query("select * from tbl_kontak where id_kontak='$kirim'");
				$c1 = $sql->sql_fetchrow($c);
				
				include(P_LIBRARIES."phpmailer_pi.php");
				send_email_smtp($email, $nama, $c1["email"], $c1["kontak"], 'KONTAK SAYA', $isi);
				berhasil(URI);
			}
			else
				gagal(URI);
		}
	}
?>
<h3 class="grve-element grve-title-line" style="font-weight: bold;">KONTAK KAMI</h3>
<form action="<?php echo URI; ?>" method="post" class="form-horizontal">
	<input type="hidden" name="validasi" value="<?php echo md5(session_id()); ?>" />
    <div class="form-group">
		<label class="col-sm-3 control-label"><span class="required">*</span>Kirim Ke : </label>
        <div class="col-sm-7">                	
			<select name="kirim" class="form-control" required="required">
            	<option value="">Pilih Kirim Ke</option>
                <?php
					$c = $sql->sql_query("select * from tbl_kontak where publish='Y' order by kontak asc");
					while($c1 = $sql->sql_fetchrow($c))
					{
						echo '<option value="'.$c1["id_kontak"].'">'.$c1["kontak"].'</option>';						
					}
				?>
            </select>
        </div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label"><span class="required">*</span>Nama : </label>
        <div class="col-sm-7">                	
			<input type="text" name="nama" class="form-control" required="required" />
        </div>
	</div>
    <div class="form-group">
		<label class="col-sm-3 control-label"><span class="required">*</span>E-Mail : </label>
        <div class="col-sm-7">                	
			<input type="email" name="email" class="form-control" required="required" placeholder="E-mail tidak akan dipublish" />
        </div>
	</div>
    <div class="form-group">
		<label class="col-sm-3 control-label"><span class="required">*</span>No. Handphone : </label>
        <div class="col-sm-7">                	
			<input type="text" name="hp" class="form-control" required="required" placeholder="No. Handphone tidak akan dipublish" />
        </div>
	</div>
    <div class="form-group">
		<label class="col-sm-3 control-label"><span class="required">*</span>Pesan : </label>
        <div class="col-sm-7">
        	<textarea name="isi" class="form-control" style="height:100px;" required="required"></textarea>
        </div>
	</div>
    <div class="form-group">
		<label class="col-sm-3 control-label"><span class="required">*</span>Captcha : </label>
        <div class="col-sm-7">
        	<img src="<?php echo P_SLASH.P_JS; ?>captcha/captcha.php">
        </div>
	</div>
    <div class="form-group">
		<label class="col-sm-3 control-label"></label>
        <div class="col-sm-7">
        	<input type="text" maxlength="8" name="kode" class="form-control" placeholder="Masukkan 8 Kode Di Atas" />
        </div>
	</div>
    <div class="form-group">
		<label class="col-sm-3 control-label"></label>
        <div class="col-sm-7">
        	<button type="submit" class="btn btn-primary">KIRIM</button>
            <button type="reset" class="btn btn-default">RESET</button>
        </div>
	</div>



<br>
<br>
<br>
    <br>
</form>
