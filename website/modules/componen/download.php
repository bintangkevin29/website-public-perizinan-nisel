<?php
class download extends sql_db{
	function show_kategori($mode)
	{
		echo '<h3 class="grve-element grve-title-line" style="font-weight: bold;">'.strtoupper(urldecode($mode)).'</h3>';
		$d = parent::sql_query("select id_kat_download from tbl_kat_download where nama='$mode' and publish='Y' order by waktu desc");
		$d1 = parent::sql_fetchrow($d);
		$id_kat_download = $d1["id_kat_download"];

		echo '<div class="list-content">';
		$d = parent::sql_query("select gambar, nama_file, nama from tbl_download where id_kat_download='$id_kat_download' and publish='Y' order by waktu desc");
		echo '<ul class="list-content">';
		while($d1 = parent::sql_fetchrow($d))
		{
		?>
        	<li>
            	<a href="<?php echo P_SERVERNAME.$d1["nama_file"]; ?>" target="_blank">	            	
					<?php echo $d1["nama"]; ?>
                </a>
            </li>
        <?php
		}
		echo '</ul>';


		$d = parent::sql_query("select year(waktu) as tahun, month(waktu) as bulan, nama, id_kat_download from tbl_kat_download where id_connect='$id_kat_download' and publish='Y' order by waktu desc");

		echo '<ul class="list-content">';
		while($d1 = parent::sql_fetchrow($d))
		{			
		?>
			<li>
               <a href="<?php echo P_SLASH.'plugin/download/'.$d1["tahun"].'/'.$d1["bulan"].'/'.urlencode($d1["nama"]); ?>.html">         
                    <?php echo $d1["nama"]; ?>
               </a>
            </li>
        <?php
		}
		echo '</ul>';
		echo '</div>';
	}

	function show_download($tahun, $bulan, $kat_download)
	{

		echo '<h3 class="grve-element grve-title-line" style="font-weight: bold;">'.strtoupper(urldecode($kat_download)).'</h3>';
		$d = parent::sql_query("select id_kat_download from tbl_kat_download where nama='$kat_download' and year(waktu)='$tahun' and month(waktu)='$bulan'");
		$d1 = parent::sql_fetchrow($d);
		$id_kat_download = $d1["id_kat_download"];

		
		echo '<div class="list-content">';
		$d = parent::sql_query("select gambar, nama_file, nama from tbl_download where id_kat_download='$id_kat_download' and publish='Y' order by waktu desc");
		echo '<ul>';
		while($d1 = parent::sql_fetchrow($d))
		{
		?>
        	<li>
            	<a href="<?php echo P_SERVERNAME.P_SLASH.$d1["nama_file"]; ?>" target="_blank">	            	
					<?php echo $d1["nama"]; ?>
                </a>
            </li>
        <?php
		}
		echo '</ul>';
		echo '</div>';
	}

	function show_all()
	{
		echo '<h3 class="grve-element grve-title-line" style="font-weight: bold;">KATEGORI FILE DOWLOAD</h3>';

		echo '<div class="list-content">';
		$d = parent::sql_query("select * from tbl_kat_download where publish='Y' order by waktu desc");
		echo '<ul class="list-content">';
		while($d1 = parent::sql_fetchrow($d))
		{
		?>
        	<li>
            	<a href="<?php echo P_SLASH.P_PLUGIN.'download/'.urlencode($d1["nama"]); ?>.html">	            	
					<?php echo strtoupper($d1["nama"]); ?>
                </a>
            </li>
        <?php
		}
		echo '</ul>';
		echo '</div>';
	}
}

$download = new download;
if(isset($_GET["tahun"]) and isset($_GET["bulan"]) and isset($_GET["mode"]))
	//echo  "a";
	$download->show_download(anti($_GET["tahun"]), anti($_GET["bulan"]), anti(urldecode($_GET["mode"])));
elseif(isset($_GET["mode"]))
	//echo "b";
	$download->show_kategori(anti(urldecode($_GET["mode"])));
else
	//echo "c";
	$download->show_all();
?>