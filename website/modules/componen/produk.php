<?php
	$tahun = anti($_GET["tahun"]);
	$bulan = anti($_GET["bulan"]);
	$produk = anti(urldecode($_GET["title"]));
	
	$d = $sql->sql_query("select a.id_produk, a.produk, a.harga, b.kategori, a.deskripsi from eco_produk a inner join eco_kategori b on a.id_kategori=b.id_kategori where year(a.date_added)='$tahun' and month(a.date_added)='$bulan' and lower(a.produk)='$produk'");
	$d1 = $sql->sql_fetchrow($d);
	
?>
<div class="panel panel-default panel-kiri">
	<div class="panel-heading"><?php echo $d1["kategori"].' - '.$d1["produk"]; ?></div>
    <div class="panel-default">
    	<div class="row">
        	<div class="col-md-6">                                               
            	<?php include(P_MODULES."componen/produk_gambar.php"); ?>
            </div>
            <div class="col-md-6">
            	<?php include(P_MODULES."componen/produk_beli.php"); ?>
            </div>
        </div>
        <div class="row" style="margin: 30px 10px 20px 10px;">
            <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#sectionA">Deskripsi</a></li>                    
                    <li><a data-toggle="tab" href="#sectionB">Spesifikasi</a></li>
                    <li><a data-toggle="tab" href="#sectionC">Testimoni</a></li>
                </ul>
                <div class="tab-content" style="border:solid 1px #CCCCCC; border-top:none; padding-top:10px;">
                    <div id="sectionA" class="tab-pane fade in active">
                        <?php include(P_MODULES.'componen/produk_deskripsi.php'); ?>
                    </div>
                    <div id="sectionB" class="tab-pane fade">
                        <?php include(P_MODULES.'componen/produk_spesifikasi.php'); ?>
                    </div>
                    <div id="sectionC" class="tab-pane fade">
                        <?php include(P_MODULES.'componen/produk_testimoni.php'); ?>
                    </div>
                </div>
        </div>        
    </div>
</div>