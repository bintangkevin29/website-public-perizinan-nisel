<?php
	//include(P_MODULES.'mod_utama/menu_nonlogin.php');

	if(isset($_POST["username"]) and isset($_POST["password"]) and isset($_POST["kode"]))
	{
		$password = anti($_POST["password"]);
		$username = anti($_POST["username"]);
		$kode = anti($_POST["kode"]);

		$data = $sess->login($username, $password, $kode);

		if($data != false)
		{
			$_SESSION[P_SESSIONUSER] = $data[0];
			header("Location:".P_SLASH.P_PLUGIN.'output/login.html');
		}
		else
			$error = "Anda salah memasukkan username atau password";
	}

	$error = isset($error) ? $error:'';
?>
<link href="<?php echo P_SLASH.P_CSS; ?>signin.css" rel="stylesheet">
<div id="isi_content">
	<form class="form-signin" action="<?php echo URI; ?>" method="post">
    	<div class="error"><?php echo $error; ?></div>
    	<h3 class="text-center">Silahkan Sign In</h3>
    	<div class="form-group">
            <input type="text" class="form-control" name="username" placeholder="Username" required="required">
        </div>
        <div class="form-group">
            <input type="password" class="form-control" name="password" placeholder="Password" required="required">
        </div>
        <div class="form-group">
            <div><img src="<?php echo P_SLASH.P_JS; ?>captcha/captcha.php" class="img-responsive" style="width:100%;" /><br></div>
            <input type="text" class="form-control" name="kode" placeholder="Masukkan 8 Kode Di Atas" required="required">
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign In</button>
      </form>

      <br>
</div>
