<h3 class="grve-element grve-title-line" style="font-weight: bold;">OUTPUT PIMPINAN</h3>

<?php
	if($sess->cek_session(P_SESS))
	{
?>
		<div class="list-content">
			<ul>
				<li><a href="../<?php echo P_SLASH; ?>website_lama/pimpinan-output-ikm" target="_blank">INDEKS KEPUASAN MASYARAKAT</a></li>
			    <li><a href="../<?php echo P_SLASH; ?>website_lama/pimpinan-ouput-pengaduan" target="_blank">PENGADUAN MASYARAKAT</a></li>
			    <hr />
			    <li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=data_potensial_lap" target="_blank">LAPORAN DATA POTENSIAL</a></li>
			    <li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=data_perusahaan_lap" target="_blank">LAPORAN DATA PERUSAHAAN</a></li>
			    <li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=resi_lap" target="_blank">LAPORAN NOMOR RESI</a></li>
			    <li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=lbr_kendali_lap" target="_blank">LAPORAN LEMBAR KENDALI</a></li>
			    <li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=berkas_lengkap_lap" target="_blank">LAPORAN BERKAS YANG HARUS DILENGKAPI</a></li>
			    <li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=berkas_sudah_lap" target="_blank">LAPORAN BERKAS YANG SUDAH DILENGKAPI</a></li>
			    <li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=berkas_serah_lap" target="_blank">LAPORAN BERKAS SELESAI YANG BELUM DIAMBIL</a></li>
				<li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=berkas_selesai_lap" target="_blank">LAPORAN BERKAS YANG SUDAH SELESAI</a></li>
			    <li><a href="<?php echo P_SLASH; ?>website_lama/output.php?mode=berkas_proses_lap" target="_blank">LAPORAN BERKAS DALAM PROSES PENGURUSAN</a></li> 
			    <li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=berkas_monitor" target="_blank">LIHAT PROSES PENGURUSAN</a></li>    
			    <li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=berkas_arsip_lap" target="_blank">LAPORAN BERKAS DALAM PROSES PENGARSIPAN</a></li> 
				<li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=berkas_arsip_sudah_lap" target="_blank">LAPORAN BERKAS YANG SUDAH DIARSIPKAN</a></li>       
				<li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=dpkk_lap" target="_blank">LAPORAN JUMLAH SETORAN DPKK</a></li>   
				<li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=tenaga_lap" target="_blank">LAPORAN JUMLAH TENAGA KERJA</a></li>          
				<li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=imta_lap" target="_blank">LAPORAN PERPANJANGAN IMTA</a></li>       
				<li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=imta_habis_lap" target="_blank">LAPORAN IMTA YANG BELUM MEMPERPANJANG</a></li>       
				<li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=wl_lap" target="_blank">LAPORAN SK WAJIB LAPOR</a></li>       
				<li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=wl_habis_lap" target="_blank">LAPORAN SK WAJIB LAPOR YANG BELUM MELAPOR</a></li>            
				<li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=pengaduan_lap" target="_blank">LAPORAN BERKAS PENGADUAN YANG MASUK</a></li>       
				<li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=pengaduan_proses_lap" target="_blank">LAPORAN BERKAS PENGADUAN YANG SEDANG PROSES</a></li>       
				<li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=pengaduan_mediasi_lap" target="_blank">LAPORAN BERKAS PENGADUAN DALAM PROSES MEDIASI</a></li>    
				<li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=pengaduan_mediator_lap" target="_blank">LAPORAN BERKAS PENGADUAN YANG DITANGANI MEDIATOR</a></li>        
				<li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=pengaduan_selesai_lap" target="_blank">LAPORAN BERKAS PENGADUAN YANG TELAH SELESAI</a></li> 

			    <li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=pp_lap" target="_blank">LAPORAN BERKAS PP/PKB YANG MASUK</a></li> 
			    <li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=pp_proses_lap" target="_blank">LAPORAN BERKAS PP/PKB YANG SEDANG PROSES</a></li> 
			    <li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=pp_selesai_lap" target="_blank">LAPORAN BERKAS PP/PKB YANG TELAH SELESAI</a></li> 

			    <li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=iplk_lap" target="_blank">LAPORAN BERKAS IPLK YANG MASUK</a></li> 
			    <li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=iplk_proses_lap" target="_blank">LAPORAN BERKAS IPLK YANG SEDANG PROSES</a></li> 
			    <li><a href="../<?php echo P_SLASH; ?>website_lama/output.php?mode=iplk_selesai_lap" target="_blank">LAPORAN BERKAS IPLK YANG TELAH SELESAI</a></li>      

			    <hr />
			    <li><a href="<?php echo P_SLASH.P_PLUGIN; ?>output/logout.html">LOGOUT</a></li>
			</ul>
		</div>
<?php
	}
	else
	{
		include(P_MODULES.'componen/output/login2.php');
	}
?>