<h3 class="grve-element grve-title-line" style="font-weight: bold;">INFORMASI PROSES PENGURUSAN</h3>
<br>
<?php
	$sql->sql_close();

	include(P_INCLUDES.'sql_db1.php');
	$sql = new sql_db1;

	$resi = anti($_POST["resi"]);	
	if ($resi == '')
	{
		pesan('Isi nomor resi !', P_SLASH.P_PLUGIN.'output/proses.html');
		exit();		
	}	

	$q_info = $sql->sql_query("select pengurusan,id_proses_skrg from vw_info_berkas where no_resi='$resi'");
	$hsl = $sql->sql_fetchrow($q_info);
	
	$pengurusan=$hsl["pengurusan"];
	$id_keg=$hsl["id_proses_skrg"];
	
	$q_pos = $sql->sql_query("select id_kegiatan from tbl_alur_baru where id_proses='$id_keg'");
	$hsl = $sql->sql_fetchrow($q_pos);
	
	$posisi=$hsl["id_kegiatan"];
	if($posisi!='' and $pengurusan!='')
	{	
?>

<table class="table table-bordered">
    <tr id="tbl-content">
    	<td width="20%">NO RESI</td>
        <td><?php echo $resi;?></td>
    </tr>
    <tr id="tbl-content">
    	<td>PENGURUSAN</td>
        <td><?php echo $pengurusan;?></td>
    </tr> 
    <tr id="tbl-content">
    	<td>POSISI BERKAS</td>
        <td><?php echo $posisi;?></td>
    </tr>   
     
</table>
<?php
	}
	else
	{
		echo "Data tidak ditemukan";
	}
?>
<div align="right"><a href="<?php echo P_SLASH.P_PLUGIN; ?>output/proses.html">KEMBALI</a></div>
<?php
	$sql->sql_close();
	$sql = new sql_db;
?>
