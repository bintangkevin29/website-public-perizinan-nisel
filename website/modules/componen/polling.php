<div class="panel panel-default">
	<div class="panel-heading">Hasil Polling</div>
    <div class="panel-body">
    <?php
		if(isset($_POST["kat_polling"]))
		{		
			$polling = anti($_POST["polling"]);	
			$ip_address = $_SERVER["REMOTE_ADDR"];
			
			if($polling == '')
				$error = 'Isi form dengan lengkap';
			else
			{
				$d = $sql->sql_query("select id_kat_polling, kat_polling from tbl_kat_polling where publish='Y'");
				$d1 = $sql->sql_fetchrow($d);
				$id_kat_polling=$d1["id_kat_polling"];
				
				$d = $sql->sql_query("select id_polling from tbl_polling where publish='Y' and id_kat_polling='$id_kat_polling' and polling='$polling'");
				$d1 = $sql->sql_fetchrow($d);
				$polling=$d1["id_polling"];
			
				$d = $sql->sql_query("select id_jawaban from tbl_jawaban_polling where ip_address='$ip_address' and date(waktu)=curdate() and id_kat_polling='$id_kat_polling'");
				if($sql->sql_numrows($d)>0)
					$error = 'Dalam satu hari Anda hanya bisa melakukan polling satu kali';
				else
				{
					$d = $sql->sql_query("insert into tbl_jawaban_polling (id_kat_polling, id_polling, ip_address) values ('$id_kat_polling','$polling','$ip_address')");
					
					if($d)
						$error = 'Berhasil melakukan polling';
					else
						$error = 'Gagal melakukan polling';
				}
			}
		}
		
		$error = isset($error) ? $error : '';
	?>
    <div class="error"><?php echo $error; ?></div>
    <?php
		$d = $sql->sql_query("select id_kat_polling, kat_polling from tbl_kat_polling where publish='Y'");
		$d1 = $sql->sql_fetchrow($d);
		$id_kat_polling=$d1["id_kat_polling"];
		
		echo '<h3>'.$d1["kat_polling"].'</h3>';
		
		$d = $sql->sql_query("select count(id_jawaban) as jumlah from tbl_jawaban_polling where id_kat_polling='$id_kat_polling'");
		$d1 = $sql->sql_fetchrow($d);
		$jumlah = $d1["jumlah"];
		
		echo '<div class="form-horizontal">';
		$d = $sql->sql_query("select a.polling, count(b.id_jawaban) as jumlah from tbl_polling a left join tbl_jawaban_polling b on a.id_polling=b.id_polling where a.id_kat_polling='$id_kat_polling' group by a.id_polling order by a.urutan asc");
		while($d1 = $sql->sql_fetchrow($d))
		{
			$polling = ($d1["jumlah"]/$jumlah) * 100;
		?>
        	
            	<div class="form-group">
                	<label class="col-sm-3 text-right"><?php echo $d1["polling"]; ?> : </label>
                    <div class="col-sm-8">
                    	<div class="progress">
                          <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<?php echo $polling; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $polling; ?>%">
                            <?php echo $d1["jumlah"]; ?>
                          </div>
                        </div>
                    </div>
                </div>
        <?php
		}
		echo '</div>';
	?>
    </div>
    </div>