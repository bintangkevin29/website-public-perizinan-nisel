<div class="panel panel-default">
<?php

class sitemap extends sql_db
{
	function get_sitemap()
	{
		echo '<div class="panel-heading">Peta Website</div>';
		echo '<div class="row" style="margin-top:30px; margin-left:20px; margin-right:20px;">';
		
		$this->content();
		$this->download();
		$this->link();
		
		echo '</div>';
	}
	
	private function content()
	{
		echo '<br><b>Content:</b>';
		$d = parent::sql_query("select id_module, nama from tbl_module where jenis='2' and publish='Y' and id_connect='0'");
		echo '<ul class="list-content">';
		while($d1 = parent::sql_fetchrow($d))
		{
			echo '<li>'.ucwords($d1["nama"]);
				echo '<ul>';
					$b = parent::sql_query("select judul, year(waktu) as tahun, month(waktu) as bulan from tbl_content where id_module='".$d1["id_module"]."'");
					while($b1 = parent::sql_fetchrow($b))
					{
						echo '<li><a href="'.P_SLASH.'content/'.$b1["tahun"].'/'.$b1["bulan"].'/'.urlencode($b1["judul"]).'.html">'.ucwords($b1["judul"]).'</a></li>';
					}
					
					$c = parent::sql_query("select id_module, nama, year(waktu) as tahun, month(waktu) as bulan from tbl_module where id_connect='".$d1["id_module"]."' and publish='Y'");
					while($c1 = parent::sql_fetchrow($c))
					{
						echo '<li>';
						echo '<a href="'.P_SLASH.'kategori/'.$c1["tahun"].'/'.$c1["bulan"].'/'.urlencode($c1["nama"]).'.html">'.$c1["nama"].'</a>';
							echo '<ul>';
								$b = parent::sql_query("select judul, year(waktu) as tahun, month(waktu) as bulan from tbl_content where id_module='".$c1["id_module"]."'");
								while($b1 = parent::sql_fetchrow($b))
								{
									echo '<li><a href="'.P_SLASH.'content/'.$b1["tahun"].'/'.$b1["bulan"].'/'.urlencode($b1["judul"]).'.html">'.ucwords($b1["judul"]).'</a></li>';
								}
							echo '</ul>';
						echo '</li>';
					}
				echo '</ul>';
			echo '</li>';
		}
		echo '</ul>';
	}
	
	private function download()
	{
		echo '<br><b>File Download:</b>';

		$d = parent::sql_query("select year(waktu) as tahun, month(waktu) as bulan, nama, id_kat_download from tbl_kat_download where publish='Y' order by waktu desc");

		echo '<ul class="list-content">';
		while($d1 = parent::sql_fetchrow($d))
		{			
		?>
			<li>
               <a href="<?php echo P_SLASH.'plugin/download/'.$d1["tahun"].'/'.$d1["bulan"].'/'.urlencode($d1["nama"]); ?>.html">         
                    <?php echo $d1["nama"]; ?>
               </a>
            </li>
        <?php
		}
		echo '</ul>';	
	}
	
	private function link()
	{
		$c = parent::sql_query("select * from tbl_jenis_link order by id_jenis asc");
		while($c1 = parent::sql_fetchrow($c))
		{
			echo '<br><b>'.$c1["jenis"].':</b>';
			$d = parent::sql_query("select year(waktu) as tahun, month(waktu) as bulan, kategori, id_kat_link_info from tbl_kat_link_info where id_jenis='".$c1["id_jenis"]."' and publish='Y' order by waktu desc");
	
			echo '<ul class="list-content">';
			while($d1 = parent::sql_fetchrow($d))
			{			
			?>
				<li>
				   <a href="<?php echo P_SLASH.'plugin/link_info/'.$d1["tahun"].'/'.$d1["bulan"].'/'.urlencode($d1["kategori"]); ?>.html">         
						<?php echo $d1["kategori"]; ?>
				   </a>
				</li>
			<?php
			}
			echo '</ul>';
		}
		
	}
}

$site = new sitemap;
$site->get_sitemap();
?>
</div>
