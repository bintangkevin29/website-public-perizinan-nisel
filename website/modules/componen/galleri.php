<style type="text/css">
.thumb{	
	margin-bottom:20px;	
}
</style>

<?php
class galleri extends sql_db{
	function show_album	($mode)
	{
		echo '<h3 class="grve-element grve-title-line" style="font-weight: bold;">'.strtoupper(urldecode($mode)).'</h3>';
		//echo urldecode($mode);
		$d = parent::sql_query("select id_galleri from tbl_galleri where galleri='".urldecode($mode)."'");
		$d1 = parent::sql_fetchrow($d);
		$id_galleri = $d1["id_galleri"];

		
		$d = parent::sql_query("select year(waktu) as tahun, month(waktu) as bulan, album, nama_file from tbl_album where id_galleri='$id_galleri' and publish='Y' order by id_album desc");
		while($d1 = parent::sql_fetchrow($d))
		{
		?>
        	<div class="col-lg-3 col-md-4 col-xs-6 thumb" style="height:200px;">
        		<center>
	            	<a href="<?php echo P_SLASH.'plugin/galleri/'.$d1["tahun"].'/'.$d1["bulan"].'/'.urlencode($d1["album"]); ?>.html">
		            <div><img src="<?php echo P_SERVERNAME.get_thumbs($d1["nama_file"]); ?>" width="120px" height="120px"> </div>
	                <div><?php echo substr(ucfirst($d1["album"]), 0, 50); ?>...</div>
	                </a>
                </center>
            </div>
        <?php
		}		
	}

	function show_foto($tahun, $bulan, $album)
	{
	?>
    	<script type="text/javascript" src="<?php echo P_SLASH.P_JS;?>fancybox/source/jquery.fancybox.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo P_SLASH.P_JS;?>fancybox/source/jquery.fancybox.css" media="screen" />
        <script type="text/javascript">
			$(document).ready(function(e) {
                $("a[rel=example_group]").fancybox({
					'transitionIn'		: 'none',
					'transitionOut'		: 'none',
					'titlePosition' 	: 'over',
					'titleFormat'       : function(title, currentArray, currentIndex, currentOpts) {
						return '<span id="fancybox-title-over">Image ' +  (currentIndex + 1) + ' / ' + currentArray.length + ' ' + title + '</span>';
					}
				});
            });
		</script>
    <?php
		echo '<h3 class="grve-element grve-title-line" style="font-weight: bold;">'.strtoupper(urldecode($album)).'</h3>';
		$d = parent::sql_query("select id_album from tbl_album where album='".urldecode($album)."' and year(waktu)='$tahun' and month(waktu)='$bulan'");
		$d1 = parent::sql_fetchrow($d);
		$id_album = $d1["id_album"];

		
		$d = parent::sql_query("select keterangan, nama_file from tbl_foto where id_album='$id_album' and publish='Y' order by waktu desc");
		while($d1 = parent::sql_fetchrow($d))
		{
		?>
        	<div class="col-lg-3 col-md-4 col-xs-6 thumb">
            	<a  href="<?php echo P_SERVERNAME.$d1["nama_file"]; ?>" rel="example_group">
	            	<img src="<?php echo P_SERVERNAME.get_thumbs($d1["nama_file"]); ?>" width="120px" height="120px">              
                </a>
            </div>
        <?php
		}		


	}
	function show_galeri()

	{

		echo '<h3 class="grve-element grve-title-line" style="font-weight: bold;">KATEGORI GALERY</h3>';



		echo '<div class="list-content">';

		$d = parent::sql_query("select * from tbl_galleri where publish='Y' order by id_galleri desc");

		echo '<ul class="list-content">';

		while($d1 = parent::sql_fetchrow($d))

		{

		?>

					<li>

							<a href="<?php echo P_SLASH.P_PLUGIN.'galleri/'.urlencode($d1["galleri"]); ?>.html">

					<?php echo strtoupper($d1["galleri"]); ?>

								</a>

						</li>

				<?php

		}

		echo '</ul>';

		echo '</div>';

	}
}

$galleri = new galleri;

if(isset($_GET["tahun"]) and isset($_GET["bulan"]) and isset($_GET["mode"]))
	///echo "a";
	$galleri->show_foto(anti($_GET["tahun"]), anti($_GET["bulan"]), anti($_GET["mode"]));
else if(isset($_GET["mode"]))
	//echo "b";
	$galleri->show_album(anti(urldecode($_GET["mode"])));

else
	//echo "c";LETAKKAN FUNGSI tbl_galeri di sini
	$galleri->show_galeri();
?>
