<?php
class download extends sql_db{
	function show_kategori($mode)
	{
		echo '<h3 class="grve-element grve-title-line" style="font-weight: bold;">'.strtoupper(urldecode($mode)).'</h3>';
		
		$d = parent::sql_query("select id_jenis from tbl_jenis_link where jenis='$mode' and publish='Y'");
		$d1 = parent::sql_fetchrow($d);
		$id_jenis = $d1["id_jenis"];

		

		$d = parent::sql_query("select year(waktu) as tahun, month(waktu) as bulan, kategori, id_kat_link_info from tbl_kat_link_info where id_jenis='$id_jenis' and publish='Y' order by waktu desc");

		echo '<div class="list-content">';
		echo '<ul>';
		while($d1 = parent::sql_fetchrow($d))
		{			
		?>
			<li>
               <a href="<?php echo P_SLASH.'utama.php?mod=plugin&opt=link_info&tahun='.$d1["tahun"].'&bulan='.$d1["bulan"].'&mode='.urlencode($d1["kategori"]); ?>">         
                    <?php echo strtoupper($d1["kategori"]); ?>
               </a>
            </li>
        <?php
		}
		echo '</ul>';
		echo '</div>';
	}

	function show_download($tahun, $bulan, $kat_download)
	{

		echo '<h3 class="grve-element grve-title-line" style="font-weight: bold;">'.strtoupper(urldecode($kat_download)).'</h3>';
		$d = parent::sql_query("select id_kat_link_info from tbl_kat_link_info where kategori='$kat_download' and year(waktu)='$tahun' and month(waktu)='$bulan'");
		$d1 = parent::sql_fetchrow($d);
		$id_kat_link_info = $d1["id_kat_link_info"];
		
		$d = parent::sql_query("select nama, link from tbl_link_info where id_kat_link_info='$id_kat_link_info' and publish='Y' order by waktu desc");
		echo '<div class="list-content">';
		echo '<ul>';
		while($d1 = parent::sql_fetchrow($d))
		{
			if(substr($d1["link"], 0, 7) != 'http://')
			{
				if(substr($d1["link"], 0, 8) == 'https://')
					$d1["link"] = $d1["link"];
				else
					$d1["link"] = 'http://'.$d1["link"];	
			}
		?>
        	<li>
            	<a href="<?php echo $d1["link"]; ?>" target="_blank">	            	
					<?php echo strtoupper($d1["nama"]); ?>
                </a>
            </li>
        <?php
		}
		echo '</ul>';
		echo '</div>';
	}
}

$download = new download;
if(isset($_GET["tahun"]) and isset($_GET["bulan"]) and isset($_GET["mode"]))
	$download->show_download(anti($_GET["tahun"]), anti($_GET["bulan"]), anti(urldecode($_GET["mode"])));
elseif(isset($_GET["mode"]))
	$download->show_kategori(anti(urldecode($_GET["mode"])));
else
	die('kat_download not found');
?>
