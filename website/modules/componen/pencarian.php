<div class="panel panel-default panel-kiri">
<div class="panel-heading">HASIL PENCARIAN</div>
<div class="panel-body">
<div style="margin:10px;">
<?php

$kategori = 'content';
$kata = trim(anti($_POST["cari"]));

if($kategori == 'content')
{
  // mencegah XSS
  $kata = htmlentities(htmlspecialchars($kata), ENT_QUOTES);

  // pisahkan kata per kalimat lalu hitung jumlah kata
  $pisah_kata = explode(" ",$kata);
  $jml_katakan = (integer)count($pisah_kata);
  $jml_kata = $jml_katakan-1;

  $cari = "SELECT count(*) as jumlah FROM tbl_content WHERE publish='Y' and " ;
    for ($i=0; $i<=$jml_kata; $i++){
      $cari .= "isi LIKE '%$pisah_kata[$i]%' or judul LIKE '%$pisah_kata[$i]%'";
      if ($i < $jml_kata ){
        $cari .= " OR ";
      }
    }
  $cari .= " ORDER BY tanggal DESC";
  $hasil  = $sql->sql_query($cari);
  $ketemu = $sql->sql_fetchrow($hasil);
  $jumlah = $ketemu["jumlah"];

  $posisi = 0;
  $batas = 20;
  if(!isset($_GET["page"]))
  	$_GET["page"] = 0;

  if((int)$_GET["page"] != 0)
  	$posisi = ((int)$_GET["page"] -1) * $batas;
  else
  	$posisi = 0;

  $cari = "SELECT id_content, judul, intro, year(waktu) as tahun, month(waktu) as bulan, waktu, lihat FROM tbl_content WHERE publish='Y' and " ;
    for ($i=0; $i<=$jml_kata; $i++){
      $cari .= "isi LIKE '%$pisah_kata[$i]%' or judul LIKE '%$pisah_kata[$i]%'";
      if ($i < $jml_kata ){
        $cari .= " OR ";
      }
    }
  $cari .= " ORDER BY waktu DESC LIMIT $posisi, $batas";
  $hasil  = $sql->sql_query($cari);
  $ketemu = $sql->sql_numrows($hasil);

  if ($ketemu > 0){
    while($t=$sql->sql_fetchrow($hasil)){
		$c = $sql->sql_query("select count(id_comment) as jumlah from tbl_comment where id_content='".$t["id_content"]."'");
		$c1 = $sql->sql_fetchrow($c);
	?>
    	<div class="intro">
        	<div class="sub_judul">
                <a href="<?php echo P_SLASH; ?>content/<?php echo $t["tahun"]; ?>/<?php echo $t["bulan"]; ?>/<?php echo urlencode($t["judul"]); ?>.html">
                    <?php echo $t["judul"]; ?>
                </a>
             </div>
             <div class="statistik_content">
                <span class="glyphicon glyphicon-user"></span> Disdukcapil Medan&nbsp;&nbsp;
                <span class="glyphicon glyphicon-calendar"></span> <?php echo tgl_indo($t["waktu"]); ?>&nbsp;&nbsp;
                <span class="glyphicon glyphicon-comment"></span> <?php echo $c1["jumlah"]; ?> Comment&nbsp;&nbsp;
                <span class="glyphicon glyphicon-eye-open"></span> <?php echo $t["lihat"]; ?>
             </div>

            <div class="content_intro"><?php echo $t["intro"]; ?></div>
            <div class="text-right">
            	<span class="selengkapnya">
            		<a href="<?php echo P_SLASH; ?>content/<?php echo $t["tahun"]; ?>/<?php echo $t["bulan"]; ?>/<?php echo urlencode($t["judul"]); ?>.html">Selengkapnya...</a>
                </span>
            </div>
            <hr />
		</div>
    <?php
    }

  }
  else{
    echo "<p>Tidak ditemukan berita dengan kata <b>$kata</b></p>";
  }
}
else {
  echo '<link rel="stylesheet" type="text/css" href="'.P_SLASH.P_CSS.'produk.css">';
  $i = 1;
  $d = $sql->sql_query("select a.id_produk, b.file, a.produk, year(a.date_added) as tahun, month(a.date_added) as bulan, harga from eco_produk a inner join eco_produk_gambar b on a.id_produk=b.id_produk where a.produk like '%$kata%' group by a.id_produk order by a.date_added desc, b.id_gambar asc  limit 0, 16");
  while($d1 = $sql->sql_fetchrow($d))
  {
    if($i % 4 == 1)
      echo '<div class="row">';

    echo '<div class="col-md-3 list-produk">';
      echo '<a href="'.P_SLASH.'produk/'.$d1["tahun"].'/'.$d1["bulan"].'/'.urlencode(strtolower($d1["produk"])).'.html">';
      echo '<div class="list-produk-kotak">';
        echo '<div align="center"><img src="'.P_SERVERNAME.$d1["file"].'" class="img-responsive" /></div>';
        echo '<div class="list-produk-title">'.$d1["produk"].'</div>';


        $c = $sql->sql_query("select diskon from eco_produk_diskon where tanggal<=date(now()) and tanggal1>=date(now()) and id_produk='".$d1["id_produk"]."'");
        if($sql->sql_numrows($c))
        {
          $c1 = $sql->sql_fetchrow($c);

          echo '<div class="list-produk-diskon">Rp '.number_format($d1["harga"],0,',','.').'</div>';
          echo '<div class="list-produk-price">Rp '.number_format($d1["harga"] - (($d1["harga"]/100)*$c1["diskon"]),0,',','.').'</div>';

          echo '<div class="a-label-diskon">';
            echo '<div class="label-diskon">'.$c1["diskon"].'%</div>';
          echo '</div>';
        }else
        {
          echo '<div class="list-produk-diskon"></div>';
          echo '<div class="list-produk-price">Rp '.number_format($d1["harga"],0,',','.').'</div>';
        }

      echo '</div>';
      echo '</a>';
    echo '</div>';

    if($i % 4 == 0)
      echo '</div>';
    $i++;
  }

  if($i-1 % 4 != 0)
    echo '</div>';
}
?>
</div>
</div>
</div>
