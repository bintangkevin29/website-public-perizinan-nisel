<style type="text/css">
	.nav-tabs {
	  border-bottom: none;	  
	}
	.nav-tabs > li {
	  float: left;
	  margin-bottom: -1px;
	  padding: 0px;
	  height:35px;
	  vertical-align:middle;
	  border-right:solid 1px #CCC;
	}
	.nav-tabs > li > a {
	  margin-right: 0px;
	  line-height: 1.42857143;
	  border: none;
	  border-radius: 0;
	  background-color: #FFC744;
	  color: #FFF;
	  text-align: center;	  
	  font-size:11px;
	  height:100%; 
	}
	.nav-tabs > li > a:hover {
	  border:none;
	  background-color: #F3BD11;
	}
	.nav-tabs > li.active > a,
	.nav-tabs > li.active > a:hover,
	.nav-tabs > li.active > a:focus {
	  color: #FFF;
	  cursor: default;
	  background-color: #F3BD11;
	  border: none;
	  border-bottom-color: transparent;
	}

	.tab-content{
		border: solid 1px #CCC;
		width: 100%;
		min-height: 272px;			
		max-height: 272px;
		padding: 10px 5px 10px 5px;
		overflow: hidden;
		margin-top: 4px;
		position: relative;
	}

	.tab-content a.readmore {
		position: absolute;
		top: 250px;
		font-size: 10px;
		bottom: 2px;
		left: 0;

		padding-left: 10px;
		padding-bottom: 10px;

		color: #000;
		text-decoration: none;		
	}

	.tab-content a.readmore:hover {
		color: #CC6600;
	}

	.tab-pane ul{
		max-height: 230px;
	}

		
</style>

<div>
	<div style="margin-bottom:5px;">
		<form action="<?php echo P_SLASH.P_PLUGIN; ?>pencarian.html" method="post">
			<input type="text" name="cari" class="form-control" placeholder="Pencarian">
		</form>
	</div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 7px">
    <li role="presentation" class="active" style="width: 50%;"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">BERITA & INFORMASI</a></li>
    <li role="presentation" style="width: 50%;"><a href="#profile2" aria-controls="profile2" role="tab" data-toggle="tab">PENGUMUMAN</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">


  	<!-- Berita dan Informasi ====================== -->  
    <div role="tabpanel" class="tab-pane active list-content" id="home">
    	<ul>
    	<?php
    		$d = $sql->sql_query("select judul, year(waktu) as tahun, month(waktu) as bulan from tbl_content where id_module='148' order by waktu desc limit 0, 10");
    		while($d1 = $sql->sql_fetchrow($d))
    		{
    			echo '<li><a href="'.P_SLASH.'content/'.$d1["tahun"].'/'.$d1["bulan"].'/'.urlencode($d1["judul"]).'.html">'.strtoupper($d1["judul"]).'</a></li>';
    		}
    	?>
    	</ul>
    	<!-- Link arsip berita ====================== -->  
    	<a href="<?php echo P_SLASH; ?>kategori/2018/3/berita+dan+informasi.html" class="readmore">
			<span class="glyphicon glyphicon-folder-open"></span>
			&nbsp;&nbsp; Arsip Berita dan Informasi</a>
    </div>
    <!-- end of Berita dan Informasi ==================== -->


     <!-- Pengumuman ====================== -->  
    <div role="tabpanel" class="tab-pane list-content" id="profile2">
    	<ul>
    	<?php
    		$d = $sql->sql_query("select judul, year(waktu) as tahun, month(waktu) as bulan from tbl_content where id_module='149' order by waktu desc limit 0, 10");
    		while($d1 = $sql->sql_fetchrow($d))
    		{
    			echo '<li><a href="'.P_SLASH.'content/'.$d1["tahun"].'/'.$d1["bulan"].'/'.urlencode($d1["judul"]).'.html">'.strtoupper($d1["judul"]).'</a></li>';
    		}
    	?>
    	</ul>
    	<!-- Link arsip berita ====================== -->  
    	<a href="<?php echo P_SLASH; ?>kategori/2016/12/pengumuman.html" class="readmore"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp; Arsip Pengumuman</a>
    </div>
    <!-- Pengumuman ====================== -->    
  </div>
</div>