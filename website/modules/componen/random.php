<style type="text/css">
	.carousel-control:hover, .carousel-control.left, .carousel-control.right{
		background: none;
	}
</style>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="padding:0px; width:100%;">
        <ol class="carousel-indicators">
        	<?php
				$i=0;
				$d = $sql->sql_query("select * from tbl_random where publish='Y' and letak='atas' order by id_random desc limit 0, 10");

				while($d1 = $sql->sql_fetchrow($d))
				{
					if($i==0)
						echo '<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>';
					else
						echo '<li data-target="#carousel-example-generic" data-slide-to="'.$i.'"></li>';
					$i++;
				}
			?>          
        </ol>
        <div class="carousel-inner" role="listbox">
        	<?php
				$i=0;
				$d = $sql->sql_query("select * from tbl_random where publish='Y' and letak='atas' order by id_random desc limit 0, 10");
				while($d1 = $sql->sql_fetchrow($d))
				{
					if($i==0)
					{
						echo '<div class="item active">';
			            echo '<img src="'.P_SERVERNAME.$d1["nama_file"].'" class="img-responsive">';
						echo '<div class="carousel-caption">';
						echo '<h3>'.$d1["nama"].'</h3>';
						echo '<p>'.$d1["keterangan"].'</p>';
						echo '</div>';
          				echo '</div>';
					}
					else
					{
						echo '<div class="item">';
			            echo '<img src="'.P_SERVERNAME.$d1["nama_file"].'" class="img-responsive">';
						echo '<div class="carousel-caption">';
						echo '<h3>'.$d1["nama"].'</h3>';
						echo '<p>'.$d1["keterangan"].'</p>';
						echo '</div>';
          				echo '</div>';
					}
					$i++;
				}
			?>          
        </div>
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>