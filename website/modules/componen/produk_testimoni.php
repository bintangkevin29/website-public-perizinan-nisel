<?php
	if(!$sess->cek_session(P_SESS))
	{
	?>
		<div class="text-center" style="margin-top: 20px; margin-bottom: 20px;">
			<button type="button" class="btn btn-primary" onclick="document.location.href='<?php echo P_SLASH.P_PLUGIN; ?>login.html'">Untuk Mengisi Testimoni Silahkan Login Terlebih Dahulu</button>
		</div>
	<?php
	}
	else
	{
		if(isset($_POST["testimoni"]))
		{
			$kode = anti($_POST["captcha"]);
			$testimoni = anti($_POST["testimoni"]);

			if(empty($testimoni) or $kode!= $_SESSION["captcha_session"])
				$error = 'Isi Testimoni dan Captcha  dengan benar';
			else
			{
				$id_user = $sess->get_iduser(P_SESS);
				$d = $sql->sql_query("select id_produk from eco_testimoni where id_produk='$id_produk' and id_user='$id_user'");
				if($sql->sql_numrows($d)>0)
					$error = 'Anda sudah pernah mengisi testimoni ini';
				else
				{
					$d = $sql->sql_query("insert into eco_testimoni (id_produk, id_user, testimoni) value ('$id_produk', '$id_user', '$testimoni')");

					$pesan = $sql->sql_error($d);
					if(empty($pesan["message"]))
						header("Location:".URI);
					else
						$error = 'Gagal mengisi testimoni';

				}
			}
		}
	?>
		<form class="form-horizontal" action="<?php echo URI; ?>" method="post" style="margin:20px;">
			<div class="error"><?php echo isset($error)?$error:''; ?></div>
			<div class="form-group">
				<label class="control-label col-sm-3"><span class="required">*</span>Testimoni : </label>
				<div class="col-sm-7">
					<textarea name="testimoni" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3"><span class="required">*</span>Captcha : </label>
				<div class="col-sm-7">
					<img src="<?php echo P_SLASH.P_JS; ?>captcha/captcha.php">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3"></label>
				<div class="col-sm-7">
					<input type="text" name="captcha" maxlength="8" required="required" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3"></label>
				<div class="col-sm-7">
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</div>
		</form>
	<?php
	}
?>
	