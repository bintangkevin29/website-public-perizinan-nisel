<div class="panel panel-default">
	<div class="panel-heading">Kurs Rupiah</div>
    <div class="panel-body" style="padding:2px;">
    <?php
		$data = json_decode(file_get_contents('http://www.adisurya.net/kurs-bca/get'));
		$tgl = explode(" ", $data->LastUpdate);		
	?>
    	<table class="table table-striped table-responsive" style="margin-bottom:5px;">
        	<thead>
            	<tr>
                	<th colspan="3" class="text-center"><?php echo tgl_indo($tgl[0]).' - '.$tgl[1].' WIB'; ?></th>
                </tr>
                <tr>
                	<th class="text-center">Mata Uang</th>
                    <th class="text-center">Jual</th>
                    <th class="text-center">Beli</th>
                </tr>
            </thead>
            <tbody>
            	<tr>
                	<td class="text-center">USD</td>
                	<td class="text-right"><?php echo number_format($data->Data->USD->Jual, 2, ',','.'); ?></td>
                    <td class="text-right"><?php echo number_format($data->Data->USD->Beli, 2, ',','.'); ?></td>
                </tr>
                <tr>
                	<td class="text-center">EUR</td>
                	<td class="text-right"><?php echo number_format($data->Data->EUR->Jual, 2, ',','.'); ?></td>
                    <td class="text-right"><?php echo number_format($data->Data->EUR->Beli, 2, ',','.'); ?></td>
                </tr>
                <tr>
                	<td class="text-center">SGD</td>
                	<td class="text-right"><?php echo number_format($data->Data->SGD->Jual, 2, ',','.'); ?></td>
                    <td class="text-right"><?php echo number_format($data->Data->SGD->Beli, 2, ',','.'); ?></td>
                </tr>
                <tr>
                	<td class="text-center">JPY</td>
                	<td class="text-right"><?php echo number_format($data->Data->JPY->Jual, 2, ',','.'); ?></td>
                    <td class="text-right"><?php echo number_format($data->Data->JPY->Beli, 2, ',','.'); ?></td>
                </tr>
                <tr>
                	<td class="text-center">AUD</td>
                	<td class="text-right"><?php echo number_format($data->Data->AUD->Jual, 2, ',','.'); ?></td>
                    <td class="text-right"><?php echo number_format($data->Data->AUD->Beli, 2, ',','.'); ?></td>
                </tr>
            </tbody>
        </table>
        <p class="text-right">        
        	<a href="<?php echo P_SLASH.P_PLUGIN.'kurs.html'; ?>">Selengkapnya...</a>
        </p>
    </div>
</div>