<div class="row">
<?php
	$d = $sql->sql_query("select intro, judul, year(waktu) as tahun, month(waktu) as bulan from tbl_content where id_module='148' and publish='Y' order by waktu desc limit 0, 2");
	while($d1 = $sql->sql_fetchrow($d))
	{
		echo '<div class="col-md-6">';
			echo '<h3 class="grve-element grve-title-line" style="font-weight: bold;"><a href="'.P_SLASH.'content/'.$d1["tahun"].'/'.$d1["bulan"].'/'.urlencode($d1["judul"]).'.html">'.strtoupper($d1["judul"]).'</a></h3>';
			echo '<div class="text-justify">'.$d1["intro"].'</div>';
			echo '<div class="text-right"><a href="'.P_SLASH.'content/'.$d1["tahun"].'/'.$d1["bulan"].'/'.urlencode($d1["judul"]).'.html">Selengkapnya...</a></div>';
		echo '</div>';
	}
?>
</div>

<div class="row" style="margin-top:10px;">
<?php
	$d = $sql->sql_query("select intro, judul, year(waktu) as tahun, month(waktu) as bulan from tbl_content where id_module='148' and publish='Y' order by waktu desc limit 2, 2");
	while($d1 = $sql->sql_fetchrow($d))
	{
		echo '<div class="col-md-6">';
			echo '<h3 class="grve-element grve-title-line" style="font-weight: bold;"><a href="'.P_SLASH.'content/'.$d1["tahun"].'/'.$d1["bulan"].'/'.urlencode($d1["judul"]).'.html">'.strtoupper($d1["judul"]).'</a></h3>';
			echo '<div class="text-justify">'.$d1["intro"].'</div>';
			echo '<div class="text-right"><a href="'.P_SLASH.'content/'.$d1["tahun"].'/'.$d1["bulan"].'/'.urlencode($d1["judul"]).'.html">Selengkapnya...</a></div>';
		echo '</div>';
	}
?>
</div>

<?php include(P_MODULES.'menu_componen/content_atas.php');