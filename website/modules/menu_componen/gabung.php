<style type="text/css">
.gabung-utama{
	background:#089607;
	padding-top:40px;
	padding-bottom:40px;
	margin-top:50px;
}

.gabung-isi{
	background:#FFF;
	padding:20px;
	color:#089607;
	font-size:18px;
	font-family:Arial, Helvetica, sans-serif;
}

a.gabung-button{
	text-decoration:none;
	color:#FFF;
	background:#089607;
	font-size:14px;
	padding:10px 30px 10px 30px;
	
	border-radius:5px 5px 5px 5px;
}
</style>

<div class="container-fluid gabung-utama">
	<div class="container gabung-isi text-center">
    	BERGABUNG DENGAN WAHYU WAHAB CENTER, KALAU BUKAN SEKARANG  KAPAN LAGI ?
        <a href="" class="gabung-button"><span class="glyphicon glyphicon-thumbs-up"></span> Gabung</a>
    </div>
</div>