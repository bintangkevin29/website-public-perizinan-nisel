<div class="row" style="margin-top:40px;">
	<!-- pengumuman -->
	<div class="col-md-6 list-content">
		<h5 class="grve-element grve-title-line" style="font-weight: bold;">PENGUMUMAN</h5>
		<ul>
			<?php
				$d = $sql->sql_query("select judul, year(waktu) as tahun, month(waktu) as bulan from tbl_content where id_module='149' and publish='Y' order by waktu desc limit 0, 5");
				while($d1 = $sql->sql_fetchrow($d))
				{
					echo '<li><a href="'.P_SLASH.'content/'.$d1["tahun"].'/'.$d1["bulan"].'/'.urlencode($d1["judul"]).'.html">'.strtoupper($d1["judul"]).'</a></li>';
				}

			?>
		</ul>
		<div class="text-right">
			<a href="<?php echo P_SLASH.'kategori/2016/12/pengumuman.html'; ?>">Selengkapnya...</a>
		</div>
	</div>
	<!-- end of pengumuman -->


	<!-- FILE DOWNLOAD -->
	<div class="col-md-6  list-content">
		<h5 class="grve-element grve-title-line" style="font-weight: bold;">FILE DOWNLOAD</h5>
		<ul>
			<?php
				$d = $sql->sql_query("select nama from tbl_kat_download where publish='Y' order by waktu desc limit 0, 5");
				while($d1 = $sql->sql_fetchrow($d))
				{
					echo '<li><a href="'.P_SLASH.P_PLUGIN.'download/'.urlencode($d1["nama"]).'.html">'.strtoupper($d1["nama"]).'</a></li>';
				}

			?>
		</ul>
		<div class="text-right">
			<a href="<?php echo P_SLASH.P_PLUGIN.'download.html'; ?>">Selengkapnya...</a>
		</div>
	</div>
	<!-- end of download -->
</div>