
<style type="text/css">
    .link-portal{
        width: 100%;
        height: 50px;
        padding-top: 15px;
        background-color: #FFF;font-size: 12px;font-weight: bold;
        text-align: center;
        color:#000;
    }

    .link-portal a{
        line-height: 2em;
        color: #000;
        text-decoration: none;font-size: 12px;font-weight: bold;
        margin: 0 5px 0px 5px;
    }
</style>
<body id="grve-wrapper">
    <div id="header-title">
        <div style="background-color: #F3BD11; position: fixed; width: 100%; height: 60px; border-bottom: solid 5px #fff;">
            <div class="title1">
                DINAS PENANAMAN MODAL DAN PELAYANAN PERIZINAN TERPADU SATU PINTU<br>
                PEMERINTAHAN KABUPATEN NIAS SELATAN
            </div>
        </div>
    </div>

	<div id="grve-feature-section">
	    <div class="kepaladaerah">
	        <div style="overflow: hidden;">
                <div class="text-center">
                    <img src="<?php echo P_SLASH.P_IMAGES; ?>sumut.png" class="logo-sumut">
                    <img src="<?php echo P_SLASH.P_IMAGES; ?>logo.png" class="logo-sumut">
                </div>
                <div class="text-center"><img src="<?php echo P_SLASH.P_IMAGES; ?>walikota.jpg" class="gambar-gubernur"></div>
                <div class="text-center"><img src="<?php echo P_SLASH.P_IMAGES; ?>kadis.jpg" class="gambar-kadis"></div>           
	        </div>
    </div>
    <div id="slide">
        <?php include(P_MODULES.'componen/random.php'); ?>
        <div class="link-portal">

<?php		
$d = $sql->sql_query("select * from tbl_link where letak='Header Website' order by id_link asc");
$b2 = $sql->sql_numrows($d);
$i=1;
while($b1 = $sql->sql_fetchrow($d))
{
        if(substr($b1["link"], 0, 7) != 'http://')
        {
                if(substr($b1["link"], 0, 8) == 'https://')
                        $b1["link"] = $b1["link"];
                else
                        $b1["link"] = 'http://'.$b1["link"];
        }

        if($i == $b2)
                echo '<a href="'.$b1["link"].'" target="_blank">'.strtoupper($b1["nama"]).'</a>';
        else
                echo '<a href="'.$b1["link"].'" target="_blank">'.strtoupper($b1["nama"]).'</a> &#124; ';

        $i++;
}
?>                
        </div>
    </div>
</div>
</body>