<link rel="stylesheet" href="<?php echo P_SLASH.P_LIBRARIES; ?>mCustomToolbar/jquery.mCustomScrollbar.css">
<script src="<?php echo P_SLASH.P_LIBRARIES; ?>mCustomToolbar/jquery.mCustomScrollbar.concat.min.js"></script>
	<script>
		(function($){
			$(window).load(function(){				
				$("#berita .panel-body").mCustomScrollbar({
					setHeight:500,
					theme:"minimal-dark"
				});
				
				$("#testimoni .panel-body").mCustomScrollbar({
					setHeight:500,
					theme:"minimal-dark"
				});												
			});
		})(jQuery);
	</script>
<div class="container-fluid" style="margin-top:20px; width:100%;">
<div class="container equal">
	<div class="col-md-8">
    	<?php include(P_MODULES.'componen/main.php'); ?>
    </div>
    <div class="col-md-4">
    	<?php include(P_MODULES.'componen/testimoni.php'); ?>
    </div>
</div> 
<div id="content">    	
		    <?php 
           		include(P_MODULES.'menu_componen/banner_tengah.php');
			?>
         </div><br />
   
</div>