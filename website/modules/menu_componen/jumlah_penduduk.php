
<div class="panel panel-default">
<div class="panel-heading">Jumlah Penduduk Kota Medan</div>
<div class="panel-body">
<?php
	$b = $sql->sql_query("select * from tbl_jumlah_penduduk order by tahun desc, semester desc limit 0,1");
	$b1 = $sql->sql_fetchrow($b);
	
	echo '<div class="text-center">';
	echo 'Jumlah Penduduk Kota Medan Pada Tahun '.$b1["tahun"].' Semester '.($b1["semester"] == '1' ? 'Satu':'Dua');
	echo '</div>';
?>
	<script type="text/javascript" src="<?php echo P_SLASH.P_JS; ?>jquery.animateNumber.js"></script>
	<script type="text/javascript">
    var comma_separator_number_step = $.animateNumber.numberStepFactories.separator('.')
    $(window).load(function(){
        $('.Pria')
            .animateNumber(
                { 
                    number: <?php echo $b1["pria"]; ?>,
                    numberStep: comma_separator_number_step	
                },
                10000
            );
			
		$('.Wanita')
            .animateNumber(
                { 
                    number: <?php echo $b1["wanita"]; ?>,
                    numberStep: comma_separator_number_step	
                },
                10000
            );
    });
    </script>
    <style type="text/css">
		.Single{
			font-family:Arial, Helvetica, sans-serif;
			font-size:36px;
			color:#001E6D;
			font-weight:bold;
		}
	</style>
	<div class="text-center">
        <h4>Laki-Laki</h4>
        <div class="Single Pria"></div>
        
        <h4>Perempuan</h4>
        <div class="Single Wanita"></div>
    </div>
	

</div>
</div>