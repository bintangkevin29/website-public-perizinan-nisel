<div class="panel panel-primary kategori-list">
	<div class="panel-heading">Kategori</div>
	<div class="panel-body">
    	<ul>
        	<?php
				$c = $sql->sql_query("select id_module, nama, year(waktu) as tahun, month(waktu) as bulan, jenis from tbl_module where (jenis='2' or jenis='1') and id_connect='0' and ismenu='N' order by urutan asc");
				while($c1 = $sql->sql_fetchrow($c))
				{
					echo '<li>';
					if($c1["jenis"] == '1')
						 echo '<a href="'.P_SLASH.'content/'.$c1["tahun"].'/'.$c1["bulan"].'/'.urlencode($c1["nama"]).'.html">'.$c1["nama"].'</a>';
					elseif($c1["jenis"] == '2')
						echo '<a href="'.P_SLASH.'kategori/'.$c1["tahun"].'/'.$c1["bulan"].'/'.urlencode($c1["nama"]).'.html">'.$c1["nama"].'</a>';
					echo '</li>';
				}
			?>
        </ul>
	</div>
</div>