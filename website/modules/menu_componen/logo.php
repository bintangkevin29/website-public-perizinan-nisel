<style type="text/css">

a.border-circle{
	moz-box-sizing: content-box;
 	-webkit-box-sizing: content-box;
	box-sizing: content-box;
	border-radius: 1000px;
	border-width: 3px;
	border-style: solid;
	display: block;
	margin: 0 auto;
	padding: 10px;
	color: #F20000;
	border-color: #F20000;
	position: relative;
	font-size: 100px;
	line-height: 150px;
	width: 150px;
}

a.border-circle:hover{
	text-decoration:none;
}

.title-border-cirle{
	color: #F20000;
	font-family:Arial, Helvetica, sans-serif;
	font-size:14px;
	font-weight:600;
	padding-top:10px;
}
</style>


<div class="container-fluid">
	<div class="col-md-2 text-center">
    </div>
	<div class="col-md-4 text-center">
    	<a href="content/2015/9/CURRICULUM+VITAE.html" class="glyphicon glyphicon-user border-circle"></a>
        <div class="title-border-cirle">RIWAYAT HIDUP WAYU WAHAB</div>
    </div>
    <div class="col-md-4 text-center">
    	<a href="content/2015/9/VISI+DAN+MISI.html" class="glyphicon glyphicon-book border-circle"></a>
        <div class="title-border-cirle">VISI DAN MISI WAHYU WAHAB</div>
    </div>
    <div class="col-md-2 text-center">
    </div>
</div>