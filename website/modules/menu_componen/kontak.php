<div id="judul_content">Kontak <span>Kami</span></div>
<p style="margin-top:10px;">
	<a href="https://www.facebook.com/DispendaMedan" target="_blank"><img src="<?php echo P_SLASH.P_IMAGES; ?>statistik/facebook.png"></a>&nbsp;&nbsp;&nbsp;
    <a href="https://plus.google.com/104824735997427886102" target="_blank"><img src="<?php echo P_SLASH.P_IMAGES; ?>statistik/googleplus.png"></a>&nbsp;&nbsp;&nbsp;
    <a href="https://twitter.com/dispendamedan" target="_blank"><img src="<?php echo P_SLASH.P_IMAGES; ?>statistik/twitter.png"></a>
</p>
<p>
	<div class="row" id="content_contact">
    	<table>
        	<tr>
            	<td valign="top" align="center" style="padding:10px;">
                	<img src="<?php echo P_SLASH.P_IMAGES; ?>statistik/telp.png">
                </td>
                <td>
                	<b>Telepon Kami:</b><br>
                    <div id="phone">(061)7851694 - 95</div>
                    <b>Dering Pajak:</b><br>
                    <div id="phone1">500 221</div>
                </td>
            </tr>
        </table>    	
    </div>
</p>
<p>
	<div class="row" id="content_contact">
    	<table>
        	<tr>
            	<td valign="top" align="center" style="padding:10px;">
                	<img src="<?php echo P_SLASH.P_IMAGES; ?>statistik/mail.png">
                </td>
                <td>
                	<b>E-Mail Kami</b>:<br>pengaduan@ dispenda.pemkomedan.go.id
                </td>
            </tr>
        </table>    	
    </div>
</p>
<p>
	<div class="row" id="content_contact">
    	<table>
        	<tr>
            	<td valign="top" align="center" style="padding:10px;">
                	<img src="<?php echo P_SLASH.P_IMAGES; ?>statistik/world.png">
                </td>
                <td>
                	<b>Website</b>:<br>dispenda.pemkomedan.go.id
                </td>
            </tr>
        </table>    	
    </div>
</p>
<p>
	<div class="row" id="content_contact">
    	<table>
        	<tr>
            	<td valign="top" align="center" style="padding:10px;">
                	<img src="<?php echo P_SLASH.P_IMAGES; ?>statistik/rumah.png">
                </td>
                <td>
                	Jalan Jendral Besar H. Abdul Haris Nasution No. 32 Medan - 20143
                </td>
            </tr>
        </table>
    </div>
</p>