$(document).ready(function(){
		$("#statistik_browser").html('Browser : ' + $.browser.name + ' ver: ' + $.browser.version);
		
		var dt = new Date();
		var month = new Array();
		month[0] = "January";
		month[1] = "February";
		month[2] = "March";
		month[3] = "April";
		month[4] = "May";
		month[5] = "June";
		month[6] = "July";
		month[7] = "August";
		month[8] = "September";
		month[9] = "October";
		month[10] = "November";
		month[11] = "December";
		$("#statistik_waktu").html('Waktu : ' + dt.getDate() + ' ' + month[dt.getMonth()] + ' ' + dt.getFullYear() + ' - ' + dt.getHours() + ':' + dt.getMinutes() + ':' + dt.getSeconds());
	});