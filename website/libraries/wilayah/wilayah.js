// JavaScript Document
var text;

function wilayah(slash, id_asal, id_hasil, jenis, opsi1, pesan_opsi1, opsi2, pesan_opsi2)
{
	var error;
	if(jenis == 'kabupaten.html')
		error = '<option value="0">PILIH KABUPATEN</option>';
	else if(jenis == 'kecamatan.html')
		error = '<option value="0">PILIH KECAMATAN</option>';
	else if(jenis == 'kelurahan.html')
		error = '<option value="0">PILIH KELURAHAN</option>';

	$(id_hasil).html('<option value="0">LOADING</option>');

	if(opsi1)
		$(opsi1).html('<option value="0">'+pesan_opsi1+'</option>');
	if(opsi2)
		$(opsi2).html('<option value="0">'+pesan_opsi2+'</option>');

	if($(id_asal).val() == '0')
	{
		$(id_hasil).html(error);
	}
	else
	{
		if(id_asal.substr(1, 9) == 'kabupaten')
		{
			text = '&provinsi='+$("#provinsi"+id_asal.substr(11)).val();
		}
		else if(id_asal.substr(1, 9) == 'kecamatan')
		{
			text = '&provinsi='+$("#provinsi"+id_asal.substr(11)).val()+'&kabupaten='+$("#kabupaten"+id_asal.substr(11)).val();
		}
		var id = $(id_asal).val();

		$.ajax({
			url:slash+jenis,
			data:'id='+$(id_asal).val()+text,
			success: function(data){
				$(id_hasil).html(data);
			},
			error: function(){
				$(id_hasil).html(error);
			}
		});
	}
}
