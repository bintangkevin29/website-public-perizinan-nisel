var headerHeight = 50;

$(function(){

	"use strict";		

	var Main = {
		init: function(){			
			//header setting
			this.headerSettings();

			//Feature Section
			this.featureSection();			

			//Menu Section
			this.mainMenu();

		},


		headerSettings: function() {			
			var header = $('#grve-header');				
			var slideimg = $('#slide').find('img');
			
			header.css('height',headerHeight);
			/*logo.css('height',headerHeight);
			logo.find('img').css('max-height',headerHeight);*/
			
			if(!$('#grve-feature-section').length && !isMobile.any()){
				header.css('position','fixed');
			}

			header.animate({'opacity':1},500);
			$('#grve-wrapper').css('visibility','visible');				
		},


		featureSection: function(){
			var windowHeight = $(window).height();
			var windowWidth = $(window).width();			

			$('#grve-feature-section').css({'height':windowHeight - (headerHeight+60)});
			$('#grve-main-menu').css({'height':windowHeight});
			$('.main-menu').css({'height':windowHeight - headerHeight});
			$('.kepaladaerah').css({'height':windowHeight - (headerHeight+60)});

			if($("#type-page").val() == 'portal')
			{
				//pengaturan tinggi 1 halaman
				$('#grve-feature-section').css({'height':windowHeight - (headerHeight+135)});

				//pengaturan tinggi gambar kepala daerah
				$('.kepaladaerah').css({'height':windowHeight - (headerHeight+135)});


				$("#slide img").css({'height':windowHeight - (headerHeight+185), 'width':windowWidth-230});
				
				$(".social-media").find('img').each(function(){
					$(this).attr('src',$(this).attr('src').replace('./images/','./website/images/'));
				});
				$(".grve-menu-btn").css('display','none');
				$(".btn-home").css('display','none');

				$(".kepaladaerah").css('position','absolute');
				$("#slide").css('position','absolute');
			}
			else
			{
				$("#slide img").css({'height':windowHeight - (headerHeight+110), 'width':windowWidth-250});
			}


		},

		mainMenu: function() {			

			$('.grve-menu-btn').click(function(){
				$('#grve-main-menu').css({'display':'block'});
			});

			$('.grve-mainmenu-btn').click(function(){
				
				$(this).parent().toggleClass('active');
				$(this).parent().find(' > .grve-submenu ').slideToggle(200);
				
			});

			$('.grve-close-btn').click(function (event){							
				$('.grve-submenu').css('display','none');
				$('#grve-main-menu ul li').removeClass('active');
				$('#grve-main-menu').css({'display':'none'});
			});
		}
	};

	var isMobile = {
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
		BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i);
		},
		iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
		Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
		Windows: function() {
			return navigator.userAgent.match(/IEMobile/i);
		},
		any: function() {
			return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		}
	};

	$(window).scroll(function() {
		var eTop = $('.body-content').offset().top;					
		if(($(window).scrollTop() - (eTop-60)) > 0)
			$("#grve-header").css({'position':'fixed','top':'0px'});
		else
			$("#grve-header").css({'position':'relative'});
	});


	Main.init();

	//Window Resize
	$(window).afterResize(function() {
		alert('hai');
		
	});
	
})(jQuery);
