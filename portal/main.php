<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>

<meta charset="utf-8">
<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="./website/images/logo.jpg" type="image/x-icon">


<title><?php echo P_TITLE; ?></title><link rel="shortcut icon" href="images/logo.png" type="image/x-icon">
<script src="./website/<?php echo P_JS; ?>jquery.js"></script>
<script src="./website/<?php echo P_JS; ?>main.js"></script>
<script type="text/javascript" src="./website/<?php echo P_JS; ?>bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="./website/<?php echo P_JS; ?>bootstrap/js/jquery.bootstrap.newsbox.min.js"></script>
<link href="./website/<?php echo P_JS; ?>bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="./website/<?php echo P_CSS; ?>template.css" rel="stylesheet">
<link href="./website/<?php echo P_CSS; ?>elements.css" rel="stylesheet">
<link href="./website/<?php echo P_LIBRARIES; ?>font-awesome/css/font-awesome.min.css" rel="stylesheet">

<style type="text/css">
    .link-portal{
        width: 100%;
        height: 50px;
        padding-top: 15px;
        background-color: #FFF;font-size: 12px;font-weight: bold;
        text-align: center;
        color:#000;
    }

    .link-portal a{
        line-height: 2em;
        color: #000;
        text-decoration: none;font-size: 12px;font-weight: bold;
        margin: 0 5px 0px 5px;
    }
</style>
</head>

<body id="grve-wrapper">
	<input type="hidden" id="type-page" value="portal" />
	<div id="header-title">
	    <div style="background-color: #F3BD11; position: fixed; width: 100%; height: 60px; border-bottom: solid 5px #fff;">
	        <div class="title1">
                DINAS PENANAMAN MODAL DAN PELAYANAN PERIZINAN TERPADU SATU PINTU <br />
                PEMERINTAHAN KABUPATEN NIAS SELATAN
            </div>
	    </div>
	</div>
	<div id="grve-feature-section">
	    <div class="kepaladaerah">
	        <div style="overflow: hidden;">
                <div class="text-center">
                    <img src="<?php echo './website/'.P_IMAGES; ?>sumut.png" class="logo-sumut">
                    <img src="<?php echo './website/'.P_IMAGES; ?>logo.png" class="logo-sumut">
                </div>
                <div class="text-center"><img src="<?php echo './website/'.P_IMAGES; ?>walikota.jpg" class="gambar-gubernur"></div>
                <div class="text-center"><img src="<?php echo './website/'.P_IMAGES; ?>kadis.jpg" class="gambar-kadis"></div>           
            </div>
	    </div>
	    <div id="slide">
	        <?php include("./website/".P_MODULES.'componen/random.php'); ?>
	        <div class="link-portal">	        	

<?php
$d = $sql->sql_query("select * from tbl_link where letak='Header Portal' order by id_link asc");
$b2 = $sql->sql_numrows($d);
$i=1;
while($b1 = $sql->sql_fetchrow($d))
{
        if(substr($b1["link"], 0, 7) != 'http://')
        {
                if(substr($b1["link"], 0, 8) == 'https://')
                        $b1["link"] = $b1["link"];
                else
                        $b1["link"] = 'http://'.$b1["link"];
        }

        if($i == $b2)
                echo '<a href="'.$b1["link"].'" target="_blank">'.strtoupper($b1["nama"]).'</a>';
        else
                echo '<a href="'.$b1["link"].'" target="_blank">'.strtoupper($b1["nama"]).'</a> &#124; ';

        $i++;
}
?>

	        </div>
            
               

	    </div>
	</div>


	 <!-- HEADER ============================================= -->
    <header id="grve-header" data-height="50">
        <div class="selamat-datang"><b>Selamat Datang</b> di Website Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Pemerintah Kabupaten Nias Selatan</div>
        <?php include("./website/".P_MODULES.'menu_componen/socialmedia.php'); ?>    
    </header>
    <!-- end of HEADER ============================================= -->


    <?php
    	include("website/".P_MODULES.'menu_componen/foother.php');
    ?>
</body>
</html>
